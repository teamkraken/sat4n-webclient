// App.js
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import App, { DefaultContainer, NotFoundContainer } from './App'

describe('App', () => {
  it('Renders without crashing ', () => {
    const wrapper = shallow(<App />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
