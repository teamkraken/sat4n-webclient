import { ADD_ROLE, DELETE_ROLE, GET_ROLE, GET_ROLES, UPDATE_FORM, UPDATE_ROLE, TOGGLE_ENDPOINT, ERROR_ROLES, CLEAR_ROLE_ERRORS } from '../constants/roleConstants'
import service from '../services/roleService'

/**
 * Retrieve token from localstorage
 * @returns JWT token
 */
const getToken = () => {
  return window.localStorage.sat4nJWT || ''
}

export /**
 * Action to add a role
 * Calls role service function addRole
 * @param {object} newRole the role to be added
 * @returns Function addRoleSuccess or addRoleFailed
 */
const addRole = (newRole) => (dispatch) => {
  return service.addRole(newRole, getToken())
    .then((role) => {
      dispatch(addRoleSuccess(role))
    })
    .catch((err) => {
      dispatch(addRoleFailed(err))
    })
}

export /**
 * Called when a Roles have been successfully retrieved
 * Returns an action to the role reducer
 * @param {object} role
 * @returns Action GET_ROLES
 */
const addRoleSuccess = (role) => {
  return {
    type: ADD_ROLE,
    payload: role
  }
}

export /**
 * Called when the server responds with an error to the POST request
 * Returns an action to the role reducer
 * @param {object} err error object from server
 * @returns Action ERROR_ROLES
 */
const addRoleFailed = (err) => {
  return {
    type: ERROR_ROLES,
    payload: `${err.message} CREATE`
  }
}

export /**
 * Action to delete a role
 * Calls role service function deleteRole
 * @param {number} id Id of the role to delete
 * @returns Function deleteRoleSuccess or deleteRoleFailed
 */
const deleteRole = (id) => (dispatch) => {
  return service.deleteRole(id, getToken())
    .then(() => {
      dispatch(deleteRoleSuccess(id))
    })
    .catch((err) => {
      dispatch(deleteRoleFailed(err))
    })
}

export /**
 * Called when a Roles have been successfully retrieved
 * Returns an action to the role reducer
 * @param {int} id
 * @returns Action DELETE_ROLE
 */
const deleteRoleSuccess = (id) => {
  return {
    type: DELETE_ROLE,
    payload: id
  }
}

export /**
 * Called when the server respnds with an error to the DELETE request
 * Returns an action to the role reducer
 * @param {object} err error object from server
 * @returns Action ERROR_ROLES
 */
const deleteRoleFailed = (err) => {
  return {
    type: ERROR_ROLES,
    payload: `${err.message} DELETE`
  }
}

export /**
 * Action to get role by ID
 * Calls device service funtion getRole
 * @param {number} id
 * @returns Function getRoleSuccess or getRoleFailed
 */
const getRole = (id) => (dispatch) => {
  return service.getRole(id, getToken())
    .then((role) => {
      dispatch(getRoleSuccess(role))
    })
    .catch((err) => {
      dispatch(getRoleFailed(err))
    })
}

export /**
 * Called when a Role have been successfully retrieved
 * Returns an action to the role reducer
 * @param {object} role
 * @returns Action GET_ROLE
 */
const getRoleSuccess = (role) => {
  return {
    type: GET_ROLE,
    payload: role
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns action to the role reducer
 * @param {object} err error object from server
 * @returns Action ERROR_ROLES
 */
const getRoleFailed = (err) => {
  return {
    type: ERROR_ROLES,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to get a list of all roles
 * Calls roles service function getRoles
 * @returns Function getRolesSuccess or getRolesFailed
 */
const getRoles = () => (dispatch) => {
  return service.getRoles(getToken())
    .then((roles) => {
      dispatch(getRolesSuccess(roles))
    })
    .catch((err) => {
      dispatch(getRolesFailed(err))
    })
}

export /**
 * Called when a Roles have been successfully retrieved
 * Returns an action to the role reducer
 * @param {object} roles
 * @returns Action GET_ROLES
 */
const getRolesSuccess = (roles) => {
  return {
    type: GET_ROLES,
    payload: roles
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the role reducer
 * @param {object} err error object from server
 * @returns Action ERROR_ROLES
 */
const getRolesFailed = (err) => {
  return {
    type: ERROR_ROLES,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to update a user
 * Calls role service function updateRole
 * @param {object} newRole
 * @returns Function updateRoleSuccess or updateRoleFailed
 */
const updateRole = (newRole) => (dispatch) => {
  return service.updateRole(newRole, getToken())
    .then((role) => {
      dispatch(updateRoleSuccess(role))
    })
    .catch((err) => {
      dispatch(updateRoleFailed(err))
    })
}

export /**
 * Called when a Roles have been successfully retrieved
 * Returns an action to the role reducer
 * @param {object} Role
 * @returns Action UPDATE_ROLE
 */
const updateRoleSuccess = (role) => {
  return {
    type: UPDATE_ROLE,
    payload: role
  }
}

export /**
 * Called when the server responds with an error to the PUT request
 * Returns an action to the role reducer
 * @param {object} err error object from server
 * @returns Action ERROR_ROLES
 */
const updateRoleFailed = (err) => {
  return {
    type: ERROR_ROLES,
    payload: `${err.message} UPDATE`
  }
}

export /**
 * onChange update Object key on input name
 * @param {object} e object that triggered change event
 * @returns Action UPDATE_FORM
 */
const onChange = (e) => (dispatch) => {
  dispatch({ type: UPDATE_FORM, payload: e.target })
}

export /**
 * onChange update Object key on input name
 * @param {object} e object that triggered change event
 * @returns Action TOGGLE_ENDPOINT
 */
const onToggle = (e) => (dispatch) => {
  dispatch({ type: TOGGLE_ENDPOINT, payload: e.target })
}

export /**
 * Action to clear the error from the redux store
 * @returns Action CLEAR_ROLE_ERRORS
 */
const clearErrors = () => {
  return {
    type: CLEAR_ROLE_ERRORS
  }
}
