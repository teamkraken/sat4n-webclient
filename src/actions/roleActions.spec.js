/* eslint-env jest */
import moxios from 'moxios'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './roleActions'
import * as types from '../constants/roleConstants'

const middlewares = [thunk]
let mockStore, initialState

describe('Role Actions', () => {
  beforeEach(() => {
    mockStore = configureStore(middlewares)
    initialState = {}
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should create an action to getRole', () => {
    const expectedReply = {
      id: 1,
      name: 'Administrators'
    }

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_ROLE,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getRole(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getRole)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getRole(9999))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to getRoles', () => {
    const expectedReply = [
      { id: 1, name: 'Administrators' }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getRoles())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getRoles)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getRoles())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to addRole', () => {
    const newRole = {
      id: 3,
      name: 'testRole'
    }

    const expectedReply = newRole

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.ADD_ROLE,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addRole(newRole))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (addRole)', () => {
    const newRole = {
      id: 3,
      name: 'testRole'
    }

    const expectedReply = '401 undefined CREATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addRole(newRole))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to deleteRole', () => {
    const id = 3

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: 1
      })
    })

    const expectedActions = [{
      type: types.DELETE_ROLE,
      payload: id
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.deleteRole(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (deleteRole)', () => {
    const id = 3

    const expectedReply = '401 undefined DELETE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.deleteRole(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to updateRole', () => {
    const updateRole = {
      name: 'update'
    }

    const expectedReply = updateRole

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.UPDATE_ROLE,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateRole(updateRole))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (updateRole)', () => {
    const updateRole = {
      name: 'update'
    }

    const expectedReply = '401 undefined UPDATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_ROLES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateRole(updateRole))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action clearErrors', () => {
    const expectedActions = {
      type: types.CLEAR_ROLE_ERRORS
    }

    const store = mockStore(initialState)

    expect(store.dispatch(actions.clearErrors())).toEqual(expectedActions)
  })
})
