import { AUDIT_GET, ERROR_AUDIT } from '../constants/auditConstants'
import service from '../services/auditService'

/**
 * Retrieve token from localstorage
 * @returns JWT token
 */
const getToken = () => {
  return window.localStorage.sat4nJWT || ''
}

export /**
 * Action to get a list of all audit logs
 * Calls audit service function getAudit
 * @returns Function getAuditSuccess or getAuditFailed
 */
const getAudit = () => (dispatch) => {
  return service.getAudit(getToken())
    .then((logs) => {
      dispatch(getAuditSuccess(logs))
    })
    .catch((err) => {
      dispatch(getAuditFailed(err))
    })
}

export /**
 * Called when the audit log has been successfully retrieved
 * Returns an action to the audit reducer
 * @param {list} logs list of audit logs
 * @returns Action GET_AUDIT
 */
const getAuditSuccess = (logs) => {
  return {
    type: AUDIT_GET,
    payload: logs
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Retruns an action to the audit reducer
 * @param {object} err error object from server
 * @returns Action ERROR_AUDIT
 */
const getAuditFailed = (err) => {
  return {
    type: ERROR_AUDIT,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to get a list of all audit logs for specific user
 * Calls audit service function getAuditByUserId
 * @param {number} id id of user
 * @returns Function getAuditByUserIdSuccess or getAuditByUserIdFailed
 */
const getAuditByUserId = (id) => (dispatch) => {
  return service.getAuditByUserId(getToken(), id)
    .then((logs) => {
      dispatch(getAuditByUserIdSuccess(logs))
    })
    .catch((err) => {
      dispatch(getAuditByUserIdFailed(err))
    })
}

export /**
 * Called when the audit log for a specific user has been successfully retrieved
 * Returns an action to the audit reducer
 * @param {list} logs list of audit logs for a specific user
 * @returns Action Audit_GET
 */
const getAuditByUserIdSuccess = (logs) => {
  return {
    type: AUDIT_GET,
    payload: logs
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the audit reducer
 * @param {object} err error object from server
 * @returns Action ERROR_AUDIT
 */
const getAuditByUserIdFailed = (err) => {
  return {
    type: ERROR_AUDIT,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to get a list of all audit logs for specific device
 * Calls audit service function getAuditByDeviceId
 * @param {number} id id of device
 * @returns Function getAuditByDeviceIdSuccess or getAuditByDeviceIdFailed
 */
const getAuditByDeviceId = (id) => (dispatch) => {
  return service.getAuditByDeviceId(getToken(), id)
    .then((logs) => {
      dispatch(getAuditByDeviceIdSuccess(logs))
    })
    .catch((err) => {
      dispatch(getAuditByDeviceIdFailed(err))
    })
}

export /**
 * Called when the audit log for a specific device has been successfully retrieved
 * Returns an action to the audit reducer
 * @param {list} logs list of audit logs for a specific device
 * @returns Action Audit_GET
 */
const getAuditByDeviceIdSuccess = (logs) => {
  return {
    type: AUDIT_GET,
    payload: logs
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the audit reducer
 * @param {object} err error object from server
 * @returns Action ERROR_AUDIT
 */
const getAuditByDeviceIdFailed = (err) => {
  return {
    type: ERROR_AUDIT,
    payload: `${err.message} GET`
  }
}
