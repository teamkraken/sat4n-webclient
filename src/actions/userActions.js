import { ADD_USER, DELETE_USER, GET_USER, GET_USERS, UPDATE_FORM, UPDATE_USER, ERROR_USERS, CLEAR_USER_ERRORS } from '../constants/userConstants'
import service from '../services/userService'

/**
 * Retrieve token from localstorage
 * @returns JWT token
 */
const getToken = () => {
  return window.localStorage.sat4nJWT || ''
}

export /**
 * Action to add a user
 * Calls user service function addUser
 * @param {object} newUser the user to be added
 * @returns Function addUserSuccess or addUserFailed
 */
const addUser = (newUser) => (dispatch) => {
  return service.addUser(newUser, getToken())
    .then((user) => {
      dispatch(addUserSuccess(user))
    })
    .catch((err) => {
      dispatch(addUserFailed(err))
    })
}

export /**
 * Called when a users have been successfully retrieved
 * Returns an action to the user reducer
 * @param {object} users
 * @returns Action GET_USERS
 */
const addUserSuccess = (user) => {
  return {
    type: ADD_USER,
    payload: user
  }
}

export /**
 * Called when the server responds with an error to the POST request
 * Returns an action to the user reducer
 * @param {object} err error object from server
 * @returns Action ERROR_USERS
 */
const addUserFailed = (err) => {
  return {
    type: ERROR_USERS,
    payload: `${err.message} CREATE`
  }
}

export /**
 * Action to delete a user
 * Calls user service function deleteUser
 * @param {number} id Id of the user to be deleted
 * @returns Function deleteUserSuccess or deleteUserFailed
 */
const deleteUser = (id) => (dispatch) => {
  return service.deleteUser(id, getToken())
    .then(() => {
      dispatch(deleteUserSuccess(id))
    })
    .catch((err) => {
      dispatch(deleteUserFailed(err))
    })
}

export /**
 * Called when the server responds with an error to the DELETE request
 * Returns an action to the user reducer
 * @param {object} err error object from server
 * @returns Action ERROR_USERS
 */
const deleteUserFailed = (err) => {
  return {
    type: ERROR_USERS,
    payload: `${err.message} DELETE`
  }
}

export /**
 * Called when a users have been successfully retrieved
 * Returns an action to the user reducer
 * @param {object} users
 * @returns Action DELETE_USER
 */
const deleteUserSuccess = (id) => {
  return {
    type: DELETE_USER,
    payload: id
  }
}

export /**
 * Action to get user by ID
 * Calls user service function getUser
 * @param {number} id
 * @returns Function getUserSuccess or getUserFailed
 */
const getUser = (id) => (dispatch) => {
  return service.getUser(id, getToken())
    .then((user) => {
      dispatch(getUserSuccess(user))
    })
    .catch((err) => {
      dispatch(getUserFailed(err))
    })
}

export /**
 * Called when a user have been successfully retrieved
 * Returns an action to the user reducer
 * @param {object} user
 * @returns Action GET_USER
 */
const getUserSuccess = (user) => {
  return {
    type: GET_USER,
    payload: user
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the user reducer
 * @param {object} err error object from server
 * @returns Action ERROR_USERS
 */
const getUserFailed = (err) => {
  return {
    type: ERROR_USERS,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to get a list of all users
 * Calls user service function getUsers
 * @returns Function getUsersSuccess or getUsersFailed
 */
const getUsers = () => (dispatch) => {
  return service.getUsers(getToken())
    .then((users) => {
      dispatch(getUsersSuccess(users))
    })
    .catch((err) => {
      dispatch(getUsersFailed(err))
    })
}

export /**
 * Called when a users have been successfully retrieved
 * Returns an action to the user reducer
 * @param {object} users
 * @returns Action GET_USERS
 */
const getUsersSuccess = (users) => {
  return {
    type: GET_USERS,
    payload: users
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the user reducer
 * @param {object} err error object from server
 * @returns Action ERROR_USERS
 */
const getUsersFailed = (err) => {
  return {
    type: ERROR_USERS,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to update a user
 * Calls user service function updateUser
 * @param {object} user
 * @returns Function updateUserSuccess or updateUserFailed
 */
const updateUser = (user) => (dispatch) => {
  return service.updateUser(user, getToken())
    .then((user) => {
      dispatch(updateUserSuccess(user))
    })
    .catch((err) => {
      dispatch(updateUserFailed(err))
    })
}

export /**
 * Called when a users have been successfully retrieved
 * Returns an action to the user reducer
 * @param {object} user
 * @returns Action UPDATE_USER
 */
const updateUserSuccess = (users) => {
  return {
    type: UPDATE_USER,
    payload: users
  }
}

export /**
 * Called when the server responds with an error to the PUT request
 * Returns an action to the user reducer
 * @param {object} err error object from server
 * @returns Action ERROR_USERS
 */
const updateUserFailed = (err) => {
  return {
    type: ERROR_USERS,
    payload: `${err.message} UPDATE`
  }
}

export /**
 * onChange update Object key on input name
 * @param {object} e object that triggered change event
 * @returns Action UPDATE_FORM
 */
const onChange = (e) => (dispatch) => {
  // onChange update Object key on input name
  dispatch({ type: UPDATE_FORM, payload: e.target })
}

export /**
 * Action to clear the error from the redux store
 * @returns Action CLEAR_USER_ERRORS
 */
const clearErrors = () => {
  return {
    type: CLEAR_USER_ERRORS
  }
}
