import { ADD_DEVICE, GET_DEVICE, GET_DEVICES, DELETE_DEVICE, DEVICE_PORT_DISABLE, DEVICE_PORT_ENABLE, DEVICE_PORT_RESTART, DEVICE_PORT_UPDATE, ERROR_DEVICES, CLEAR_DEVICE_ERRORS, LOADING_UPDATE } from '../constants/deviceConstants'
import service from '../services/deviceService'

/**
 * Retrieve token from localstorage
 * @returns JWT token
 */
const getToken = () => {
  return window.localStorage.sat4nJWT || ''
}

export /**
 * Action to get device by ID
 * Calls device service function getDevice
 * @param {number} id
 * @returns Function getDeviceSuccess or getDeviceFailed
 */
const getDevice = (id) => (dispatch) => {
  return service.getDevice(id, getToken())
    .then((device) => {
      dispatch(getDeviceSuccess(device))
    })
    .catch((err) => {
      dispatch(getDeviceFailed(err))
    })
}

export /**
 * Called when a device has been successfully retrieved
 * Returns an action to the device reducer
 * @param {object} device object with device info
 * @returns Action GET_DEVICE
 */
const getDeviceSuccess = (device) => {
  return {
    type: GET_DEVICE,
    payload: device
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const getDeviceFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to get a list of all devices
 * Calls device service function getDevices
 * @returns Function getDevicesSuccess or getDevicesFailed
 */
const getDevices = () => (dispatch) => {
  return service.getDevices(getToken())
    .then((devices) => {
      dispatch(getDevicesSuccess(devices))
    })
    .catch((err) => {
      dispatch(getDevicesFailed(err))
    })
}

export /**
 * Called when the devices list has been successfully retrieved
 * Returns an action to the device reducer
 * @param {object} devices
 * @returns Action GET_DEVICES
 */
const getDevicesSuccess = (devices) => {
  return {
    type: GET_DEVICES,
    payload: devices
  }
}

export /**
 * Called when the server responds with an error to the GET request
 * Retruns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const getDevicesFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} GET`
  }
}

export /**
 * Action to add a device
 * Calls device service function addDevice
 * @param {object} newDevice the device to be added
 * @returns Function addDeviceSuccess or addDeviceFailed
 */
const addDevice = (newDevice) => (dispatch) => {
  return service.addDevice(newDevice, getToken())
    .then((device) => {
      dispatch(addDeviceSuccess(device))
    })
    .catch((err) => {
      dispatch(addDeviceFailed(err))
    })
}

export /**
 * Called when a device has been successfully added
 * Returns an action to the device reducer
 * @param {object} device the device that was added
 * @returns Action ADD_DEVICE
 */
const addDeviceSuccess = (device) => {
  return {
    type: ADD_DEVICE,
    payload: device
  }
}

export /**
 * Called when the server responds with an error to the POST request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICE
 */
const addDeviceFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} CREATE`
  }
}

export /**
 * Action to delete a device
 * Calls device survice function deleteDevice
 * @param {number} id Id of the device to delete
 * @returns Function removeDeviceSuccess or removeDeviceFailed
 */
const removeDevice = (id) => (dispatch) => {
  return service.deleteDevice(id, getToken())
    .then(() => {
      dispatch(removeDeviceSuccess(id))
    })
    .catch((err) => {
      dispatch(removeDeviceFailed(err))
    })
}

export /**
 * Called when a device has been successfully deleted
 * Returns an action to the device reducer
 * @param {number} id Id of the deleted device
 * @returns Action DELETE_DEVICE
 */
const removeDeviceSuccess = (id) => {
  return {
    type: DELETE_DEVICE,
    payload: id
  }
}

export /**
 * Called when the server responds with an error to the DELETE request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const removeDeviceFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} DELETE`
  }
}

export /**
 * Action to restart port on a device
 * Calls device survice function restartDevicePort
 * @param {number} deviceID Id of the device
 * @param {number} portID id of the port to be restarted
 * @returns Function restartDevicePortSuccess or restartDevicePortFailed
 */
const restartDevicePort = (deviceID, portID) => (dispatch) => {
  return service.restartDevicePort(deviceID, portID, getToken())
    .then((data) => {
      dispatch(restartDevicePortSuccess(data, portID))
    })
    .catch((err) => {
      dispatch(restartDevicePortFailed(err))
    })
}

export /**
 * Called when a port has been successfully restarted
 * Returns an action to the device reducer
 * @param {number} data 1
 * @param {number} portID id of port that was restarted
 * @returns Action DEVICE_PORT_RESTART
 */
const restartDevicePortSuccess = (data, portID) => {
  return {
    type: DEVICE_PORT_RESTART,
    payload: portID
  }
}

export /**
 * Called when the server responds with an error to the RESTART request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const restartDevicePortFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} RESTART`
  }
}

export /**
 * Action to update port on a device
 * Calls device survice function updateDevicePort
 * @param {number} deviceID Id of the device
 * @param {number} portID id of the port to be updated
 * @param {number} vlanID id of the vlan to be applied to the port
 * @returns Function updateDevicePortSuccess or updateDevicePortFailed
 */
const updateDevicePort = (deviceID, portID, vlanID) => (dispatch) => {
  return service.updateDevicePort(deviceID, portID, vlanID, getToken())
    .then((device) => {
      dispatch(updateDevicePortSuccess(device, portID))
    })
    .catch((err) => {
      dispatch(updateDevicePortFailed(err))
    })
}

export /**
 * Called when a port has been successfully updated
 * Returns an action to the device reducer
 * @param {number} device
 * @param {number} portID id of port that was updated
 * @returns Action DEVICE_PORT_UPDATE
 */
const updateDevicePortSuccess = (device, portID) => {
  return {
    type: DEVICE_PORT_UPDATE,
    payload: {
      device,
      portID
    }
  }
}

export /**
 * Called when the server responds with an error to the UPDATE request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const updateDevicePortFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} UPDATE`
  }
}

export /**
 * Action to disable port on a device
 * Calls device survice function disableDevicePort
 * @param {number} deviceID Id of the device
 * @param {number} portID id of the port to be disabled
 * @returns Function updateDevicePortSuccess or updateDevicePortFailed
 */
const disableDevicePort = (deviceID, portID) => (dispatch) => {
  return service.disableDevicePort(deviceID, portID, getToken())
    .then((device) => {
      dispatch(disableDevicePortSuccess(device, portID))
    })
    .catch((err) => {
      dispatch(disableDevicePortFailed(err))
    })
}

export /**
 * Called when a port has been successfully disabled
 * Returns an action to the device reducer
 * @param {object} device
 * @param {number} portID id of port that was disabled
 * @returns Action DEVICE_PORT_DISABLE
 */
const disableDevicePortSuccess = (device, portID) => {
  return {
    type: DEVICE_PORT_DISABLE,
    payload: {
      device,
      portID
    }
  }
}

export /**
 * Called when the server responds with an error to the DISABLE request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const disableDevicePortFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} DISABLE`
  }
}

export /**
 * Action to enable port on a device
 * Calls device survice function enableDevicePort
 * @param {number} deviceID Id of the device
 * @param {number} portID id of the port to be enabled
 * @returns Function updateDevicePortSuccess or updateDevicePortFailed
 */
const enableDevicePort = (deviceID, portID) => (dispatch) => {
  return service.enableDevicePort(deviceID, portID, getToken())
    .then((device) => {
      dispatch(enableDevicePortSuccess(device, portID))
    })
    .catch((err) => {
      dispatch(enableDevicePortFailed(err))
    })
}

export /**
 * Called when a port has been successfully enabled
 * Returns an action to the device reducer
 * @param {object} device
 * @param {number} portID id of port that was enabled
 * @returns Action DEVICE_PORT_ENABLE
 */
const enableDevicePortSuccess = (device, portID) => {
  return {
    type: DEVICE_PORT_ENABLE,
    payload: {
      device,
      portID
    }
  }
}

export /**
 * Called when the server responds with an error to the enable request
 * Returns an action to the device reducer
 * @param {object} err error object from server
 * @returns Action ERROR_DEVICES
 */
const enableDevicePortFailed = (err) => {
  return {
    type: ERROR_DEVICES,
    payload: `${err.message} ENABLE`
  }
}

export /**
 * Action to clear the error from the redux store
 * @returns Action CLEAR_DEVICES_ERRORS
 */
const clearErrors = () => {
  return {
    type: CLEAR_DEVICE_ERRORS
  }
}

export /**
 * Action to update loading array in redux store
 * @param {number} portID
 * @returns ACTION LOADING_UPDATE
 */
const updateLoading = (portID) => {
  return {
    type: LOADING_UPDATE,
    payload: portID
  }
}
