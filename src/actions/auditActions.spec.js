/* eslint-env jest */
import moxios from 'moxios'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './auditActions'
import * as types from '../constants/auditConstants'

const middlewares = [thunk]
let mockStore, initialState

describe('Audit Actions', () => {
  beforeEach(() => {
    mockStore = configureStore(middlewares)
    initialState = {}
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should create an action to getAudit', () => {
    const expectedReply = [
      { id: 1, action: 'Device:CREATE', details: 'Detailed info', userId: 1, createdAt: '01/01/2019', deviceId: 1 }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.AUDIT_GET,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAudit())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getAudit)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_AUDIT,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAudit())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to getAuditByUserId', () => {
    const expectedReply = [
      { id: 1, action: 'Device:CREATE', details: 'Detailed info', userId: 1, createdAt: '01/01/2019', deviceId: 1 }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.AUDIT_GET,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAuditByUserId(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getAuditByUserId)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_AUDIT,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAuditByUserId(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to getAuditByDeviceId', () => {
    const expectedReply = [
      { id: 1, action: 'Device:CREATE', details: 'Detailed info', userId: 1, createdAt: '01/01/2019', deviceId: 1 }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.AUDIT_GET,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAuditByDeviceId(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getAuditByDeviceId)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_AUDIT,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getAuditByDeviceId(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })
})
