/* eslint-env jest */
import moxios from 'moxios'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './deviceActions'
import * as types from '../constants/deviceConstants'

const middlewares = [thunk]
let mockStore, initialState

describe('Device Actions', () => {
  beforeEach(() => {
    mockStore = configureStore(middlewares)
    initialState = {}
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should create an action to getDevice', () => {
    const expectedReply = {
      id: 1,
      name: 'c3560-test-1',
      ip: '1.1.1.1'
    }

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_DEVICE,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getDevice(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getDevice)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getDevice(9999))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to getDevices', () => {
    const expectedReply = [
      { id: 1, name: 'c3560-test-1', ip: '1.1.1.1' }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getDevices())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getDevices)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getDevices())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to addDevice', () => {
    const newDevice = {
      ip: '10.100.66.1'
    }

    const expectedReply = newDevice

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.ADD_DEVICE,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addDevice(newDevice))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (addDevice)', () => {
    const newDevice = {
      ip: 'ads'
    }

    const expectedReply = '401 undefined CREATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addDevice(newDevice))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to removeDevice', () => {
    const id = 2

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: 1
      })
    })

    const expectedActions = [{
      type: types.DELETE_DEVICE,
      payload: id
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.removeDevice(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (removeDevice)', () => {
    const id = 2

    const expectedReply = '401 undefined DELETE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.removeDevice(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to restartDevicePort', () => {
    const deviceID = 1
    const portID = 1

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: 1
      })
    })

    const expectedActions = [{
      type: types.DEVICE_PORT_RESTART,
      payload: portID
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.restartDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (restartDevicePort)', () => {
    const deviceID = 1
    const portID = 1

    const expectedReply = '401 undefined RESTART'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.restartDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to disableDevicePort', () => {
    const deviceID = 1
    const portID = 1

    const expectedReply = 1

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.DEVICE_PORT_DISABLE,
      payload: {
        device: expectedReply,
        portID
      }
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.disableDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (disableDevicePort)', () => {
    const deviceID = 1
    const portID = 1

    const expectedReply = '401 undefined DISABLE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.disableDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to enableDevicePort', () => {
    const deviceID = 1
    const portID = 1

    const expectedReply = 1

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.DEVICE_PORT_ENABLE,
      payload: {
        device: expectedReply,
        portID
      }
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.enableDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (enableDevicePort)', () => {
    const deviceID = 1
    const portID = 1

    const expectedReply = '401 undefined ENABLE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.enableDevicePort(deviceID, portID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to updateDevicePort', () => {
    const deviceID = 1
    const portID = 1
    const vlanID = 1

    const expectedReply = {
      ip: '10.100.66.1',
      vlanID
    }

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.DEVICE_PORT_UPDATE,
      payload: {
        device: expectedReply,
        portID
      }
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateDevicePort(deviceID, portID, vlanID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (updateDevicePort)', () => {
    const deviceID = 1
    const portID = 1
    const vlanID = 1

    const expectedReply = '401 undefined UPDATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_DEVICES,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateDevicePort(deviceID, portID, vlanID))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action clearErrors', () => {
    const expectedActions = {
      type: types.CLEAR_DEVICE_ERRORS
    }

    const store = mockStore(initialState)

    expect(store.dispatch(actions.clearErrors())).toEqual(expectedActions)
  })

  it('Should create action updateLoading', () => {
    const portID = 1

    const expectedActions = {
      type: types.LOADING_UPDATE,
      payload: portID
    }

    const store = mockStore(initialState)

    expect(store.dispatch(actions.updateLoading(portID))).toEqual(expectedActions)
  })
})
