/* eslint-env jest */
import moxios from 'moxios'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './userActions'
import * as types from '../constants/userConstants'

const middlewares = [thunk]
let mockStore, initialState

describe('User Actions', () => {
  beforeEach(() => {
    mockStore = configureStore(middlewares)
    initialState = {}
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should create an action to getUser', () => {
    const expectedReply = {
      id: 1,
      username: 'admin',
      role: {
        id: 1,
        name: 'Administrators'
      }
    }

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_USER,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getUser(1))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getUser)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getUser(9999))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to getUsers', () => {
    const expectedReply = [
      {
        id: 1,
        username: 'admin',
        role: {
          id: 1,
          name: 'Administrators'
        }
      }
    ]

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.GET_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getUsers())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (getUsers)', () => {
    const expectedReply = '401 undefined GET'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.getUsers())
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to addUser', () => {
    const newUser = {
      username: 'testus',
      password: '123',
      roleId: 1
    }

    const expectedReply = newUser

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 201,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.ADD_USER,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addUser(newUser))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (addUser)', () => {
    const newUser = {
      username: 'testus',
      password: '123',
      roleId: 1
    }

    const expectedReply = '401 undefined CREATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.addUser(newUser))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to deleteUser', () => {
    const id = 2

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: 1
      })
    })

    const expectedActions = [{
      type: types.DELETE_USER,
      payload: id
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.deleteUser(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthodized (deleteUser)', () => {
    const id = 2

    const expectedReply = '401 undefined DELETE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.deleteUser(id))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action to updateUser', () => {
    const updateUser = {
      username: 'testus',
      password: '123',
      roleId: 1
    }

    const expectedReply = updateUser

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const expectedActions = [{
      type: types.UPDATE_USER,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateUser(updateUser))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized (updateUser)', () => {
    const updateUser = {
      username: 'testus',
      password: '123',
      roleId: 1
    }

    const expectedReply = '401 undefined UPDATE'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const expectedActions = [{
      type: types.ERROR_USERS,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.updateUser(updateUser))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create action clearErrors', () => {
    const expectedActions = {
      type: types.CLEAR_USER_ERRORS
    }

    const store = mockStore(initialState)

    expect(store.dispatch(actions.clearErrors())).toEqual(expectedActions)
  })
})
