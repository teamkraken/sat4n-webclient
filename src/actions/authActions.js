import { AUTH_LOG_IN, AUTH_LOG_OUT, AUTH_FAILED } from '../constants/authConstants'
import service from '../services/authService'

export /**
 * Action to authenticate a user
 * Calls auth service function login
 * and adds the token to localStorage if successfull
 * @param {object} credentials the credentials the user provides
 * @returns Function loginSuccess or loginFailed
 */
const login = (credentials) => (dispatch) => {
  return service.login(credentials)
    .then((login) => {
      window.localStorage.sat4nJWT = login.token
      window.localStorage.resources = JSON.stringify(login.resources)
      window.localStorage.user = JSON.stringify(login.user)
      dispatch(loginSuccess(login))
    })
    .catch(err => {
      dispatch(loginFailed(err))
    })
}

/**
 * Called when a user successfully logs in
 * Returns an action to the auth reducer
 * @param {string} token authentication token from server
 * @returns Action AUTH_LOG_IN
 */
const loginSuccess = (login) => {
  return {
    type: AUTH_LOG_IN,
    payload: login
  }
}

/**
 * Called when the server responds with an error to the login request
 * Returns an action to the auth reducer
 * @param {object} err error object from server
 * @returns Action AUTH_FAILED
 */
const loginFailed = (err) => {
  return {
    type: AUTH_FAILED,
    payload: err.message
  }
}

export /**
 * Action to logout a user, removes authentication token from locaStorage
 * @returns Action AUTH_LOG_OUT
 */
const logout = () => {
  window.localStorage.removeItem('sat4nJWT')
  return {
    type: AUTH_LOG_OUT
  }
}
