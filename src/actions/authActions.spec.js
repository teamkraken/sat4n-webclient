/* eslint-env jest */
import moxios from 'moxios'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './authActions'
import * as types from '../constants/authConstants'

const middlewares = [thunk]
let mockStore, initialState

describe('Auth Actions', () => {
  beforeEach(() => {
    mockStore = configureStore(middlewares)
    initialState = {}
    moxios.install()
  })

  afterEach(() => {
    moxios.uninstall()
  })

  it('Should create an action to login', () => {
    const expectedReply = {
      user: {
        id: 1,
        username: 'admin'
      },
      resources: {
        devices: ['create', 'delete', 'update', 'get', 'list'],
        roles: ['create', 'delete', 'update', 'get', 'list'],
        users: ['create', 'delete', 'update', 'get', 'list']
      },
      token: 'random token'
    }

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: expectedReply
      })
    })

    const credentials = {
      username: 'admin',
      password: 'admin'
    }

    const expectedActions = [{
      type: types.AUTH_LOG_IN,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.login(credentials))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should fail when unauthorized', () => {
    const expectedReply = '401: undefined'

    moxios.wait(() => {
      const request = moxios.requests.mostRecent()
      request.respondWith({
        status: 401,
        expectedReply
      })
    })

    const credentials = {
      username: 'wrong',
      password: 'wrong'
    }

    const expectedActions = [{
      type: types.AUTH_FAILED,
      payload: expectedReply
    }]

    const store = mockStore(initialState)

    return store.dispatch(actions.login(credentials))
      .then(() => expect(store.getActions()).toEqual(expectedActions))
  })

  it('Should create an action to logout', () => {
    const expectedAction = {
      type: types.AUTH_LOG_OUT
    }

    expect(actions.logout()).toEqual(expectedAction)
  })
})
