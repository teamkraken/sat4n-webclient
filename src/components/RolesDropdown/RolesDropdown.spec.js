// RolesDropdown.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { RolesDropdown } from './RolesDropdown.jsx'

describe('RolesDropdown', () => {
  const props = {
    id: 1,
    restriction: false,
    disabled: false,
    onChange: jest.fn(),
    getRoles: jest.fn(),
    roles: []
  }

  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<RolesDropdown {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should get roles if user is authorized', () => {
    const newProps = {
      ...props,
      restriction: true
    }
    const wrapper = shallow(<RolesDropdown {...newProps} />)

    expect(wrapper.instance().props.getRoles.mock.calls.length).toBe(0)
  })
})
