import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { getRoles } from '../../actions/roleActions'

class RolesDropdown extends Component {
  componentDidMount () {
    const { restriction, getRoles } = this.props

    if (!restriction) {
      getRoles()
    }
  }

  render () {
    const { onChange, id, roles, disabled } = this.props

    return (
      <>
        <select
          className='form-control'
          type='select'
          name='role'
          id='role'
          value={id}
          onChange={onChange}
          disabled={disabled}
        >
          <option value='0'>Select role</option>
          {roles.map(/* istanbul ignore next */(role) => {
            return <option key={role.id} value={role.id}>{role.name}</option>
          })}
        </select>
      </>
    )
  }
}

/* istanbul ignore next */
const mapStateToProps = ({ role }) => {
  return {
    roles: role.roles
  }
}

RolesDropdown.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired,
  restriction: PropTypes.bool,
  disabled: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  getRoles: PropTypes.func.isRequired,
  roles: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
      description: PropTypes.string
    })
  ).isRequired
}

RolesDropdown.defaultProps = {
  restriction: false,
  disabled: false
}

export { RolesDropdown }
export default connect(mapStateToProps, { getRoles })(RolesDropdown)
