// AuditPage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { AuditPage } from './AuditPage'

describe('AuditPage', () => {
  let wrapper
  const props = {
    audit: {
      error: '',
      logs: [
        {
          id: 1,
          action: 'Device:CREATE',
          details: 'Detailed info',
          userId: 1,
          createdAt: '01/01/2019',
          deviceId: 1
        }
      ]
    },
    getAudit: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<AuditPage {... props} />)
  })

  it('Should render correctly, given correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when visible is false', () => {
    wrapper.setProps({ ...props, audit: { ...props.audit, error: 'error' } })
    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })

  it('Should change state on search', () => {
    const event = {
      target: { value: 'value' }
    }

    wrapper.find('SearchBar').dive()
      .find('input').simulate('change', event)

    wrapper.instance().handleInput(event)
    expect(wrapper.instance().state.search).toEqual('value')
  })
})
