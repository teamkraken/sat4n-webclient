import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { getAudit } from '../../actions/auditActions'
/* Components */
import { AuditList } from '../AuditList/AuditList'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import SearchBar from '../SearchBar/SearchBar'
import AuditPageManual from '../Manuals/AuditPageManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Styles */
import '../../styles/Main.less'

class AuditPage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      search: ''
    }

    this.handleInput = this.handleInput.bind(this)
  }

  componentDidMount () {
    const { getAudit } = this.props

    getAudit()
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { search } = this.state
    const { audit } = this.props
    const { error, logs } = audit
    return error
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2>Audit Log</h2>
          </span>
          <UserManualModal
            title='Audit Log User Information'
            CurrentPageManual={AuditPageManual}
          />
          <SearchBar onInput={this.handleInput} />
          <AuditList logs={logs} searchInput={search} />
        </div>
        )
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ audit }) => {
  return { audit }
}

AuditPage.propTypes = {
  getAudit: PropTypes.func.isRequired,
  audit: PropTypes.shape({
    error: PropTypes.string,
    logs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        action: PropTypes.string.isRequired,
        details: PropTypes.string.isRequired,
        userId: PropTypes.number,
        createdAt: PropTypes.string.isRequired,
        deviceId: PropTypes.number
      }).isRequired
    ).isRequired
  }).isRequired
}

export { AuditPage }
export default connect(mapStateToProps, { getAudit })(AuditPage)
