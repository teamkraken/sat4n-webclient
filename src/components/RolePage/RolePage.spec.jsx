// UserPage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { RolePage } from './RolePage.jsx'

describe('RolePage', () => {
  let wrapper
  const props = {
    role: {
      error: ''
    },
    getRole: jest.fn(),
    match: {
      params: {
        id: 1
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<RolePage {...props} />)
  })

  it('should render correctly given the right props', () => {
    expect(toJson).toMatchSnapshot()
    expect(props.getRole.mock.calls.length).toBe(1)
  })

  it('Should render differently when error is resieved', () => {
    wrapper.setProps({ ...props, role: { ...props.role, error: 'GET' } })
    wrapper.update()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
