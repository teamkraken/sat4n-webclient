import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { getRole } from '../../actions/roleActions'
/* Components */
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import RoleInfo from '../RoleInfo/RoleInfo'
import RolePageManual from '../Manuals/RolePageManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Styles */
import '../../styles/Main.less'
import { withRouter } from '../../utils'

class RolePage extends Component {
  componentDidMount () {
    const { getRole } = this.props

    getRole(this.props.match.params.id)
  }

  render () {
    const { error } = this.props.role
    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2>Edit Role</h2>
          </span>
          <UserManualModal
            title='Edit Role User Information'
            CurrentPageManual={RolePageManual}
          />
          <RoleInfo />
        </div>
        )
  }
}

/* istanbul ignore next */
const mapStateToProps = ({ role }) => {
  return { role }
}

RolePage.propTypes = {
  getRole: PropTypes.func.isRequired,
  role: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired
}

export { RolePage }
export default connect(mapStateToProps, { getRole })(withRouter(RolePage))
