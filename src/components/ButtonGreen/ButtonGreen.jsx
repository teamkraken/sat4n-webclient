import React from 'react'
import PropTypes from 'prop-types'

const AddButton = ({ onClick, label, type }) => {
  return (<button type={type} className='btn btn-md btn-success' onClick={onClick}>{label}</button>)
}

AddButton.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string
}

AddButton.defaultProps = {
  type: 'button'
}

export default AddButton
