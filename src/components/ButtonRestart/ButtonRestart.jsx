import React from 'react'
import PropTypes from 'prop-types'

const ButtonRestart = ({ onClick, id }) => {
  return (
    <span className='col-md deletebtn'>
      <i className='fa fa-sync text-danger' onClick={(id) => onClick(id)} />
    </span>
  )
}

ButtonRestart.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired
}

export default ButtonRestart
