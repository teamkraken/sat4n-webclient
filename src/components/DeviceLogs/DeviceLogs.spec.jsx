// DeviceLogs.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DeviceLogs } from './DeviceLogs'

describe('DeviceLogs', () => {
  let wrapper
  const props = {
    searchInput: '',
    device: {
      device: {
        log: []
      }
    }
  }

  it('should render given the correct props', () => {
    wrapper = shallow(<DeviceLogs {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
