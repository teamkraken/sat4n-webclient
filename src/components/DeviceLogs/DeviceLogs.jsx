import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import UserManualModal from '../UserManualModal/UserManualModal'
import LogsManual from '../Manuals/LogsManual'
/* Styles */
import '../../styles/Main.less'

class DeviceLogs extends Component {
  render () {
    const { log } = this.props.device.device
    return (
      <>
        <span>
          <h2>Logs</h2>
        </span>
        <UserManualModal
          title='Logs User Information'
          CurrentPageManual={LogsManual}
        />
        <ul className='list listLog'>
          <ListHeader headers={['LogEntry']} />
          {log.filter(/* istanbul ignore next */(l) => l !== '').map(/* istanbul ignore next */(line, i) => {
            return line.toLowerCase().includes(this.props.searchInput.toLowerCase())
              ? <li key={i}> {line} </li>
              : null
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

DeviceLogs.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      log: PropTypes.array
    })
  })
}

export { DeviceLogs }
export default connect(mapStateToProps)(DeviceLogs)
