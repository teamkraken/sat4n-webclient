// DevicePortModal.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import DevicePortModal from './DevicePortModal.jsx'

describe('DevicePortModal', () => {
  let wrapper
  const props = {
    onCloseModal: jest.fn(),
    onChange: jest.fn(),
    onSubmit: jest.fn(),
    showModal: true,
    visible: true,
    port: {},
    vlans: [],
    vlanID: 1
  }
  const e = {
    preventDefault: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<DevicePortModal {...props} />)
  })

  it('Should render correctly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should call onClosModal when close button is clicked', () => {
    wrapper.find('ButtonGrey[label="Close"]').simulate('click')
    expect(wrapper.instance().props.onCloseModal.mock.calls.length).toBe(1)
  })

  it('Should render differently when visible is false', () => {
    wrapper.setProps({ ...props, visible: false })
    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })

  it('Should call onCloseModal when requestClose is called', () => {
    wrapper.instance().handleClose()
    expect(wrapper.instance().props.onCloseModal).toHaveBeenCalledWith('close')
  })

  it('Should call onSubmit when a form is submitted', () => {
    wrapper.find('form').simulate('submit', e)

    expect(wrapper.instance().props.onSubmit.mock.calls.length).toBe(1)
  })
})
