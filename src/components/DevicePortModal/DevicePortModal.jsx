import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'

/* Components */
import DropDownList from '../VlansDropdown/VlansDropdown'
import GreenButton from '../ButtonGreen/ButtonGreen'
import GreyButton from '../ButtonGrey/ButtonGrey'
/* Styles */
import '../../styles/Main.less'

class DevicePortModal extends Component {
  constructor (props) {
    super(props)

    this.handleClose = this.handleClose.bind(this)
  }

  handleClose () {
    const { onCloseModal } = this.props
    onCloseModal('close')
  }

  render () {
    const { visible, port, vlans, onChange, onSubmit, vlanID } = this.props
    const portKeys = Object.keys(port).filter((key) => key !== 'id')

    return visible
      ? (
        <ReactModal
          isOpen={this.props.showModal}
          onRequestClose={/* istanbul ignore next */this.handleClose}
          contentLabel='Change Vlan'
          className='modal-dialog'
          overlayClassName='modal-backdrop'
          style={{
            overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
          }}
        >
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='exampleModalLabel'>Change Vlan</h5>
            </div>
            <div className='modal-body'>
              <form className='portModal' onSubmit={onSubmit}>
                <dl className=''>
                  {portKeys.map(/* istanbul ignore next */(key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt>
                        <dd>
                          {key === 'vlan'
                            ? <DropDownList id={vlanID} vlans={vlans} onChange={onChange} />
                            : port[key]}
                        </dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
                <div className='modal-footer'>
                  <GreyButton label='Close' onClick={this.handleClose} />
                  <GreenButton label='Save' type='submit' />
                </div>
              </form>
            </div>
          </div>
        </ReactModal>
        )
      : null
  }
}

DevicePortModal.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  port: PropTypes.object.isRequired,
  vlans: PropTypes.array.isRequired,
  vlanID: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
}

export { DevicePortModal }
export default DevicePortModal
