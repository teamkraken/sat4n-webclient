// LoginForm.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import LoginForm from './LoginForm'

describe('AddDeviceForm', () => {
  it('should render correctly given the correct props', () => {
    const props = {
      submit: jest.fn()
    }

    const wrapper = shallow(<LoginForm {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('it should call the proper actions given a successful submit call', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    }

    const props = {
      submit: jest.fn(),
      onSubmit: jest.fn()
    }
    const wrapper = shallow(<LoginForm {...props} />)
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('input[type="text"]').simulate(
      'change', { target: { name: 'username', value: 'janedoe' } })
    expect(wrapper.state().data.username).toEqual('janedoe')

    wrapper.find('input[type="password"]').simulate(
      'change', { target: { name: 'password', value: 'password' } })
    expect(wrapper.state().data.password).toEqual('password')

    wrapper.find('button').simulate('click')
    wrapper.instance().handleSubmit(mockEvent)

    expect(wrapper.find('idx')).toEqual({})
  })

  it('it should call the proper actions given a unsuccessful submit call', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    }

    const props = {
      submit: jest.fn(),
      onSubmit: jest.fn()
    }
    const wrapper = shallow(<LoginForm {...props} />)
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('button').simulate('click')
    wrapper.instance().handleSubmit(mockEvent)

    expect(wrapper.state().errors).toEqual({ passwordError: 'Password is required', usernameError: 'Username is required' })
    expect(wrapper.find('idx')).toBeDefined()
  })
})
