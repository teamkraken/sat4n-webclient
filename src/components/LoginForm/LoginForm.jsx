import React, { Component } from 'react'
import PropTypes from 'prop-types'

/* Styles */
import '../../styles/Main.less'

class LoginForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: {
        username: '',
        password: ''
      },
      errors: {
        usernameError: '',
        passwordError: ''
      }
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  /**
   * Called when an input field is changed
   * Changes the state of the component
   * @param {object} e Event trigger
   * @returns New state
   * @memberof LoginForm
   */
  onChange (e) {
    const { name, value } = e.target
    return this.setState({ data: { ...this.state.data, [name]: value } })
  }

  /**
   * Called when the form is submitted
   * Checks if the data provided is valid and calls submit in LoginPage
   * @param {object} e Event trigger
   * @memberof LoginForm
   */
  handleSubmit (e) {
    e.preventDefault()
    const { data } = this.state
    const { submit } = this.props
    const valid = this.validate(data)
    if (valid) {
      this.setState({ ...data, errors: { usernameError: '', passwordError: '' } })
      submit(data)
    }
  }

  /**
   * Checks if the data is valid and sets the errors value in the state accordingly
   * @param {object} data Data to be validated
   * @returns true if the data is valid, false otherwise
   * @memberof LoginForm
   */
  validate (data) {
    const errors = {}
    if (!data.username) errors.username = 'Username is required'
    if (!data.password) errors.password = 'Password is required'
    if (Object.keys(errors).length > 0) {
      this.setState({ ...data, errors: { usernameError: errors.username, passwordError: errors.password } })
      return false
    }
    return true
  }

  /**
   * Renders the components jsx
   * @returns A form containing input fields for username and password
   * @memberof LoginForm
   */
  render () {
    const { data, errors } = this.state
    const { serverError } = this.props
    const { usernameError, passwordError } = errors
    return (

      <div className='loginForm'>
        <form className='form-signin' onSubmit={this.handleSubmit}>
          <h1>SAT:4N</h1>
          <div className='text-danger'>{serverError}</div>
          <div id='inputEmail'>
            <label htmlFor='inputEmail' className='sr-only'>Email address</label>
            <input
              className='form-control'
              id='inputEmail'
              name='username'
              type='text'
              placeholder='Username'
              value={data.username}
              onChange={this.onChange.bind(this)}
            />
            <div className='text-danger'>{usernameError}</div>
          </div>
          <div className='input-password'>
            <label htmlFor='inputPassword' className='sr-only'>Password</label>
            <input
              className='form-control'
              id='inputPassword'
              name='password'
              type='password'
              placeholder='Password'
              value={data.password}
              onChange={this.onChange.bind(this)}
            />
            <div className='text-danger'>{passwordError}</div>
          </div>
          <button className='btn btn-lg btn-secondary btn-block' submit='submit'><b>Login</b></button>
          <p>&copy; 2019</p>
        </form>
      </div>
    )
  }
}

LoginForm.propTypes = {
  submit: PropTypes.func.isRequired,
  serverError: PropTypes.string
}

LoginForm.defaultProps = {
  serverError: ''
}

export default LoginForm
