// SubNavigationBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import SubNavigationBar from './SubNavigationBar'

describe('SubNavigationBar', () => {
  const props = {
    onClick: jest.fn()
  }
  it('should render correctly given correct props', () => {
    const wrapper = shallow(<SubNavigationBar {... props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
