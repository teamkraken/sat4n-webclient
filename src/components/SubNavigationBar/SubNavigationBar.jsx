import React from 'react'
import PropTypes from 'prop-types'

/* Components */
import SubNavbarManual from '../Manuals/SubNavbarManual'
import UserManualModal from '../UserManualModal/UserManualModal'

/* Styles */
import '../../styles/Main.less'

const SubNavigationBar = ({ onClick, audit }) => (
  <nav className='subNavbar nav navbar-expand-md navbar-dark bg-secondary'>
    <button
      className='navbar-toggler'
      type='button'
      data-toggle='collapse'
      data-target='#navbarColor02'
      aria-controls='navbarColor02'
      aria-expanded='false'
      aria-label='Toggle navigation'
    >
      <span className='navbar-toggler-icon' />
    </button>
    <ul className='navbar-nav navbar-collapse collapse' id='navbarColor02'>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('Basic')}>Ports</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('Vlans')}>Vlans</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('ARPTable')}>ARP Table</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('FDBTable')}>FDB Table</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('DHCPSnoop')}>DHCP Snooping</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('Neighbors')}>Neighbors</a>
      </li>
      <li>
        <a className='nav-link text-light px-2' href='#' onClick={() => onClick('Logs')}>Logs</a>
      </li>
      <li>
        <a className={`nav-link text-light ${audit ? '' : 'd-none'}`} href='#' onClick={() => onClick('Audit Log')}>Audit Log</a>
      </li>
    </ul>
    <UserManualModal
      className='text-light!important'
      title='Navigation Bar User Information'
      CurrentPageManual={SubNavbarManual}
    />
  </nav>
)

SubNavigationBar.propTypes = {
  onClick: PropTypes.func.isRequired
}

export default SubNavigationBar
