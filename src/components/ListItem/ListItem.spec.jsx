// ListItem.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ListItem } from './ListItem'
// import * as actions from '../../actions/deviceActions'

describe('ListItem', () => {
  let wrapper
  const props = {
    children: []
  }

  beforeEach(() => {
    wrapper = shallow(<ListItem {...props} />)
  })

  it('Should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
