import React from 'react'
import PropTypes from 'prop-types'

const ListItem = ({ children }) => {
  return (
    <li>{children}</li>
  )
}

ListItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object
  ])
}

export { ListItem }
