// UserPage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { UserPage } from './UserPage.jsx'

describe('UserPage', () => {
  let wrapper
  const props = {
    user: {
      error: ''
    },
    audit: {
      logs: [
        {
          id: 1,
          action: 'Device:CREATE',
          details: 'Detailed info',
          createdAt: '01/01/2019',
          deviceId: 1,
          userId: 1
        }
      ]
    },
    auth: {
      resources: { audit: ['get'] }
    },
    getAuditByUserId: jest.fn(),
    getUser: jest.fn(),
    match: {
      params: {
        id: {
          id: 1
        }
      }
    }
  }

  it('should render correctly given the right props', () => {
    wrapper = shallow(<UserPage {...props} />)
    expect(toJson).toMatchSnapshot()
    expect(props.getUser.mock.calls.length).toBe(1)
  })

  it('Should change state on input', () => {
    wrapper = shallow(<UserPage {...props} />)
    wrapper.instance().handleInput({ target: { name: 'search', value: 'a' } })
    expect(wrapper.instance().state.search).toBe('a')
  })

  it('Should call the correct functions depending on props', () => {
    wrapper = shallow(<UserPage {...props} />)
    expect(props.getAuditByUserId.mock.calls.length).toBe(1)
  })

  it('Should not call getAuditByDeviceId if user does not have access', () => {
    const newProps = {
      ...props,
      auth: {
        resources: {
          audit: []
        }
      }
    }
    const newWrapper = shallow(<UserPage {...newProps} />)
    expect(newWrapper.instance().props.getAuditByUserId.mock.calls.length).toBe(0)

    expect(newWrapper.find('ErrorForbidden')).toBeDefined()
    expect(toJson(newWrapper)).toMatchSnapshot()
  })

  it('Should render differently when error is resieved', () => {
    wrapper = shallow(<UserPage {...props} />)
    wrapper.setProps({ ...props, user: { ...props.user, error: 'GET' } })
    wrapper.update()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
