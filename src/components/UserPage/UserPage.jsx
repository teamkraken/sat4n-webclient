import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { getUser } from '../../actions/userActions'
import { getAuditByUserId } from '../../actions/auditActions'
/* Components */
import { AuditList } from '../AuditList/AuditList'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import SearchBar from '../SearchBar/SearchBar'
import UserInfo from '../UserInfo/UserInfo'
import UserManualModal from '../UserManualModal/UserManualModal'
import UserPageManual from '../Manuals/UserPageManual'
/* Styles */
import '../../styles/Main.less'
import { withRouter } from '../../utils'

class UserPage extends Component {
  constructor () {
    super()
    this.state = {
      search: ''
    }
    this.handleInput = this.handleInput.bind(this)
  }

  componentDidMount () {
    const { auth, getAuditByUserId, getUser } = this.props
    const id = this.props.match.params.id

    getUser(id)
    if (auth.resources.audit.indexOf('get') !== -1) {
      getAuditByUserId(id)
    }
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { search } = this.state
    const { audit, auth, user } = this.props
    const { logs } = audit
    const { error } = user
    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2 className='text-light'>Edit User</h2>
          </span>
          <UserManualModal
            title='Edit User Information'
            CurrentPageManual={UserPageManual}
          />
          <div className='userContainer'>
            <UserInfo />
            {auth.resources.audit.indexOf('get') !== -1
              ? (
                <div className='listContainer'>
                  <h2 className='text-light'>Audit log</h2>
                  <SearchBar onInput={this.handleInput} />
                  <AuditList searchInput={search} logs={logs} />
                </div>
                )
              : null}
          </div>
        </div>
        )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ audit, auth, user }) => {
  return { audit, auth, user }
}

UserPage.propTypes = {
  getUser: PropTypes.func.isRequired,
  getAuditByUserId: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired,
  audit: PropTypes.shape({
    logs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        action: PropTypes.string.isRequired,
        details: PropTypes.string.isRequired,
        userId: PropTypes.number,
        createdAt: PropTypes.string.isRequired,
        deviceId: PropTypes.number
      }).isRequired
    ).isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { UserPage }
export default connect(mapStateToProps, { getUser, getAuditByUserId })(withRouter(UserPage))
