// Ports.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { Ports } from './Ports.jsx'

describe('Ports', () => {
  let wrapper
  const props = {
    auth: {
      resources: {
        ports: ['disable', 'enable', 'restart', 'update']
      }
    },
    device: {
      device: {
        ports: [
          { id: 1, name: 'Fa0/1', status: 'notconnected', vlan: 1, duplex: 'auto', speed: 'auto', type: '10/100BaseTX', lastUsed: 'never', inputErrors: 0, outputErrors: 0 },
          { id: 2, name: 'Fa0/2', status: 'connected', vlan: 20, duplex: 'a-full', speed: 'a-100', type: '10/100BaseTX', lastUsed: '00:00:01', inputErrors: 0, outputErrors: 0 }
        ]
      },
      error: '',
      portLoading: []
    },
    searchInput: '',
    clearErrors: jest.fn(),
    disableDevicePort: jest.fn(),
    enableDevicePort: jest.fn(),
    restartDevicePort: jest.fn(),
    updateLoading: jest.fn(),
    handleOpenModal: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<Ports {...props} />)
  })

  it('should render properly, given the correct props', () => {
    wrapper = shallow(<Ports {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should call restartDevicePort when restart button is clicked', () => {
    // Simulate restart click
    const deviceID = 1
    const portID = 1
    wrapper.setState({ deviceID, portID, method: 'restart' })
    wrapper.update()
    wrapper.instance().handleConfirm()

    expect(wrapper.instance().props.restartDevicePort).toHaveBeenCalledWith(deviceID, portID)
  })

  it('Should call disableDevicePort when disable button is clicked', () => {
    // Simulate restart click
    const deviceID = 1
    const portID = 1
    wrapper.setState({ deviceID, portID, method: 'disable' })
    wrapper.update()
    wrapper.instance().handleConfirm()

    expect(wrapper.instance().props.disableDevicePort).toHaveBeenCalledWith(deviceID, portID)
  })

  it('Should call enableDevicePort when enable button is clicked', () => {
    // Simulate restart click
    const deviceID = 1
    const portID = 1
    wrapper.setState({ deviceID, portID, method: 'enable' })
    wrapper.update()
    wrapper.instance().handleConfirm()

    expect(wrapper.instance().props.enableDevicePort).toHaveBeenCalledWith(deviceID, portID)
  })

  it('Should close modal when close button is clicked', () => {
    wrapper.instance().onClose = jest.fn()
    wrapper.update()

    wrapper.find('ConfirmModal').dive()
      .find('ButtonGrey').simulate('click')

    expect(wrapper.instance().onClose).toHaveBeenCalled()
  })

  it('Should render differently given different props', () => {
    wrapper.setProps({
      ...props,
      device: {
        device: {
          ports: []
        },
        error: ''
      }
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })
})
