import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, disableDevicePort, enableDevicePort, restartDevicePort, updateLoading } from '../../actions/deviceActions'
/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import ConfirmModal from '../ConfirmModal/ConfirmModal'
import PortsManual from '../Manuals/PortsManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class Ports extends Component {
  constructor (props) {
    super(props)

    this.state = {
      deviceID: 0,
      portID: 0,
      portName: '',
      method: '',
      showModal: false
    }
  }

  onClose () {
    const { clearErrors } = this.props

    this.setState({
      ...this.state,
      deviceID: 0,
      portID: 0,
      portName: '',
      method: '',
      showModal: false
    })

    clearErrors()
  }

  handleConfirm () {
    const { deviceID, portID, method } = this.state
    const { disableDevicePort, enableDevicePort, restartDevicePort, updateLoading } = this.props

    updateLoading(portID)

    switch (method) {
      case 'restart':
        restartDevicePort(deviceID, portID)
        break
      case 'disable':
        disableDevicePort(deviceID, portID)
        break
      case 'enable':
        enableDevicePort(deviceID, portID)
        break
    }
    this.onClose()
  }

  render () {
    const { auth, onOpenModal, searchInput } = this.props
    const { device, error, portLoading } = this.props.device
    const { ports } = device
    const portKeys = ['name', 'status', 'vlan', 'duplex', 'speed', 'type', 'lastUsed', 'inputErrors', 'outputErrors']
    const tableHeaders = ports.length ? portKeys : []
    const disableRestriction = auth.resources.ports.indexOf('disable') !== -1
    const enableRestriction = auth.resources.ports.indexOf('enable') !== -1
    const restartRestriction = auth.resources.ports.indexOf('restart') !== -1
    const updateRestriction = auth.resources.ports.indexOf('update') !== -1

    // Applying search filter
    /* istanbul ignore next */
    const portList = searchInput.length ? filterBySearchInput(ports, searchInput, portKeys) : ports

    return (
      <>
        <span>
          <h2>Ports</h2>
        </span>
        <UserManualModal
          title='Ports List User Information'
          CurrentPageManual={PortsManual}
        />
        <ul className='list listPorts listButtons'>
          <ListHeader headers={tableHeaders}>
            <span />
          </ListHeader>
          {portList.map((port) => {
            /* no need to test map functions */
            /* istanbul ignore next */
            return (
              <ListItem key={port.id}>
                <dl>
                  {portKeys.map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{port[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>

                <span>
                  {portLoading.indexOf(port.id) !== -1
                    ? <i className='fa fa-spinner fa-spin text-primary' />
                    : (
                      <>
                        {updateRestriction && !isNaN(port.vlan) && port.status !== 'disabled'
                          ? <i className='fa fa-edit' onClick={/* istanbul ignore next */() => onOpenModal(port)} id={port.id} />
                          : <i />}
                        {restartRestriction && !isNaN(port.vlan) && port.status !== 'disabled'
                          ? <i className='fa fa-sync text-danger' onClick={/* istanbul ignore next */() => this.setState({ showModal: true, deviceID: device.id, portID: port.id, portName: port.name, method: 'restart' })} id={port.id} />
                          : <i />}
                        {disableRestriction && !isNaN(port.vlan) && port.status !== 'disabled'
                          ? <i className='fa fa-toggle-on text-success' onClick={/* istanbul ignore next */() => this.setState({ showModal: true, deviceID: device.id, portID: port.id, portName: port.name, method: 'disable' })} id={port.id} />
                          : null}
                        {enableRestriction && !isNaN(port.vlan) && port.status === 'disabled'
                          ? <i className='fa fa-toggle-off text-danger' onClick={/* istanbul ignore next */() => this.setState({ showModal: true, deviceID: device.id, portID: port.id, portName: port.name, method: 'enable' })} id={port.id} />
                          : null}
                        {!disableRestriction && !enableRestriction
                          ? <i />
                          : null}
                      </>
                      )}
                </span>
              </ListItem>
            )
          })}
        </ul>
        <ConfirmModal
          title={`${this.state.method} ${this.state.portName}`}
          showModal={this.state.showModal || error.includes('RESTART')}
          onClose={() => this.onClose()}
          onConfirm={/* istanbul ignore next */() => this.handleConfirm()}
          errorMessage={error}
        />
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ auth, device }) => {
  return { auth, device }
}

Ports.propTypes = {
  clearErrors: PropTypes.func.isRequired,
  disableDevicePort: PropTypes.func.isRequired,
  enableDevicePort: PropTypes.func.isRequired,
  restartDevicePort: PropTypes.func.isRequired,
  updateLoading: PropTypes.func.isRequired,
  onOpenModal: PropTypes.func,
  searchInput: PropTypes.string,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }),
  device: PropTypes.shape({
    device: PropTypes.shape({
      ports: PropTypes.array.isRequired
    }).isRequired,
    error: PropTypes.string,
    portLoading: PropTypes.arrayOf(PropTypes.number)
  }).isRequired
}

Ports.defaultProps = {
  searchInput: '',
  device: {
    error: ''
  }
}

export { Ports }
export default connect(mapStateToProps, { clearErrors, disableDevicePort, enableDevicePort, restartDevicePort, updateLoading })(Ports)
