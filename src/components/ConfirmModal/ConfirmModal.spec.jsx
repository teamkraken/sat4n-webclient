// NavigationBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import ConfirmModal from './ConfirmModal.jsx'

describe('ConfirmModal', () => {
  const props = {
    title: 'testy',
    showModal: true,
    onClose: jest.fn(),
    onConfirm: jest.fn()
  }

  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<ConfirmModal {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
