import React, { Component } from 'react'
import ReactModal from 'react-modal'
import PropTypes from 'prop-types'

/* Components */
import GreenButton from '../ButtonGreen/ButtonGreen'
import GreyButton from '../ButtonGrey/ButtonGrey'

class ConfirmModal extends Component {
  render () {
    const { title, showModal, onClose, onConfirm, errorMessage } = this.props

    return (
      <ReactModal
        isOpen={showModal}
        onRequestClose={onClose}
        contentLabel={title}
        className='modal-dialog'
        overlayClassName='modal-backdrop'
        style={{
          overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
        }}
      >
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title' style={{ textTransform: 'capitalize' }}>{title}</h5>
            <h5 className='text-danger'>{errorMessage}</h5>
          </div>
          <div className='modal-footer'>
            <GreyButton label='Cancel' onClick={onClose} />
            <GreenButton onClick={onConfirm} label='Confirm' />
          </div>
        </div>
      </ReactModal>
    )
  }
}

ConfirmModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired
}

export default ConfirmModal
