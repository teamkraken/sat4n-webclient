// UsersList.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { UsersList } from './UsersList.jsx'

describe('UsersList', () => {
  let wrapper
  const props = {
    user: {
      users: [
        { id: 1, username: 'admin', role: { id: 1, name: 'Administrators' } },
        { id: 1, username: 'user1', role: { id: 2, name: 'Role 2' } },
        { id: 1, username: 'user2', role: { id: 2, name: 'Role 1' } }
      ],
      error: ''
    },
    auth: {
      resources: {
        users: ['delete']
      }
    },
    deleteUser: jest.fn(),
    clearErrors: jest.fn(),
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<UsersList {...props} />)
  })

  it('should render properly, given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should call the proper actions given a click', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    }

    wrapper.setState({ showModal: true })
    wrapper.find('ConfirmModal').simulate('click')
    expect(wrapper.instance().state.showModal === false)

    wrapper.setState({ showModal: true })
    wrapper.instance().handleDelete()
    mockEvent.preventDefault()
    expect(wrapper.instance().state.showModal === false)
  })

  it('renders the list in full', () => {
    wrapper.setState({ showModal: true })

    wrapper.find('ConfirmModal').simulate('click')
    wrapper.instance().handleDelete()
    expect(wrapper.instance().state.showModal === false)
    expect(props.deleteUser.mock.calls.length).toBe(1)
  })

  it('Should close modal', () => {
    wrapper.instance().onClose = jest.fn()
    wrapper.update()
    wrapper.find('ConfirmModal').dive()
      .find('ButtonGrey')
      .simulate('click')

    expect(wrapper.instance().onClose).toHaveBeenCalled()
  })

  it('Should render differently when user does not have authodization to delete', () => {
    wrapper.setProps({ ...props, auth: { resources: { users: [] } } })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('Should change sort order', () => {
    // Expected list sorted descending by username
    const expectedList = [
      { id: 1, username: 'user2', role: { id: 2, name: 'Role 1' } },
      { id: 1, username: 'user1', role: { id: 2, name: 'Role 2' } },
      { id: 1, username: 'admin', role: { id: 1, name: 'Administrators' } }
    ]

    wrapper.instance().sortByColumn('username')
    expect(wrapper.instance().state.sort.order === 'up')
    expect(wrapper.instance().props.user.users).toEqual(expectedList)
  })

  it('Should change sort column', () => {
    // Expected list sorted by description
    const expectedList = [
      { id: 1, username: 'admin', role: { id: 1, name: 'Administrators' } },
      { id: 1, username: 'user2', role: { id: 2, name: 'Role 1' } },
      { id: 1, username: 'user1', role: { id: 2, name: 'Role 2' } }
    ]

    wrapper.instance().sortByColumn('role')
    expect(wrapper.instance().props.user.users).toEqual(expectedList)
  })
})
