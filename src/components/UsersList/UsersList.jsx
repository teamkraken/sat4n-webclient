import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, deleteUser } from '../../actions/userActions'
/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import ConfirmModal from '../ConfirmModal/ConfirmModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class UsersList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      userID: null,
      userName: '',
      sort: {
        column: 'username',
        order: 'down'
      }
    }
  }

  /**
   * Called when the delete button for a user is clicked
   * Calls the deleteUser action
   * @param {number} id Id of a user
   * @memberof UsersList
   */
  handleDelete () {
    const { deleteUser } = this.props
    deleteUser(this.state.userID)
    this.setState({
      showModal: false,
      userID: null,
      userName: ''
    })
  }

  sortUsers (array) {
    const { sort } = this.state
    const { column, order } = sort

    return array.sort((a, b) => {
      const left = column !== 'role' ? a[column].toLowerCase() : a[column].name.toLowerCase()
      const right = column !== 'role' ? b[column].toLowerCase() : b[column].name.toLowerCase()

      if (left < right) {
        return order === 'down' ? -1 : 1
      }

      if (left > right) {
        return order === 'down' ? 1 : -1
      }

      return 0
    })
  }

  sortByColumn (newColumn) {
    const { sort } = this.state
    const { column, order } = sort

    this.setState({
      sort: {
        column: newColumn,
        order: column === newColumn && order === 'down' ? 'up' : 'down'
      }
    })
  }

  /* no need to test onClose functions */
  /* istanbul ignore next */
  onClose () {
    const { clearErrors } = this.props

    clearErrors()
    this.setState({
      showModal: false,
      userID: null,
      userName: ''
    })
  }

  /**
   * Renders the components jsx
   * @returns Unsorted list where each list item is an instance of ListItem
   * @memberof UsersList
   */
  render () {
    const { sort } = this.state
    const { auth, user, searchInput } = this.props
    const { users, error } = user
    const includedKeys = ['username', 'role']
    const tableHeaders = users.length ? includedKeys : []
    const deleteRestriction = auth.resources.users.indexOf('delete') !== -1

    // Applying search filter
    /* istanbul ignore next */
    const userList = searchInput.length ? filterBySearchInput(this.sortUsers(users), searchInput, ['username']) : this.sortUsers(users)

    return (
      <>
        <ul className='list listUsers listButtons'>
          <ListHeader headers={tableHeaders} sort={sort} sortByColumn={(column) => { this.sortByColumn(column) }}>
            {deleteRestriction ? <span /> : null}
          </ListHeader>

          {userList.map((user) => {
            /* no need to test map functions */
            /* istanbul ignore next */
            return (
              <ListItem key={user.id}>
                <dl>
                  {includedKeys.map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt>
                        <dd>
                          <Link to={{ pathname: `/users/${user.id}` }}>
                            {key === 'role'
                              ? user[key] ? user[key].name : null
                              : user[key]}
                          </Link>
                        </dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
                {deleteRestriction
                  ? <span><i className='fa fa-trash-alt text-danger' onClick={() => this.setState({ showModal: true, userID: user.id, userName: user.username })} id={user.id} /></span>
                  : <span />}
              </ListItem>
            )
          })}
        </ul>

        <ConfirmModal
          title={`Delete user ${this.state.userName}`}
          showModal={this.state.showModal || error.includes('DELETE')}
          onClose={() => this.onClose()}
          onConfirm={/* istanbul ignore next */ () => this.handleDelete()}
          errorMessage={error}
        />
      </>
    )
  }
}

/**
 * Maps the redux store state to the components props
 * @param {Object} user The user object of the redux store state
 * @returns The user object of the redux store state
 */
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ user, auth }) => {
  return { user, auth }
}

UsersList.propTypes = {
  searchInput: PropTypes.string,
  deleteUser: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
  user: PropTypes.shape({
    users: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { UsersList }
export default connect(mapStateToProps, { clearErrors, deleteUser })(UsersList)
