import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'

/* Actions */
import { addDevice } from '../../actions/deviceActions'
/* Components */
import FormInput from '../FormInput/FormInput'
import GreenButton from '../ButtonGreen/ButtonGreen'
import GreyButton from '../ButtonGrey/ButtonGrey'
/* Styles */

class AddDeviceForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      ip: '',
      errors: {
        ipError: ''
      }
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleClose () {
    const { onCloseModal } = this.props
    this.setState({ ip: '', errors: { ipError: '' } })
    onCloseModal('close')
  }

  handleSubmit (e) {
    e.preventDefault()
    const { addDevice, onCloseModal } = this.props
    const { ip } = this.state
    const valid = this.validate(ip)

    if (valid) {
      addDevice({ ip })
      onCloseModal('submit')
      this.setState({
        ip: '',
        errors: { ipError: '' }
      })
    }
  }

  handleChange (e) {
    const { name, value } = e.target
    return this.setState({ [name]: value })
  }

  validate (ip) {
    const { errors } = this.state
    const error = {}

    if (ip === '') {
      error.ipError = 'ip is required'
    }

    if (Object.keys(error).length > 0) {
      this.setState({ errors: { ...errors, ...error } })

      return false
    }

    return true
  }

  render () {
    const { ip } = this.state
    const { errors } = this.state
    const { onOpenModal, visible, device } = this.props
    const { error } = device

    return visible
      ? (
        <>
          <GreenButton onClick={onOpenModal} label='Add Device' />

          <ReactModal
            isOpen={this.props.showModal}
            onClose={this.handleClose}
            contentLabel='Add Device'
            className='modal-dialog'
            overlayClassName='modal-backdrop'
            style={{
              overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
            }}
          >
            <div className='modal-content'>
              <div className='modal-header'>
                <h5 className='modal-title' id='exampleModalLabel'>Add Device</h5>
                <h5 className='text-danger'>{error}</h5>
              </div>
              <div className='modal-body'>
                <form className='' onSubmit={(e) => { this.handleSubmit(e) }}>
                  <div className='form-group'>

                    <FormInput
                      id='ip'
                      name='ip'
                      type='text'
                      value={ip}
                      label='IP Address'
                      error={errors.ipError}
                      onChange={(e) => this.handleChange(e)}
                    />

                  </div>
                  <div className='modal-footer'>
                    <GreyButton label='Close' onClick={this.handleClose} />
                    <GreenButton label='Add' type='submit' />
                  </div>
                </form>
              </div>
            </div>
          </ReactModal>
        </>
        )
      : null
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

AddDeviceForm.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onOpenModal: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  addDevice: PropTypes.func.isRequired,
  device: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired
}

export { AddDeviceForm }
export default connect(mapStateToProps, { addDevice })(AddDeviceForm)
