// AddDeviceForm.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { AddDeviceForm } from './AddDeviceForm'

// uncovered lines : 34,63,97
describe('AddDeviceForm', () => {
  let wrapper
  const props = {
    device: {
      error: ''
    },
    showModal: false,
    visible: true,
    addDevice: jest.fn(),
    onCloseModal: jest.fn(),
    onOpenModal: jest.fn()
  }

  const e = {
    preventDefault: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<AddDeviceForm {...props} />)
  })

  it('Should render correctly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when visible is false', () => {
    wrapper.setProps({ ...props, visible: false })
    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })

  it('Should call the proper actions given a successful submit call', () => {
    wrapper.find('FormInput[id="ip"]').dive()
      .find('input[id="ip"]')
      .simulate('change', { target: { name: 'ip', value: '10.10.10' } })
    expect(wrapper.state().ip).toEqual('10.10.10')

    wrapper.find('form').simulate('submit', e)
    wrapper.setProps({ showModal: false })

    expect(props.addDevice.mock.calls.length).toBe(1)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('it should call the proper actions given a unsuccessful submit call', () => {
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('AddButton[label="Add"]').dive()
      .find('button[type="submit"]')
      .simulate('click')
    wrapper.find('form').simulate('submit', e)

    expect(wrapper.state().errors).toEqual({
      ipError: 'ip is required'
    })

    expect(props.addDevice.mock.calls.length).toBe(0)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('Should invoke onCloseModal', () => {
    wrapper.instance().handleClose()
    expect(props.onCloseModal.mock.calls.length).toBe(1)
    wrapper.instance().handleSubmit(e)
    expect(props.onCloseModal.mock.calls.length).toBe(1)
  })
})
