// UsersList.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { RolesList } from './RolesList.jsx'

describe('RolesList', () => {
  let wrapper
  const props = {
    role: {
      roles: [
        { id: 1, name: 'administrators', description: 'cba' },
        { id: 2, name: 'users', description: 'abc' },
        { id: 3, name: 'zodd', description: 'bca' }
      ],
      error: ''
    },
    auth: {
      resources: {
        roles: ['delete']
      },
      error: ''
    },
    deleteRole: jest.fn(),
    clearErrors: jest.fn(),
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<RolesList {...props} />)
  })

  it('should render properly, given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should handle delete/cancel clicks properly', () => {
    expect(props.deleteRole.mock.calls.length).toBe(0)

    expect(wrapper.instance().state).toEqual({
      showModal: false,
      roleID: null,
      roleName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    // Perform click on delete button for first row
    wrapper.find('ListItem').at('0').dive().find('i').simulate('click')
    expect(wrapper.instance().state).toEqual({
      showModal: true,
      roleID: 1,
      roleName: 'administrators',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    // Perform click on cancel button in delete modal
    wrapper.find('ConfirmModal').dive()
      .find('ButtonGrey[label="Cancel"]').dive()
      .find('button')
      .simulate('click')

    expect(wrapper.instance().state).toEqual({
      showModal: false,
      roleID: null,
      roleName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    expect(props.deleteRole.mock.calls.length).toBe(0)
  })

  it('should handle delete/confirm clicks properly', () => {
    expect(props.deleteRole.mock.calls.length).toBe(0)

    expect(wrapper.instance().state).toEqual({
      showModal: false,
      roleID: null,
      roleName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    // Perform click on delete button for first row
    wrapper.find('ListItem').at(1).dive().find('i').simulate('click')
    expect(wrapper.instance().state).toEqual({
      showModal: true,
      roleID: 2,
      roleName: 'users',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    // Perform click on confirm button in delete modal
    wrapper.find('ConfirmModal').dive()
      .find('AddButton[label="Confirm"]').dive()
      .find('button')
      .simulate('click')

    expect(wrapper.instance().state).toEqual({
      showModal: false,
      roleID: null,
      roleName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    })

    expect(props.deleteRole.mock.calls.length).toBe(1)
  })

  it('Should render differently given different props', () => {
    wrapper.setProps({
      ...props,
      auth: {
        resources: {
          roles: []
        }
      },
      role: {
        roles: [],
        error: ''
      }
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('Should change sort order', () => {
    // Expected list sorted descending by name
    const expectedList = [
      { id: 3, name: 'zodd', description: 'bca' },
      { id: 2, name: 'users', description: 'abc' },
      { id: 1, name: 'administrators', description: 'cba' }
    ]

    wrapper.instance().sortByColumn('name')
    expect(wrapper.instance().state.sort.order === 'up')
    expect(wrapper.instance().props.role.roles).toEqual(expectedList)
  })

  it('Should change sort column', () => {
    // Expected list sorted by description
    const expectedList = [
      { id: 2, name: 'users', description: 'abc' },
      { id: 3, name: 'zodd', description: 'bca' },
      { id: 1, name: 'administrators', description: 'cba' }
    ]

    wrapper.instance().sortByColumn('description')
    expect(wrapper.instance().props.role.roles).toEqual(expectedList)
  })
})
