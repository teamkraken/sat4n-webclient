import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, deleteRole } from '../../actions/roleActions'
/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import ConfirmModal from '../ConfirmModal/ConfirmModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class RolesList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      roleID: null,
      roleName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    }
  }

  /**
   * Called when the delete button for a role is clicked
   * Calls the deleteRole action
   * @param {number} id Id of a role
   * @memberof RolesList
   */
  handleDelete () {
    const { deleteRole } = this.props

    deleteRole(this.state.roleID)
    this.setState({
      showModal: false,
      roleID: null,
      roleName: ''
    })
  }

  sortRoles (array) {
    const { sort } = this.state
    const { column, order } = sort

    return array.sort((a, b) => {
      const left = a[column].toLowerCase()
      const right = b[column].toLowerCase()

      if (left < right) {
        return order === 'down' ? -1 : 1
      }

      if (left > right) {
        return order === 'down' ? 1 : -1
      }

      return 0
    })
  }

  sortByColumn (newColumn) {
    const { sort } = this.state
    const { column, order } = sort

    this.setState({
      sort: {
        column: newColumn,
        order: column === newColumn && order === 'down' ? 'up' : 'down'
      }
    })
  }

  /* no need to test onClose functions */
  /* istanbul ignore next */
  onClose () {
    const { clearErrors } = this.props

    clearErrors()
    this.setState({
      showModal: false,
      roleID: null,
      roleName: ''
    })
  }

  /**
   * Renders the components jsx
   * @returns Unsorted list where each list item is an instance of ListItem
   * @memberof RolesList
   */
  render () {
    const { sort } = this.state
    const { role, auth, searchInput } = this.props
    const { roles, error } = role
    const includedKeys = ['name', 'description']
    const tableHeaders = roles.length ? includedKeys : []
    const deleteRestriction = auth.resources.roles.indexOf('delete') !== -1

    // Applying search filter
    /* istanbul ignore next */
    const roleList = searchInput.length ? filterBySearchInput(this.sortRoles(roles), searchInput, includedKeys) : this.sortRoles(roles)

    return (
      <>
        <ul className='list listRoles listButtons'>
          <ListHeader headers={tableHeaders} sort={sort} sortByColumn={(column) => { this.sortByColumn(column) }}>
            {deleteRestriction ? <span /> : null}
          </ListHeader>

          {roleList.map((role) => {
            /* no need to test map functions */
            /* istanbul ignore next */
            return (
              <ListItem key={role.id}>
                <dl>
                  {includedKeys.map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt>
                        <dd>
                          <Link to={{ pathname: `/roles/${role.id}` }}>
                            <span>{role[key]}</span>
                          </Link>
                        </dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
                {deleteRestriction
                  ? <span><i className='fa fa-trash-alt text-danger' onClick={() => this.setState({ showModal: true, roleID: role.id, roleName: role.name })} id={role.id} /></span>
                  : <span />}
              </ListItem>
            )
          })}
        </ul>

        <ConfirmModal
          title={`Delete role ${this.state.roleName}`}
          showModal={this.state.showModal || error.includes('DELETE')}
          onClose={() => this.onClose()}
          onConfirm={() => this.handleDelete()}
          errorMessage={error}
        />
      </>
    )
  }
}

/**
 * Maps the redux store state to the components props
 * @param {Object} role The role object of the redux store state
 * @returns The role object of the redux store state
 */
/* istanbul ignore next */
const mapStateToProps = ({ role, auth }) => {
  return { role, auth }
}

RolesList.propTypes = {
  searchInput: PropTypes.string,
  deleteRole: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
  role: PropTypes.shape({
    roles: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { RolesList }
export default connect(mapStateToProps, { clearErrors, deleteRole })(RolesList)
