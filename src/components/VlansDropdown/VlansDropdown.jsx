import React, { Component } from 'react'
import PropTypes from 'prop-types'

class VlansDropdown extends Component {
  render () {
    const { id, vlans, onChange } = this.props
    console.log(vlans)

    return (
      <>
        <select
          className='form-control'
          type='select'
          name='vlan'
          id='vlan'
          value={id}
          onChange={onChange}
        >
          <option value='0'>Select vlan</option>
          {vlans.map(/* istanbul ignore next */(vlan) => {
            return <option key={vlan.id} value={vlan.id}>{vlan.id} ({vlan.name})</option>
          })}
        </select>
      </>
    )
  }
}

VlansDropdown.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired,
  onChange: PropTypes.func.isRequired,
  vlans: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired
    }).isRequired
  )
}

export { VlansDropdown }
export default VlansDropdown
