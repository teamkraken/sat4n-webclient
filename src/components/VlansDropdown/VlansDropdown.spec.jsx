// VlansDropdown.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { VlansDropdown } from './VlansDropdown.jsx'

describe('VlansDropdown', () => {
  const props = {
    id: 1,
    vlans: [],
    onChange: jest.fn()
  }

  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<VlansDropdown {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
