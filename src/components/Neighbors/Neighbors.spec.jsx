// Neighbors.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { Neighbors } from './Neighbors.jsx'

describe('Neighbors', () => {
  let wrapper
  const props = {
    device: {
      device: {
        neighbors: []
      }
    },
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<Neighbors {...props} />)
  })

  it('should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render the list in full', () => {
    wrapper.setProps({
      searchInput: '',
      device: {
        device: {
          neighbors: [
            { device: 'SEP000000000000', localPort: 'Fa0/1', holdTime: '100', capability: 'H P M', platform: 'IP Phone', remotePort: 'Port 1' },
            { device: 'SEP000000000001', localPort: 'Ga0/1', holdTime: '100', capability: 'H P M', platform: 'IP Phone', remotePort: 'Port 1' }
          ]
        }
      }
    })

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
