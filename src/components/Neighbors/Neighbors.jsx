import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import UserManualModal from '../UserManualModal/UserManualModal'
import NeighborsManual from '../Manuals/NeighborsManual'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class Neighbors extends Component {
  render () {
    const { device, searchInput } = this.props
    const { neighbors } = device.device
    const tableHeaders = neighbors.length ? Object.keys(neighbors[0]) : []

    // Applying search filter
    /* istanbul ignore next */
    const neighborList = searchInput.length ? filterBySearchInput(neighbors, searchInput, tableHeaders) : neighbors

    return (
      <>
        <span>
          <h2>Neighbors</h2>
        </span>
        <UserManualModal
          title='Neighbors User Information'
          CurrentPageManual={NeighborsManual}
        />
        <ul className='list listNeighbors'>
          <ListHeader headers={tableHeaders} />
          {neighborList.map(/* istanbul ignore next */(device) => {
            return (
              <ListItem key={device.device}>
                <dl>
                  {Object.keys(device).map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{device[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
              </ListItem>
            )
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

Neighbors.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      neighbors: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}

Neighbors.defaultProps = {
  searchInput: ''
}

export { Neighbors }
export default connect(mapStateToProps)(Neighbors)
