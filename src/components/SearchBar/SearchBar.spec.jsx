// SearchBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import SearchBar from './SearchBar'

describe('SearchBar', () => {
  const props = {
    onInput: jest.fn()
  }
  it('should render correctly given correct props', () => {
    const wrapper = shallow(<SearchBar {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
