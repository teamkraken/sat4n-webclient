import React from 'react'
import PropTypes from 'prop-types'

const SearchBar = ({ onInput }) => {
  return (
    <div className='form-group searchBar'>
      <input className='form-control' type='text' placeholder='Search here..' onInput={(e) => onInput(e)} />
    </div>
  )
}

SearchBar.propTypes = {
  onInput: PropTypes.func.isRequired
}

export default SearchBar
