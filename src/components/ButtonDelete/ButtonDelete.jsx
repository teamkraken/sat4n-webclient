import React from 'react'
import PropTypes from 'prop-types'

const ButtonDelete = ({ onClick, id }) => {
  return (
    <span className='col-md deletebtn'>
      <i className='fa fa-trash-alt text-danger' onClick={(id) => onClick(id)} />
    </span>
  )
}

ButtonDelete.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired
}

export default ButtonDelete
