// ButtonDelete.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import ButtonDelete from './ButtonDelete'

describe('ButtonDelete', () => {
  let wrapper
  const props = {
    onClick: jest.fn(),
    id: 1
  }

  beforeEach(() => {
    wrapper = shallow(<ButtonDelete {...props} />)
  })

  it('Should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should be able to click the delete button', () => {
    wrapper.find('i').simulate('click')
    expect(props.onClick.mock.calls.length).toBe(1)
  })
})
