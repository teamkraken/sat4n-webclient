// Deviceinfo.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { RoleInfo } from './RoleInfo.jsx'

describe('RoleInfo', () => {
  let wrapper
  const props = {
    role: {
      id: '1',
      name: 'administrators',
      resources: [{ id: 1, name: 'users_add', enabled: true }]
    },
    errors: {},
    error: '',
    resources: {
      roles: ['update']
    },
    updateRole: jest.fn(),
    onToggle: jest.fn(),
    onChange: jest.fn()
  }

  const e = {
    preventDefault: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<RoleInfo {... props} />)
  })

  it('should render correctly, given correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently given different props', () => {
    wrapper.setProps({
      ...props,
      resources: {
        roles: []
      }
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('should accept valid forms', () => {
    expect(wrapper.instance().props.role.resources).toEqual([{ id: 1, name: 'users_add', enabled: true }])
    wrapper.find('#users_add').simulate('change', { target: { name: 'users_add', value: false } })

    expect(props.onToggle.mock.calls.length).toBe(1)

    wrapper.find('form').simulate('submit', e)
    expect(wrapper.instance().props.updateRole.mock.calls.length).toBe(1)
  })

  it('Should call onChange function when description is changed', () => {
    wrapper.find('input[name="description"]').simulate('change', e)

    expect(wrapper.instance().props.onChange.mock.calls.length).toBe(1)
  })
})
