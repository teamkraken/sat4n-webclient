import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navigate } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { onChange, onToggle, updateRole } from '../../actions/roleActions'
/* Styles */
import '../../styles/Main.less'

class RoleInfo extends Component {
  constructor (props) {
    super(props)

    this.input = React.createRef()
    this.state = {
      redirect: false,
      errors: {}
    }
  }

  onSubmit (e) {
    const { updateRole, role } = this.props
    e.preventDefault()

    updateRole(role)
    this.setState({ redirect: true })
  }

  render () {
    const { errors, redirect } = this.state
    const { onChange, onToggle, role, error, resources } = this.props
    const updateRestriction = resources.roles.indexOf('update') !== -1
    const endpoints = ['audit', 'devices', 'users', 'roles', 'ports']

    return redirect
      ? <Navigate to='/roles' />
      : (
        <div className='roleContainer'>
          <form onSubmit={(e) => { this.onSubmit(e) }}>
            <div className='form-group'>
              <label className='control-label' htmlFor='name'>Name</label>
              <input
                className='form-control'
                id='name'
                name='name'
                type='text'
                value={role.name}
                placeholder='Role name'
                disabled
              />

              <div className='text-danger'>{errors.name}</div>

              <label className='control-label' htmlFor='description'>Description</label>
              <input
                className='form-control'
                id='description'
                name='description'
                type='text'
                value={role.description}
                placeholder='Description'
                disabled={!updateRestriction}
                onChange={(e) => onChange(e)}
              />

              <div className='resourceCheckBoxes'>
                {endpoints.map(endpoint => {
                  return (
                    <div key={endpoint}>
                      <span className='checkBoxContinerLabel'>{`${endpoint}:`}</span>
                      <dl>
                        {role.resources.map((resource) => {
                          const { id, name } = resource
                          return resource.name.includes(endpoint)
                            ? (
                              <React.Fragment key={id}>
                                <dt><label className='control-label' htmlFor={resource.name}>{name.split('_')[1]}</label></dt>
                                <dd>
                                  <input
                                    className='control-label'
                                    id={resource.name}
                                    name={resource.name}
                                    type='checkbox'
                                    checked={resource.enabled}
                                    value={resource.enabled}
                                    disabled={!updateRestriction}
                                    onChange={(e) => {
                                      onToggle({
                                        target: {
                                          name: e.target.name,
                                          value: !resource.enabled
                                        }
                                      })
                                    }}
                                  />
                                </dd>
                              </React.Fragment>
                              )
                            : null
                        })}
                      </dl>
                    </div>
                  )
                })}
              </div>
            </div>

            <span className='d-block text-danger'>{error}</span>
            {updateRestriction
              ? <button type='submit' className='btn btn-success saveButton'>Save</button>
              : null}
          </form>
        </div>
        )
  }
}

/* istanbul ignore next */
const mapStateToProps = ({ role, auth }) => {
  return {
    role: role.role,
    error: role.error,
    resources: auth.resources
  }
}

RoleInfo.propTypes = {
  onChange: PropTypes.func.isRequired,
  onToggle: PropTypes.func.isRequired,
  updateRole: PropTypes.func.isRequired,
  role: PropTypes.shape({
    name: PropTypes.string.isRequired,
    resources: PropTypes.array.isRequired
  }).isRequired,
  resources: PropTypes.object.isRequired
}

export { RoleInfo }
export default connect(mapStateToProps, { onChange, onToggle, updateRole })(RoleInfo)
