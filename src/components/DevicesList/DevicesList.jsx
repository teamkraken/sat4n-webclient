import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, removeDevice } from '../../actions/deviceActions'
/* Components */
import { ListItem } from '../ListItem/ListItem'
import { ListHeader } from '../ListHeader/ListHeader'
import ConfirmModal from '../ConfirmModal/ConfirmModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class DevicesList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      deviceID: null,
      deviceName: '',
      sort: {
        column: 'name',
        order: 'down'
      }
    }
  }

  /**
   * Called when the delete button for a device is clicked
   * Calls the removeDevice action
   * @param {number} id Id of a device
   * @memberof DevicesList
   */
  handleDelete () {
    const { removeDevice } = this.props

    removeDevice(this.state.deviceID)
    this.setState({
      showModal: false,
      deviceID: null,
      deviceName: ''
    })
  }

  sortDevices (array) {
    const { sort } = this.state
    const { column, order } = sort

    return array.sort((a, b) => {
      // If column is IP pad number with zeros
      const left = column !== 'ip' ? a[column].toLowerCase() : a[column].toLowerCase().split('.').map((num) => `000${num}`.slice(-3)).join('')
      const right = column !== 'ip' ? b[column].toLowerCase() : b[column].toLowerCase().split('.').map((num) => `000${num}`.slice(-3)).join('')

      if (left < right) {
        return order === 'down' ? -1 : 1
      }

      if (left > right) {
        return order === 'down' ? 1 : -1
      }

      return 0
    })
  }

  sortByColumn (newColumn) {
    const { sort } = this.state
    const { column, order } = sort

    this.setState({
      sort: {
        column: newColumn,
        order: column === newColumn && order === 'down' ? 'up' : 'down'
      }
    })
  }

  /* no need to test onClose functions */
  /* istanbul ignore next */
  onClose () {
    const { clearErrors } = this.props

    clearErrors()
    this.setState({
      showModal: false,
      deviceID: null,
      deviceName: ''
    })
  }

  /**
   * Renders the components jsx
   * @returns Unsorted list where each list item is an instance of ListItem
   * @memberof DevicesList
   */
  render () {
    const { sort } = this.state
    const { auth, device, searchInput } = this.props
    const { devices, error } = device
    const includedKeys = ['name', 'ip', 'location', 'model']
    const tableHeaders = devices.length ? includedKeys : []
    const deleteRestriction = auth.resources.devices.indexOf('delete') !== -1

    // Applying search filter
    /* istanbul ignore next */
    const deviceList = searchInput.length ? filterBySearchInput(this.sortDevices(devices), searchInput, includedKeys) : this.sortDevices(devices)

    return (
      <>
        <ul className='list listDevices listButtons'>
          <ListHeader headers={tableHeaders} sort={sort} sortByColumn={(column) => { this.sortByColumn(column) }}>
            {deleteRestriction ? <span /> : null}
          </ListHeader>

          {deviceList.map((device) => {
            /* no need to test map functions */
            /* istanbul ignore next */
            return (
              <ListItem key={device.id}>
                <dl>
                  {includedKeys.map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt>
                        <dd>
                          <Link to={{ pathname: `/devices/${device.id}` }}>
                            {device[key]}
                          </Link>
                        </dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
                {deleteRestriction
                  ? <span><i className='fa fa-trash-alt text-danger' onClick={() => this.setState({ showModal: true, deviceID: device.id, deviceName: device.name })} id={device.id} /></span>
                  : <span />}
              </ListItem>
            )
          })}
        </ul>

        <ConfirmModal
          title={`Delete device ${this.state.deviceName}`}
          showModal={this.state.showModal || error.includes('DELETE')}
          onClose={() => this.onClose()}
          onConfirm={/* istanbul ignore next */() => this.handleDelete()}
          errorMessage={error}
        />
      </>
    )
  }
}

/**
 * Maps the redux store state to the components props
 * @param {Object} device The device object of the redux store state
 * @returns The device object of the redux store state
 */
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device, auth }) => {
  return { device, auth }
}

DevicesList.propTypes = {
  searchInput: PropTypes.string,
  removeDevice: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired,
  device: PropTypes.shape({
    devices: PropTypes.array.isRequired,
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { DevicesList }
export default connect(mapStateToProps, { clearErrors, removeDevice })(DevicesList)
