// DevicesList.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DevicesList } from '../DevicesList/DevicesList.jsx'

describe('DevicesList', () => {
  let wrapper
  const props = {
    device: {
      devices: [
        { id: 8, name: 'hostName2', ip: '10.10.10.1' },
        { id: 10, name: 'hostName1', ip: '10.30.10.1' },
        { id: 11, name: 'hostName3', ip: '10.4.10.1' }
      ],
      error: ''
    },
    auth: {
      resources: {
        devices: ['delete']
      }
    },
    removeDevice: jest.fn(),
    clearErrors: jest.fn(),
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<DevicesList {...props} />)
  })

  it('should render correctly, given correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should call the proper actions given a click', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    }

    wrapper.setState({ showModal: true })
    wrapper.find('ConfirmModal').simulate('click')
    expect(wrapper.instance().state.showModal === false)

    wrapper.setState({ showModal: true })
    wrapper.instance().handleDelete()
    mockEvent.preventDefault()
    expect(wrapper.instance().state.showModal === false)
  })

  it('renders the list in full', () => {
    wrapper.setState({ showModal: true })

    wrapper.find('ConfirmModal').simulate('click')
    wrapper.instance().handleDelete()
    expect(wrapper.instance().state.showModal === false)
    expect(props.removeDevice.mock.calls.length).toBe(1)
  })

  it('Should close modal', () => {
    wrapper.instance().onClose = jest.fn()
    wrapper.update()
    wrapper.find('ConfirmModal').dive()
      .find('ButtonGrey')
      .simulate('click')

    expect(wrapper.instance().onClose).toHaveBeenCalled()
  })

  it('Should render differently when user does not have authodization to delete', () => {
    wrapper.setProps({ ...props, auth: { resources: { devices: [] } } })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('Should change sort order', () => {
    // Expected list sorted descending by name
    const expectedList = [
      { id: 11, name: 'hostName3', ip: '10.4.10.1' },
      { id: 8, name: 'hostName2', ip: '10.10.10.1' },
      { id: 10, name: 'hostName1', ip: '10.30.10.1' }
    ]

    wrapper.instance().sortByColumn('name')
    expect(wrapper.instance().state.sort.order === 'up')
    expect(wrapper.instance().props.device.devices).toEqual(expectedList)
  })

  it('Should change sort column', () => {
    // Expected list sorted by ip
    const expectedList = [
      { id: 11, name: 'hostName3', ip: '10.4.10.1' },
      { id: 8, name: 'hostName2', ip: '10.10.10.1' },
      { id: 10, name: 'hostName1', ip: '10.30.10.1' }
    ]

    wrapper.instance().sortByColumn('ip')
    expect(wrapper.instance().props.device.devices).toEqual(expectedList)
  })
})
