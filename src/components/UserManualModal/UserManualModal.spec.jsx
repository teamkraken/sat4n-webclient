// UserManualModal.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import UserManualModal from './UserManualModal'

describe('UserManualModal', () => {
  let wrapper
  const props = {
    title: 'testy',
    CurrentPageManual: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<UserManualModal {...props} />)
  })

  it('should render correctly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should change state when modal is closed or opened', () => {
    wrapper.instance().openManualModal()
    expect(wrapper.instance().state.showManualModal).toEqual(true)

    wrapper.instance().closeManualModal()
    expect(wrapper.instance().state.showManualModal).toEqual(false)
  })
})
