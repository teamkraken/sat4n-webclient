import React, { Component } from 'react'
import ReactModal from 'react-modal'
import PropTypes from 'prop-types'

/* Components */
import GreyButton from '../ButtonGrey/ButtonGrey'

/* styling */
import '../../styles/Main.less'

/**
 * This component renders a modal window with the user manual for the current page being viewed.
 * @props title: A string sent to the component for the title of the modal
 * @component CurrentPageManual: A dumb component rendering a text manual for the current page
 * Use case example:
    <UserManualModal
      title='Device Page User Information'
      CurrentPageManual={DevicesPageManual} />
 */
class UserManualModal extends Component {
  constructor () {
    super()
    this.state = {
      showManualModal: false
    }
  }

  /**
   * Opens the modal window by changing the state of showManualModal to true
   */
  openManualModal () {
    this.setState({ showManualModal: true })
  }

  /**
   * Closes the modal window by changing the state of showManualModal to false
   */
  closeManualModal () {
    this.setState({ showManualModal: false })
  }

  render () {
    const { title, CurrentPageManual } = this.props

    return (
      <>
        <span className='questionMark'>
          <i className='far fa-question-circle' onClick={this.openManualModal.bind(this)} />
        </span>
        <ReactModal
          isOpen={this.state.showManualModal}
          onRequestClose={this.closeManualModal.bind(this)}
          contentLabel={title}
          className='modal-size modal-dialog'
          overlayClassName='modal-backdrop'
          style={{
            overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
          }}
        >
          <div className='modal-content modalManualContent'>
            <div className='modal-header'>
              <h5 className='modal-title' style={{ textTransform: 'capitalize' }}>{title}</h5>
            </div>
            <CurrentPageManual />
            <div className='modal-body d-flex justify-content-end'>
              <GreyButton label='Close' onClick={this.closeManualModal.bind(this)} />
            </div>
          </div>
        </ReactModal>
      </>
    )
  }
}

UserManualModal.propTypes = {
  title: PropTypes.string.isRequired
}

export default UserManualModal
