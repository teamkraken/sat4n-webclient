import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'

/* Actions */
import { addRole } from '../../actions/roleActions'
/* Components */
import FormInput from '../FormInput/FormInput'
import GreenButton from '../ButtonGreen/ButtonGreen'
import GreyButton from '../ButtonGrey/ButtonGrey'

class AddRoleForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      name: '',
      description: '',
      errors: {}
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleClose () {
    const { onCloseModal } = this.props
    this.setState({ name: '', errors: {} })
    onCloseModal('close')
  }

  handleSubmit (e) {
    e.preventDefault()
    const { addRole, onCloseModal } = this.props
    const { name, description } = this.state
    const valid = this.validate()

    if (valid) {
      addRole({ name, description })
      onCloseModal('submit')
      this.setState({ name: '', errors: {} })
    }
  }

  handleChange (e) {
    const { name, value } = e.target

    return this.setState({ [name]: value })
  }

  validate () {
    const { name } = this.state

    const errors = {
      name: name === '' ? 'Name can not be empty' : null
    }

    this.setState({ errors })

    /* istanbul ignore next */
    return Object.keys(errors)
      .map((error) => { return errors[error] !== null ? 1 : 0 }) // Count errors
      .reduce((a, b) => { return a + b }) === 0 // Check if errors are over zero
  }

  render () {
    const { description, name, errors } = this.state
    const { onOpenModal, visible, role } = this.props
    const { error } = role

    return visible
      ? (
        <>
          <GreenButton onClick={onOpenModal} label='Add Role' />

          <ReactModal
            isOpen={this.props.showModal}
            onClose={this.handleClose}
            contentLabel='Add Role'
            className='modal-dialog'
            overlayClassName='modal-backdrop'
            style={{
              overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
            }}
          >
            <div className='modal-content'>
              <div className='modal-header'>
                <h5 className='modal-title' id='addUserModalLabel'>Add Role</h5>
                <h5 className='text-danger'>{error}</h5>
              </div>
              <div className='modal-body'>
                <form onSubmit={(e) => { this.handleSubmit(e) }}>
                  <div className='form-group'>

                    <FormInput
                      id='name'
                      name='name'
                      type='text'
                      value={name}
                      label='Name'
                      error={errors.name}
                      onChange={(e) => this.handleChange(e)}
                    />

                    <FormInput
                      id='description'
                      name='description'
                      type='text'
                      value={description}
                      label='Description'
                      onChange={(e) => this.handleChange(e)}
                    />

                  </div>
                  <div className='modal-footer'>
                    <GreyButton label='Close' onClick={this.handleClose} />
                    <GreenButton label='Add' type='submit' />
                  </div>
                </form>
              </div>
            </div>
          </ReactModal>
        </>
        )
      : null
  }
}

/* istanbul ignore next */
const mapStateToProps = ({ role }) => {
  return { role }
}

AddRoleForm.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onOpenModal: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  addRole: PropTypes.func.isRequired,
  role: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired
}

export { AddRoleForm }
export default connect(mapStateToProps, { addRole })(AddRoleForm)
