// AddRoleForm.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { AddRoleForm } from './AddRoleForm'

// uncovered lines 8,28,73,95
describe('AddRoleForm', () => {
  let wrapper
  const e = {
    preventDefault: jest.fn()
  }

  const props = {
    showModal: false,
    visible: true,
    addRole: jest.fn(),
    onOpenModal: jest.fn(),
    onCloseModal: jest.fn(),
    role: {
      error: ''
    }
  }

  beforeEach(() => {
    wrapper = shallow(<AddRoleForm {...props} />)
  })

  it('should render correctly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when visible is false', () => {
    wrapper.setProps({ ...props, visible: false })
    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })

  it('it should call the proper actions given a successful submit call', () => {
    wrapper.find('FormInput[id="name"]').dive()
      .find('input[id="name"]')
      .simulate('change', { target: { name: 'name', value: 'users' } })
    expect(wrapper.state().name).toEqual('users')

    wrapper.find('FormInput[id="description"]').dive()
      .find('input[id="description"]')
      .simulate('change', { target: { name: 'description', value: 'users role' } })
    expect(wrapper.state().description).toEqual('users role')

    wrapper.find('form').simulate('submit', e)
    wrapper.setProps({ showModal: false })

    expect(props.addRole.mock.calls.length).toBe(1)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('it should call the proper actions given a unsuccessful submit call', () => {
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('AddButton[label="Add"]').dive()
      .find('button[type="submit"]')
      .simulate('click')
    wrapper.find('form').simulate('submit', e)

    expect(wrapper.state().errors).toEqual({
      name: 'Name can not be empty'
    })

    expect(props.addRole.mock.calls.length).toBe(0)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('Should invoke onCloseModal', () => {
    wrapper.instance().handleClose()
    expect(props.onCloseModal.mock.calls.length).toBe(1)
    wrapper.instance().handleSubmit(e)
    expect(props.onCloseModal.mock.calls.length).toBe(1)
  })
})
