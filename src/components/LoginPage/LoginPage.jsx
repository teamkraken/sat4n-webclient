import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navigate } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { login } from '../../actions/authActions'
/* Components */
import LoginForm from '../LoginForm/LoginForm'

class LoginPage extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.submit = this.submit.bind(this)
  }

  /**
   * Called when the LoginForm is submitted
   * Calls the login action
   * @param {object} data the credentials provided by the user
   * @memberof LoginPage
   */
  submit (data) {
    const { login } = this.props
    login(data)
  }

  /**
   * Navigates to /devices if the user is already logged in
   * Renders the components jsx
   * @returns component LoginForm
   * @memberof LoginPage
   */
  render () {
    const { auth } = this.props

    if (window.localStorage.sat4nJWT) {
      return <Navigate to='/devices' />
    }

    return (
      <LoginForm submit={this.submit} serverError={auth.error === '401: Unauthorized' ? 'Username or password is incorrect' : auth.error} />
    )
  }
}

const mapStateToProps = ({ auth }) => {
  return { auth }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    user: PropTypes.object,
    token: PropTypes.string,
    resources: PropTypes.object,
    error: PropTypes.string
  })
}

LoginPage.defaultProps = {
  auth: PropTypes.shape({
    user: {},
    token: '',
    resources: [],
    error: ''
  })
}

export { LoginPage }
export default connect(mapStateToProps, { login })(LoginPage)
