// LoginPage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { LoginPage } from './LoginPage.jsx'

describe('LoginPage', () => {
  it('should render correctly given the right props', () => {
    const props = {
      login: jest.fn(),
      history: {
        push: jest.fn()
      },
      auth: {
        error: ''
      }
    }
    const wrapper = shallow(<LoginPage {...props} />)
    expect(toJson).toMatchSnapshot()
    wrapper.instance().submit()
    expect(props.login.mock.calls.length).toBe(1)
  })
})
