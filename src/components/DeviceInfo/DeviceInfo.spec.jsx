// Deviceinfo.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DeviceInfo } from '../DeviceInfo/DeviceInfo.jsx'

describe('DeviceInfo', () => {
  it('should render correctly, given correct props', () => {
    const props = {
      device: {
        device: {
          id: 1,
          name: 'gns3-test-1',
          ip: '10.100.10.99',
          type: 'Cisco',
          uptime: '2 hours, 45 minutes',
          ports: [
            { id: 1, name: 'Fa0/1', status: 'notconnected', vlan: 1, duplex: 'auto', speed: 'auto', type: '10/100BaseTX', lastUsed: 'never', inputErrors: 0, outputErrors: 0 },
            { id: 2, name: 'Fa0/2', status: 'connected', vlan: 20, duplex: 'a-full', speed: 'a-100', type: '10/100BaseTX', lastUsed: '00:00:01', inputErrors: 0, outputErrors: 0 }
          ]
        }
      }
    }
    const wrapper = shallow(<DeviceInfo {... props} />)
    expect(toJson(wrapper)).toMatchSnapshot()

    wrapper.instance().props.device.device.ports[0].status = 'notconnect'
    wrapper.update()

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
// Uncovered lines 51,53,54,56,66
