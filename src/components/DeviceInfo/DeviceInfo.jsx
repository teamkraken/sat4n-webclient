import React, { Component } from 'react'
import { connect } from 'react-redux'

/* Styles */
import '../../styles/Main.less'

class DeviceInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  renderPorts (ports) {
    const { onOpenModal } = this.props
    let portGroup = []

    return ports.map((port, i) => {
      /* Already tested in w/Snapshots */
      /* istanbul ignore next */
      let portStatus = 'devicePortDisabled'
      let portNotAllowed = 'devicePortNotAllowed'

      /* istanbul ignore next */
      // Colorize the port based on status
      switch (port.status) {
        case 'connected':
          portStatus = 'devicePortUp'
          break
        case 'notconnect':
          portStatus = 'devicePortDown'
          break
        case 'err-disabled':
          portStatus = 'devicePortErrDisabled'
          break
      }

      // Remove style if port is normal
      if (!isNaN(port.vlan) && port.status !== 'disabled') {
        portNotAllowed = ''
      }

      /* istanbul ignore next */
      portGroup.push(
        <li
          key={port.name}
          className={`devicePort ${portStatus} ${portNotAllowed}`}
          onClick={() => {
            if (!isNaN(port.vlan) && port.status !== 'disabled') {
              onOpenModal(port)
            }
          }}
        >
          {port.name.split('/').slice(-1)}
        </li>
      )

      if ((i + 1) % 2 === 0 && i !== 0) {
        console.log(portGroup)
        const returnGroup = portGroup
        portGroup = []
        return (
          <ul key={port.id} className='portGroup'>{returnGroup.map((p) => { return p })}</ul>
        )
      } else if (i + 1 === ports.length) {
        const returnGroup = portGroup
        portGroup = []
        return (
          <ul key={port.id} className='portGroup'>{returnGroup.map((p) => { return p })}</ul>
        )
      }
    })
  }

  render () {
    const { device } = this.props.device
    const { ports } = device
    const deviceKeys = ['name', 'model', 'ip', 'location', 'uptime']

    return (
      <div className='bg-light infoContainer'>
        <dl className='deviceInfo'>
          {deviceKeys.map((key) => {
            return (
              <React.Fragment key={key}>
                <dt>{key}</dt>
                <dd>
                  {device[key]}
                </dd>
              </React.Fragment>
            )
          })}
        </dl>
        <div className='devicePorts'>
          {this.renderPorts(ports)}
        </div>
      </div>
    )
  }
}
/* Needless to test mapStateToProps */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}
export { DeviceInfo }
export default connect(mapStateToProps)(DeviceInfo)
