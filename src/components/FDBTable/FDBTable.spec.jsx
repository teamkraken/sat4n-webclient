// FDBTable.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { FDBTable } from './FDBTable.jsx'

describe('FDBTable', () => {
  let wrapper
  const props = {
    searchInput: '',
    device: {
      device: {
        macTable: []
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<FDBTable {...props} />)
  })

  it('should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render the list in full', () => {
    wrapper.setProps({
      searchInput: '',
      device: {
        device: {
          macTable: [
            { vlan: 1, macTable: '00:00:00:00:00:00', type: 'STATIC', ports: 'Fa0/1' },
            { vlan: 2, macTable: '00:00:00:00:00:01', type: 'DYNAMIC', ports: 'Gi0/1' }
          ]
        }
      }
    })

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
