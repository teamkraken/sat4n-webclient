import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import FDBTableManual from '../Manuals/FDBTableManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class FDBTable extends Component {
  render () {
    const { device, searchInput } = this.props
    const { macTable } = device.device
    const tableHeaders = macTable.length ? Object.keys(macTable[0]) : []

    // Applying search filter
    /* istanbul ignore next */
    const macList = searchInput.length ? filterBySearchInput(macTable, searchInput, tableHeaders) : macTable
    return (
      <>
        <span>
          <h2>Forwarding Database Table</h2>
        </span>
        <UserManualModal
          title='FDB Table User Information'
          CurrentPageManual={FDBTableManual}
        />
        <ul className='list listMac'>
          <ListHeader headers={tableHeaders} />
          {macList.map(/* istanbul ignore next */(mac) => {
            return (
              <ListItem key={`${mac.vlan}${mac.macAddress}`}>
                <dl>
                  {Object.keys(mac).map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{mac[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
              </ListItem>
            )
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

FDBTable.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      macTable: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}

FDBTable.defaultProps = {
  searchInput: ''
}

export { FDBTable }
export default connect(mapStateToProps)(FDBTable)
