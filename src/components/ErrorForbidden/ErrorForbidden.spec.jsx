// ErrorForbidden.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import ErrorForbidden from './ErrorForbidden'

describe('ErrorForbidden', () => {
  const props = {
    errorMessage: 'ERROR'
  }

  it('Should render correctly given the right props', () => {
    const wrapper = shallow(<ErrorForbidden {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
