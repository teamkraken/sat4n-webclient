import React from 'react'
import PropTypes from 'prop-types'

/* Styles */
import '../../styles/Main.less'

const ErrorForbidden = ({ errorMessage }) => (
  <div className='container errorContainer'>
    <h1 className='display-3'>
      {errorMessage}
    </h1>
    <p className='lead'>
      <a className='btn btn-dark btn-lg' href='/devices' role='button'>Home</a>
    </p>
  </div>
)

ErrorForbidden.propTypes = {
  errorMessage: PropTypes.string.isRequired
}

export default ErrorForbidden
