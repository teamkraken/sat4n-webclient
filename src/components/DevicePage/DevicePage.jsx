import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, getDevice, updateDevicePort, updateLoading } from '../../actions/deviceActions'
import { getAuditByDeviceId } from '../../actions/auditActions'
/* Components */
import { AuditList } from '../AuditList/AuditList'
import { DevicePortModal } from '../DevicePortModal/DevicePortModal'
import ArpTable from '../ArpTable/ArpTable'
import DeviceInfo from '../DeviceInfo/DeviceInfo'
import DeviceLogs from '../DeviceLogs/DeviceLogs'
import DhcpSnooping from '../DhcpSnooping/DhcpSnooping'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import FDBTable from '../FDBTable/FDBTable'
import Neighbors from '../Neighbors/Neighbors'
import Ports from '../Ports/Ports'
import SearchBar from '../SearchBar/SearchBar'
import SubNavigationBar from '../SubNavigationBar/SubNavigationBar'
import VlansList from '../VlansList/VlansList'
import UserManualModal from '../UserManualModal/UserManualModal'
import AuditLogManual from '../Manuals/AuditLogManual'
/* Utils */
import { withRouter } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class DevicePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      deviceSelection: '',
      showModal: false,
      portID: 0,
      vlanID: 0,
      port: {},
      search: ''
    }
    this.handleCloseModal = this.handleCloseModal.bind(this)
    this.handleDeviceSelection = this.handleDeviceSelection.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
    const { auth, getAuditByDeviceId, getDevice } = this.props
    const id = this.props.match.params.id

    getDevice(id)
    if (auth.resources.audit.indexOf('get') !== -1) {
      getAuditByDeviceId(id)
    }
  }

  /**
   * Renders different information about device underneath the port section.
   * always on but changes when the deviceSelection state is changed.
   * @memberof DevicePage
   */
  deviceContentRender () {
    const { deviceSelection, search } = this.state
    const { audit, auth } = this.props
    const { logs } = audit

    switch (deviceSelection) {
      case 'Vlans':
        return <VlansList searchInput={search} />
      case 'Logs':
        return <DeviceLogs searchInput={search} />
      case 'ARPTable':
        return <ArpTable searchInput={search} />
      case 'FDBTable':
        return <FDBTable searchInput={search} />
      case 'DHCPSnoop':
        return <DhcpSnooping searchInput={search} />
      case 'Neighbors':
        return <Neighbors searchInput={search} />
      case 'Audit Log':
        return auth.resources.audit.indexOf('get') !== -1
          ? (
            <>
              <span>
                <h2>Audit Log</h2>
              </span>
              <UserManualModal
                title='Audit Log User Information'
                CurrentPageManual={AuditLogManual}
              />
              <AuditList searchInput={search} logs={logs} />
            </>)
          : (<ErrorForbidden errorMessage={audit.error} />)
      default:
        return <Ports searchInput={search} onOpenModal={this.handleOpenModal} />
    }
  }

  /**
   * Called when a button on the subnavigation bar is clicked
   * Sets the state of deviceSelection to the string name of the button clicked.
   * @param {string} selection string name of button being clicked in quotes
   * @memberof DevicePage
   */
  handleDeviceSelection (selection) {
    this.setState({ deviceSelection: selection })
  }

  handleOpenModal (port) {
    const { vlans } = this.props.device.device

    this.setState({
      showModal: true,
      port,
      vlanID: port.vlan,
      vlans
    })
  }

  handleCloseModal (type) {
    const { clearErrors } = this.props

    if (type === 'close') {
      clearErrors()
    }

    this.setState({
      showModal: false,
      port: {},
      portID: 0,
      vlanID: 0
    })
  }

  handleSubmit (e) {
    e.preventDefault()
    const { port, vlanID } = this.state
    const { device } = this.props.device
    const { updateDevicePort, updateLoading } = this.props

    updateLoading(port.id)

    updateDevicePort(device.id, port.id, vlanID)
    this.handleCloseModal('submit')
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { portID, port, vlanID } = this.state
    const { auth } = this.props
    const { device, error } = this.props.device
    const auditRestriction = auth.resources.audit.indexOf('get') !== -1

    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='deviceContainer'>
          <SubNavigationBar onClick={this.handleDeviceSelection} audit={auditRestriction} />
          <DeviceInfo
            portID={portID}
            onOpenModal={this.handleOpenModal}
          />
          <DevicePortModal
            deviceID={device.id}
            vlanID={vlanID}
            port={port}
            vlans={device.vlans}
            onOpenModal={this.handleOpenModal}
            onCloseModal={this.handleCloseModal}
            showModal={this.state.showModal}
            onChange={/* istanbul ignore next */(e) => { this.setState({ vlanID: e.target.value }) }}
            onSubmit={this.handleSubmit}
            visible={auth.resources.ports.indexOf('update') !== -1}
          />
          <div className='listContainer'>
            <SearchBar onInput={this.handleInput} />
            {this.deviceContentRender()}
          </div>
        </div>
        )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ auth, audit, device }) => {
  return { auth, audit, device }
}

DevicePage.propTypes = {
  clearErrors: PropTypes.func.isRequired,
  getDevice: PropTypes.func.isRequired,
  getAuditByDeviceId: PropTypes.func.isRequired,
  updateDevicePort: PropTypes.func.isRequired,
  updateLoading: PropTypes.func.isRequired,
  device: PropTypes.shape({
    device: PropTypes.shape({
      ports: PropTypes.array.isRequired,
      vlans: PropTypes.array.isRequired,
      log: PropTypes.array.isRequired
    }).isRequired,
    error: PropTypes.string.isRequired,
    portLoading: PropTypes.arrayOf(PropTypes.number)
  }).isRequired,
  audit: PropTypes.shape({
    error: PropTypes.string,
    logs: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        action: PropTypes.string.isRequired,
        details: PropTypes.string.isRequired,
        userId: PropTypes.number,
        createdAt: PropTypes.string.isRequired,
        deviceId: PropTypes.number
      }).isRequired
    ).isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { DevicePage }
export default connect(mapStateToProps, { clearErrors, getAuditByDeviceId, getDevice, updateDevicePort, updateLoading })(withRouter(DevicePage))
