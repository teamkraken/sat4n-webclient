// DevicePage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DevicePage } from './DevicePage'

describe('DevicePage', () => {
  let wrapper
  const props = {
    auth: {
      resources: {
        ports: ['update'],
        audit: ['get']
      }
    },
    device: {
      device: {
        id: 1,
        name: 'gns3-test-1',
        ip: '10.100.10.99',
        type: 'Cisco',
        uptime: '2 hours, 45 minutes',
        ports: [
          { name: 'Fa0/1', description: '', status: 'connected', vlan: 1, duplex: 'a-full', speed: 'a-100', type: '10/100BaseTX' },
          { name: 'Fa0/2', description: '', status: 'notconnect', vlan: 1, duplex: 'auto', speed: 'auto', type: '10/100BaseTX' }

        ],
        vlans: [],
        log: []
      },
      error: '',
      portLoading: []
    },
    audit: {
      logs: [
        {
          id: 1,
          action: 'Device:CREATE',
          details: 'Detailed info',
          createdAt: '01/01/2019',
          deviceId: 1,
          userId: 1
        }
      ],
      error: ''
    },
    getDevice: jest.fn(),
    getAuditByDeviceId: jest.fn(),
    clearErrors: jest.fn(),
    updateDevicePort: jest.fn(),
    updateLoading: jest.fn(),
    match: {
      params: {
        id: {
          id: 1
        }
      }
    }
  }
  const e = {
    preventDefault: jest.fn()
  }

  it('should render given the correct props', () => {
    wrapper = shallow(<DevicePage {...props} />)
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render the correct component given what link is clicked', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.find('SubNavigationBar').dive()
      .find('a').at(1).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('Vlans')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(2).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('ARPTable')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(3).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('FDBTable')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(4).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('DHCPSnoop')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(5).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('Neighbors')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(6).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('Logs')

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(7).simulate('click')

    expect(wrapper.instance().state.deviceSelection).toEqual('Audit Log')
  })

  it('Should populate the state with the correct values when modal is opened', () => {
    wrapper = shallow(<DevicePage {...props} />)
    const thePort = {
      name: 'Fa0/1',
      description: '',
      status: 'connected',
      vlan: 1,
      duplex: 'a-full',
      speed: 'a-100',
      type: '10/100BaseTX'
    }

    wrapper.instance().handleOpenModal(thePort)
    expect(wrapper.instance().state.port).toEqual(thePort)
  })

  it('Should populate the state with the correct values when modal is closed', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.instance().handleCloseModal('submit')

    expect(props.clearErrors.mock.calls.length).toBe(0)
    expect(wrapper.instance().state.showModal).toEqual(false)

    wrapper.instance().handleCloseModal('close')

    expect(props.clearErrors.mock.calls.length).toBe(1)
    expect(wrapper.instance().state.showModal).toEqual(false)
  })

  it('Should call the correct functions depending on props', () => {
    wrapper = shallow(<DevicePage {...props} />)
    expect(props.getAuditByDeviceId.mock.calls.length).toBe(1)
  })

  it('Should call function on submit', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.find('DevicePortModal').dive()
      .find('form')
      .simulate('submit', e)
  })

  it('Should change state on input', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.instance().handleInput({ target: { name: 'search', value: 'a' } })
    expect(wrapper.instance().state.search).toBe('a')
  })

  it('Should not call getAuditByDeviceId if user does not have access', () => {
    const newProps = {
      ...props,
      auth: {
        resources: {
          audit: [],
          ports: ['update']
        }
      }
    }
    wrapper = shallow(<DevicePage {...newProps} />)
    expect(wrapper.instance().props.getAuditByDeviceId.mock.calls.length).toBe(0)

    wrapper.find('SubNavigationBar').dive()
      .find('a').at(7).simulate('click')
    expect(wrapper.find('ErrorForbidden')).toBeDefined()
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render fifferently when user does not have access to audit', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.setProps({ ...props, auth: { resources: { ...props.auth.resources, audit: ['get'] } } })
    wrapper.update()
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when error is resieved', () => {
    wrapper = shallow(<DevicePage {...props} />)
    wrapper.setProps({ ...props, device: { ...props.device, error: 'GET' } })
    wrapper.update()
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
