// NavigationBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { NavigationBar } from './NavigationBar'

describe('NavigationBar', () => {
  let wrapper
  const props = {
    auth: {
      user: {
        id: 1,
        username: 'admin',
        role: {}
      },
      resources: {
        devices: ['list'],
        roles: ['list'],
        users: ['list'],
        audit: ['get']
      }
    },
    logout: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<NavigationBar {...props} />)
  })

  it('should render correctly given correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when user does not have access to ceratin resources', () => {
    wrapper.setProps({
      ...props,
      auth: {
        ...props.auth,
        resources: {
          devices: [],
          roles: [],
          users: [],
          audit: []
        }
      }
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('Should logout when logout link is clicked', () => {
    wrapper.find('a[id="logout"]').simulate('click')

    expect(wrapper.instance().props.logout.mock.calls.length).toBe(1)
  })
})
