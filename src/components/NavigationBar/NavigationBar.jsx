import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import { withRouter } from '../../utils'

/* Actions */
import { logout } from '../../actions/authActions'
/* Styles */
import '../../styles/Main.less'

class NavigationBar extends Component {
  render () {
    const { auth, logout } = this.props

    return (
      <nav className='navFixedTop navbar navbar-expand-md navbar-dark nav-color'>
        <button
          className='navbar-toggler'
          type='button'
          data-toggle='collapse'
          data-target='#navbarColor01'
          aria-controls='navbarColor01'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon' />
        </button>
        <a className='navbarBrand navbar-brand' href='/devices'>SAT:4N</a>
        <ul className='navbarList navbar-nav navbar-collapse collapse mr-auto' id='navbarColor01'>
          <li className={`nav-item ${auth.resources.devices.indexOf('list') !== -1 ? '' : 'd-none'}`}>
            <NavLink end to='/' className='nav-link'>Devices</NavLink>
          </li>
          <li className={`nav-item ${auth.resources.roles.indexOf('list') !== -1 ? '' : 'd-none'}`}>
            <NavLink end to='/roles' className='nav-link'>Roles</NavLink>
          </li>
          <li className={`nav-item ${auth.resources.users.indexOf('list') !== -1 ? '' : 'd-none'}`}>
            <NavLink end to='/users' className='nav-link'>Users</NavLink>
          </li>
          <li className={`nav-item ${auth.resources.audit.indexOf('get') !== -1 ? '' : 'd-none'}`}>
            <NavLink end to='/audit' className='nav-link'>Audit log</NavLink>
          </li>
          <li className='nav-item' id='dropdownNavitem'>
            <a className='nav-link' href='' id='logout' onClick={() => logout()}>{`Logout (${auth.user.username})`}</a>
          </li>
        </ul>
      </nav>
    )
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ auth }) => {
  return { auth }
}

NavigationBar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    user: PropTypes.object,
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { NavigationBar }
export default withRouter(connect(mapStateToProps, { logout })(NavigationBar))
