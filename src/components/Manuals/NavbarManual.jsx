import React from 'react'

const NavbarManual = () => {
  return (
    <>
      <div className='container'>
        <p>In the Navigation bar on top you can select between:</p>
        <ul>
          <li><b>The Logo:</b> which brings you back to the Home page</li>
          <li><b>Devices:</b> which also brings you back to the Home page</li>
          <li><b>Users:</b> which brings you to the Users page</li>
          <li><b>Roles:</b> which brings you to the Roles page</li>
          <li><b>Audit Log:</b> which brings you to the Audit Log page</li>
        </ul>
      </div>
    </>
  )
}

export default NavbarManual
