import React from 'react'

const PortsManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Vlans page. It shows a list of vlans available in the selected network hardware.</p>
        <p>You can search for vlans if the list is long.</p>
        <p>Each row shows you the Id and the Name of a specific vlan in the selected network hardware.</p>
        <ul>
          <li><b>To search for a vlan:</b> Start typing the Id or the Name of the vlan you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default PortsManual
