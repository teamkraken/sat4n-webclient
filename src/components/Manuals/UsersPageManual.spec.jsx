// UsersPage.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import UsersPageManual from './UsersPageManual'

describe('UsersPageManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<UsersPageManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
