import React from 'react'

const RolePageManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Edit Role page. It allows you to give the selected user group(role) less or more privileges.</p>
        <p>You can add or change the description of the selected user group(role). </p>
        <p>You can give the selected user group(role) permissions to:</p>
        <ul>
          <li>See the Audit log.</li>
          <li>Add, remove, see single and multiple devices.</li>
          <li>Add, remove, change, see single and multiple users.</li>
          <li>Add, remove, change, see single and multiple roles.</li>
          <li>Update, restart, disable and enable ports on devices.</li>
        </ul>
        <p>To change role premissions: Simply select the checkboxes for the permissions you want to give the user group and leave others blank. Press the green "Save" button.</p>
      </div>
    </>
  )
}

export default RolePageManual
