// RolesPage.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import RolesPageManual from './RolesPageManual'

describe('RolesPageManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<RolesPageManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
