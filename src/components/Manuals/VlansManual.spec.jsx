// Vlans.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import VlansManual from './VlansManual'

describe('VlansManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<VlansManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
