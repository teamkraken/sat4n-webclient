// DhcpSnooping.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import DhcpSnoopingManual from './DhcpSnoopingManual'

describe('FDBTableManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<DhcpSnoopingManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
