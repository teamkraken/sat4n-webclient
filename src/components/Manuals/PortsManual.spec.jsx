// Ports.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import PortsManual from './PortsManual'

describe('PortsManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<PortsManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
