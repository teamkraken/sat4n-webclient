import React from 'react'

const ArpTableManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the ARP Table page. It shows a list of devices associated with a given IP Address in the selected network hardware.</p>
        <p>You can search for devices if the list is long.</p>
        <p>Each row shows you a device connected to the network hardware selected. The Protocol used, It's IP Address, Age of a connection (in minutes), It's MAC Address, Type and which Interface used.</p>
        <ul>
          <li><b>To search for a device:</b> Start typing the Protocol, Address, Age, MacAddress, Type or Interface of the device you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default ArpTableManual
