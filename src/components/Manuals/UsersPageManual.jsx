import React from 'react'

const UsersPageManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Users page. It shows a list of users currently in the system.</p>
        <p>You can add a user with a username, password and assign the user a role. You can search for users if the list is long.</p>
        <p>Each row shows you the username and role of an added users.</p>
        <ul>
          <li><b>To add a user:</b> Press the green "Add User" button. In the "Add User" box that appears, fill out the Name, Password and assign a Role(all fields are required) and press the green "Add" button. To exit the window press the grey "Cancel" button.</li>
          <li><b>To remove a user:</b> Press the red trash can button on the rightmost side of a user on the list</li>
          <li><b>To search for a user:</b> Start typing the Username or a Role you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default UsersPageManual
