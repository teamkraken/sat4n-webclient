// Neighbors.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import NeighborsManual from './NeighborsManual'

describe('NeighborsManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<NeighborsManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
