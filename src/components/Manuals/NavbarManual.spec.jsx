// NavigationBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import NavbarManual from './NavbarManual'

describe('NavbarManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<NavbarManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
