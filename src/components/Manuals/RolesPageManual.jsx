import React from 'react'

const RolesPageManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Roles page. It shows a list of roles currently available.</p>
        <p>You can add a role with a Name and Description(optional). You can search for roles if the list is long.</p>
        <p>Each row shows you the Name and Description of an added role.</p>
        <ul>
          <li><b>To add a role:</b> Press the green "Add Role" button. In the "Add Role" box that appears, fill out the Name field and optionally the Description field and press the green "Add" button. To exit the window press the grey "Cancel" button.</li>
          <li><b>To remove a role:</b> Press the red trash can button on the rightmost side of a role on the list</li>
          <li><b>To search for a role:</b> Start typing the Name or the Description of the role you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default RolesPageManual
