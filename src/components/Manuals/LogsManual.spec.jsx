// DeviceLogs.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import LogsManual from './LogsManual'

describe('LogsManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<LogsManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
