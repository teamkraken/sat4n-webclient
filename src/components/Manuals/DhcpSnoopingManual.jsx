import React from 'react'

const DhcpSnoopingManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the DHCP Snooping page. It shows a list of devices in the selected network hardware that have DHCP assigned IP Addresses.</p>
        <p>You can search for devices if the list is long.</p>
        <p>Each row shows you a device connected to the network hardware selected. it's MAC Address, it's IP Address, the DHCP Lease, type of connection (DHCP/Static), which Vlan assigned and Interface.</p>
        <ul>
          <li><b>To search for a device:</b> Start typing the MacAddress, IpAddress, Lease, Type, Vlan or Interface of the device you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default DhcpSnoopingManual
