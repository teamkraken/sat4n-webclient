import React from 'react'

const DevicesPageManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Devices page which is also the Home page. It shows a list of the devices currently in the app.</p>
        <p>You can add a device with it's IP Address and do a search for devices if the list is long.</p>
        <p>Each row shows you the Name, IP Address, Location and Model type of an added device.</p>
        <ul>
          <li><b>To add a device:</b> Press the green "Add Device" button. In the "Add Device" box that appears, fill out the IP Address field (with the form [1.1.1.1]) and press the green "Add" button. To exit the window press the grey "Cancel" button.</li>
          <li><b>To remove a device:</b> Press the red trash can button on the rightmost side of a device on the list</li>
          <li><b>To search for a device:</b> Start typing the Name, Ip, Location or Model you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default DevicesPageManual
