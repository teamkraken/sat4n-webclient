// DevicesPageManual.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import DevicesPageManual from './DevicesPageManual'

describe('DevicesPageManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<DevicesPageManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
