import React from 'react'

const FDBTableManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the FDB Table page. It shows a list of devices in the selected network hardware and which port they are connected to.</p>
        <p>You can search for devices if the list is long.</p>
        <p>Each row shows you a device connected to the network hardware selected. Which Vlan is assigned, It's MAC Address, Type of connection and which Port on the network hardware it is connected to. </p>
        <ul>
          <li><b>To search for a device:</b> Start typing the Vlan, MacAddress, Type or Ports of the device you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default FDBTableManual
