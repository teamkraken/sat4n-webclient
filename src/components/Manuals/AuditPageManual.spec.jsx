// AuditPage.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import AuditPageManual from './AuditPageManual'

describe('AuditPageManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<AuditPageManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
