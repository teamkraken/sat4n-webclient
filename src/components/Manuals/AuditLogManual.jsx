import React from 'react'

const AuditLogManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Device Audit Log page. It shows a list of actions carried out on the selected network hardware and by whom. </p>
        <p>You can search for log entries if the list is long.</p>
        <p>Each row shows you a log entry on the network hardware selected. When it was done, Action, and which user preformed the action.</p>
        <ul>
          <li><b>To search for a log entry:</b> Start typing the Timestamp, Action or Details of the log entry you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default AuditLogManual
