import React from 'react'

const NeighborsManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Neighbors page. It shows a list of devices in the selected network hardware that are discoverable with cdp or lldp protocols.</p>
        <p>You can search for devices if the list is long.</p>
        <p>Each row shows you a device connected to the network hardware selected. Device name, which Port on the selected device it's connected to, Hold Time, Capability, Platform and which Port on the neighbor device is connected.</p>
        <ul>
          <li><b>To search for a device:</b> Start typing the Device, LocalPort, HoldTime, Capability, Platform or RemotePort of the device you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default NeighborsManual
