import React from 'react'

const LogsManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Logs page. It shows the logs from the selected network hardware.</p>
        <p>You can search for keywords if the list is long.</p>
        <p>Each row shows you a log lntry.</p>
        <ul>
          <li><b>To search for a log entry:</b> Start typing anything from the log entries you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default LogsManual
