// UserPage.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import UserPageManual from './UserPageManual'

describe('UserPageManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<UserPageManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
