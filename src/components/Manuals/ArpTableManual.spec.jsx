// ArpTable.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import ArpTableManual from './ArpTableManual'

describe('ArpTableManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<ArpTableManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
