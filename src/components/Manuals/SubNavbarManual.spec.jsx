// SubNavigationBar.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import SubNavbarManual from './SubNavbarManual'

describe('SubNavbarManual', () => {
  it('should render correctly given the correct props', () => {
    const wrapper = shallow(<SubNavbarManual />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
