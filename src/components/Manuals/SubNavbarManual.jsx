import React from 'react'

const SubNavbarManual = () => {
  return (
    <>
      <div className='container'>
        <p>In the sub navigation bar below the main navigation bar that appears when a device is selected you can select between:</p>
        <ul>
          <li><b>Ports:</b> Which shows you the Ports view of a selected device and is also the default view in the Device page.</li>
          <li><b>Vlans:</b> Which shows you the Vlans view of a selected device.</li>
          <li><b>ARP Tabla:</b> Which shows you the ARP Table view of a selected device.</li>
          <li><b>FDB Table:</b> Which shows you the FDB Table view of a selected device.</li>
          <li><b>DHCP Snooping:</b> Which shows you the DHCP Snooping view of a selected device.</li>
          <li><b>Neighbors:</b> Which shows you the Neighbors view of a selected device.</li>
          <li><b>Logs:</b> Which shows you the Logs view of a selected device.</li>
          <li><b>Audit Log:</b> Which shows you the Audit Log view of a selected device.</li>
        </ul>
        <p>In the white info board below the light grey Subnavigation bar on the left side you see some basic information about the selected network device. It's Name, Model, IP Address, Location and How long it's been running.</p>
        <p>In the middle of the white info board below the light grey sub navigation bar you see illustrated ports physically available on the selected network device.</p>
        <p>The colors on the ports indicate the current status of each port.</p>
        <ul>
          <li><b className='text-secondary'>Grey</b> means active but not connected.</li>
          <li><b className='text-success'>Green</b> means active and connected.</li>
          <li><b>Black</b> means inactive(disabled).</li>
          <li><b className='text-danger'>Red</b> means it has an error.</li>
        </ul>
        <p>You can also click the illustrated ports to change their assigned vlan which is the same as clicking the grey pen symbol on the rightmost side of a port on the list below.</p>
        <ul>
          <li><b>To assign a new Vlan to a port:</b> Click an illustrated port on the white info board below the subnavigation bar. In the "Change Vlan" box that appears, assign a new Vlan from the "Vlan:" dropdown list and press the green "Save" button. To exit the window press the grey "Cancel" button. </li>
        </ul>
      </div>
    </>
  )
}

export default SubNavbarManual
