import React from 'react'

const PortsManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Device page. It shows a list of ports and information available on the selected network hardware.</p>
        <p>You can select a port to assign it a new vlan, restart it or disable it. You can search for ports if the list is long.</p>
        <p>Each row shows you the Name, Status, Vlan, Duplex, Speed, Type, LastUsed, Input Errors and Output Errors of a specific port on the selected network hardware.</p>
        <ul>
          <li><b>To assign a new Vlan to a port:</b> Press grey edit button on the rightmost side of a port row in the list. In the "Change Vlan" box that appears, assign a new Vlan from the "Vlan:" dropdown list and press the green "Save" button. To exit the window press the grey "Cancel" button.</li>
          <li><b>To restart a port:</b> Press the red circle arrow button on the rightmost side of a port row on the list.</li>
          <li><b>To disable a port:</b> Press the green on/off switch on the rightmost side of a port row on the list.</li>
          <li><b>To search for a port:</b> Start typing the Name, Status, Vlan, Duplex, Speed, Type, Last Used, Input Errors or Output Errors you want to find in the "Search here.." search box on the top below the navigation bar.</li>
        </ul>
      </div>
    </>
  )
}

export default PortsManual
