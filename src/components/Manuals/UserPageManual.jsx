import React from 'react'

const UserPageManual = () => {
  return (
    <>
      <div className='container'>
        <p>This page is the Edit User page. It allows you to change user's password, assign user a new role and view his audit log.</p>
        <p>You can also search for log entries in the user's audit log to see which actions he has preformed and when.</p>
        <ul>
          <li><b>To change a the user's password:</b> Type a new password in the password field and repeat the same password in the field below it. Press the green "Save" button. To exit the window press the grey "Cancel" button.</li>
          <li><b>To assign the user a new role:</b> Select a new role from the role dropdown list.</li>
          <li><b>To search for an entry in the Audit log:</b> Start typing the Timestamp, Action or Details of the log entry you want to find.</li>
        </ul>
      </div>
    </>
  )
}

export default UserPageManual
