import React from 'react'

/* Styles */
import '../../styles/Main.less'

const ErrorPageNotFound = () => (
  <div className='container errorContainer'>
    <h1 className='display-3'>404 page not found</h1>
    <p className='lead'>
      <a className='btn btn-dark btn-lg' href='/devices' role='button'>Home</a>
    </p>
  </div>
)

export default ErrorPageNotFound
