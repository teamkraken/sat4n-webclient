// PageNotFound.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import PageNotFound from './ErrorPageNotFound.jsx'

describe('PageNotFound', () => {
  const props = {}

  it('should render correctly given the right props', () => {
    const wrapper = shallow(<PageNotFound {...props} />)

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
