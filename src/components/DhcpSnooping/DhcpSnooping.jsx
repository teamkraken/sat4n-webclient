import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import DhcpSnoopingManual from '../Manuals/DhcpSnoopingManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class DhcpSnooping extends Component {
  render () {
    const { device, searchInput } = this.props
    const { ipSnoop } = device.device
    const tableHeaders = ipSnoop.length ? Object.keys(ipSnoop[0]) : []

    // Applying search filter
    /* istanbul ignore next */
    const snoopList = searchInput.length ? filterBySearchInput(ipSnoop, searchInput, tableHeaders) : ipSnoop

    return (
      <>
        <span>
          <h2>DHCP Snooping</h2>
        </span>
        <UserManualModal
          title='DHCP Snooping User Information'
          CurrentPageManual={DhcpSnoopingManual}
        />
        <ul className='list listSnooping'>
          <ListHeader headers={tableHeaders} />
          {snoopList.map(/* istanbul ignore next */(address) => {
            return (
              <ListItem key={address.ipAddress}>
                <dl>
                  {Object.keys(address).map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{address[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
              </ListItem>
            )
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

DhcpSnooping.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      ipSnoop: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}

DhcpSnooping.defaultProps = {
  searchInput: ''
}

export { DhcpSnooping }
export default connect(mapStateToProps)(DhcpSnooping)
