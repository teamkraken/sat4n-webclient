// DhcpSnooping.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DhcpSnooping } from './DhcpSnooping.jsx'

describe('DhcpSnooping', () => {
  let wrapper
  const props = {
    searchInput: '',
    device: {
      device: {
        ipSnoop: [
          { macAddress: '00:00:00:00:00:00', ipAddress: '10.0.0.1', lease: 1000, type: 'dhcp-snooping', vlan: 1, interface: 'FastEthernet0/1' },
          { macAddress: '00:00:00:00:00:01', ipAddress: '10.0.0.2', lease: 1000, type: 'dhcp-snooping', vlan: 1, interface: 'GigabitEthernet0/1' }
        ]
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<DhcpSnooping {...props} />)
  })

  it('should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render the list in full', () => {
    wrapper.setProps({
      searchInput: '',
      device: {
        device: {
          ipSnoop: []
        }
      }
    })

    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })
})
