import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, getUsers } from '../../actions/userActions'
/* Components */
import AddUserForm from '../AddUserForm/AddUserForm'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import SearchBar from '../SearchBar/SearchBar'
import UsersList from '../UsersList/UsersList'
import UserManualModal from '../UserManualModal/UserManualModal'
import UsersPageManual from '../Manuals/UsersPageManual'
/* Styles */
import '../../styles/Main.less'

class UsersPage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      search: ''
    }
    this.handleInput = this.handleInput.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)
  }

  componentDidMount () {
    const { getUsers } = this.props

    getUsers()
  }

  handleOpenModal () {
    this.setState({ showModal: true })
  }

  handleCloseModal (type) {
    const { clearErrors } = this.props

    if (type === 'close') {
      clearErrors()
    }

    this.setState({ showModal: false })
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { search } = this.state
    const { auth, user } = this.props
    const { error } = user

    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2 className='text-light'>Users</h2>
            <AddUserForm
              onOpenModal={this.handleOpenModal}
              onCloseModal={this.handleCloseModal}
              showModal={this.state.showModal || error !== ''}
              visible={auth.resources.users.indexOf('create') !== -1}
            />
          </span>
          <UserManualModal
            title='Users Page User Information'
            CurrentPageManual={UsersPageManual}
          />
          <SearchBar onInput={this.handleInput} />
          <UsersList searchInput={search} />
        </div>
        )
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ auth, user }) => {
  return { auth, user }
}

UsersPage.propTypes = {
  clearErrors: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { UsersPage }
export default connect(mapStateToProps, { clearErrors, getUsers })(UsersPage)
