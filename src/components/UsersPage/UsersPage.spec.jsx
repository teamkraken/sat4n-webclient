// UsersPage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { UsersPage } from './UsersPage.jsx'

describe('UsersPage', () => {
  let wrapper
  const props = {
    auth: {
      resources: {
        users: ['create']
      }
    },
    user: {
      error: ''
    },
    getUsers: jest.fn(),
    clearErrors: jest.fn(),
    match: {
      params: {
        id: {
          id: 1
        }
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<UsersPage {...props} />)
  })

  it('should render correctly given the right props', () => {
    expect(toJson).toMatchSnapshot()
    expect(props.getUsers.mock.calls.length).toBe(1)
  })

  it('Should change showModal state to true/false', () => {
    expect(wrapper.state().showModal).toBe(false)

    wrapper.instance().handleOpenModal()
    expect(wrapper.state().showModal).toBe(true)

    wrapper.instance().handleCloseModal('close')
    expect(wrapper.state().showModal).toBe(false)
  })

  it('Should change state on input', () => {
    wrapper.instance().handleInput({ target: { value: 'search input' } })

    expect(wrapper.state().search).toEqual('search input')
  })

  it('Should clearErrors when modal is closed', () => {
    wrapper.instance().handleCloseModal()
    expect(wrapper.instance().props.clearErrors.mock.calls.length).toBe(0)

    wrapper.instance().handleCloseModal('close')
    expect(wrapper.instance().props.clearErrors.mock.calls.length).toBe(1)
    expect(wrapper.state().showModal).toBe(false)
  })

  it('Should render differently on error', () => {
    wrapper.setProps({ ...props, user: { error: 'GET' } })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })
})
