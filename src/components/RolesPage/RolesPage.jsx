import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, getRoles } from '../../actions/roleActions'
/* Components */
import AddRoleForm from '../AddRoleForm/AddRoleForm'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import RolesList from '../RolesList/RolesList'
import SearchBar from '../SearchBar/SearchBar'
import RolesPageManual from '../Manuals/RolesPageManual'
import UserManualModal from '../UserManualModal/UserManualModal'
/* Styles */
import '../../styles/Main.less'

class RolesPage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      search: ''
    }
    this.handleInput = this.handleInput.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)
  }

  componentDidMount () {
    const { getRoles } = this.props

    getRoles()
  }

  handleOpenModal () {
    this.setState({ showModal: true })
  }

  handleCloseModal (type) {
    const { clearErrors } = this.props

    if (type === 'close') {
      clearErrors()
    }

    this.setState({ showModal: false })
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { search } = this.state
    const { auth, role } = this.props
    const { error } = role

    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2 className='text-light'>Roles</h2>
            <AddRoleForm
              onOpenModal={this.handleOpenModal}
              onCloseModal={this.handleCloseModal}
              showModal={this.state.showModal || error.includes('CREATE')}
              visible={auth.resources.roles.indexOf('create') !== -1}
            />
          </span>
          <UserManualModal
            title='Roles Page User Information'
            CurrentPageManual={RolesPageManual}
          />
          <SearchBar onInput={this.handleInput} />
          <RolesList searchInput={search} />
        </div>
        )
  }
}

/* istanbul ignore next */
const mapStateToProps = ({ auth, role }) => {
  return { auth, role }
}

RolesPage.propTypes = {
  clearErrors: PropTypes.func.isRequired,
  getRoles: PropTypes.func.isRequired,
  role: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { RolesPage }
export default connect(mapStateToProps, { clearErrors, getRoles })(RolesPage)
