// ListHeader.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ListHeader } from './ListHeader'

describe('ListHeader', () => {
  let wrapper
  const props = {
    headers: ['id', 'name', 'ip'],
    sort: {
      column: 'name',
      order: 'down'
    }
  }

  beforeEach(() => {
    wrapper = shallow(<ListHeader {...props} />)
  })

  it('Should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
