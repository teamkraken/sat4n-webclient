import React from 'react'
import PropTypes from 'prop-types'

const ListHeader = ({ headers, sort, sortByColumn, children }) => {
  const { column, order } = sort
  return (
    <li>
      <dl>
        {headers.map((header) => {
          return (
            <React.Fragment key={header}>
              <dt>
                <span onClick={() => sortByColumn(header)}>
                  {header} {column === header ? <i className={`fa fa-sort-${order}`} /> : null}
                </span>
              </dt>
              <dd />
            </React.Fragment>
          )
        })}
      </dl>
      {children}
    </li>
  )
}

ListHeader.defaultProps = {
  sort: {
    column: 'name',
    order: 'down'
  }
}

ListHeader.propTypes = {
  headers: PropTypes.arrayOf(
    PropTypes.string
  ).isRequired,
  sort: PropTypes.objectOf(
    PropTypes.string
  )
}

export { ListHeader }
