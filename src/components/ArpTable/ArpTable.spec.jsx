// ArpTable.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { ArpTable } from './ArpTable.jsx'

describe('ArpTable', () => {
  let wrapper
  const props = {
    searchInput: '',
    device: {
      device: {
        arpTable: []
      }
    }
  }

  beforeEach(() => {
    wrapper = shallow(<ArpTable {...props} />)
  })

  it('should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render the list in full', () => {
    wrapper.setProps({
      searchInput: '',
      device: {
        device: {
          arpTable: [
            { protocol: 'Internet', address: '10.0.0.1', age: '-', macAddress: '00:00:00:00:00:00', type: 'ARPA', interface: 'Vlan1' },
            { protocol: 'Internet', address: '10.0.0.2', age: '-', macAddress: '00:00:00:00:00:01', type: 'ARPA', interface: 'Vlan666' }
          ]
        }
      }
    })

    expect(toJson(wrapper)).toMatchSnapshot()
  })
})
