import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import UserManualModal from '../UserManualModal/UserManualModal'
import ArpTableManual from '../Manuals/ArpTableManual'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class ArpTable extends Component {
  render () {
    const { device, searchInput } = this.props
    const { arpTable } = device.device
    const tableHeaders = arpTable.length ? Object.keys(arpTable[0]) : []

    // Applying search filter
    /* istanbul ignore next */
    const arpList = searchInput.length ? filterBySearchInput(arpTable, searchInput, tableHeaders) : arpTable

    return (
      <>
        <span>
          <h2>ARP Table</h2>
        </span>
        <UserManualModal
          title='ARP Table User Information'
          CurrentPageManual={ArpTableManual}
        />
        <ul className='list listArp'>
          <ListHeader headers={tableHeaders} />
          {arpList.map((mac) => {
            return (
              <ListItem key={mac.address}>
                <dl>
                  {Object.keys(mac).map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{mac[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
              </ListItem>
            )
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

ArpTable.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      arpTable: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}

ArpTable.defaultProps = {
  searchInput: ''
}

export { ArpTable }
export default connect(mapStateToProps)(ArpTable)
