// Deviceinfo.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { UserInfo } from './UserInfo.jsx'

describe('UserInfo', () => {
  let wrapper
  const props = {
    user: {
      id: 1,
      username: 'admin',
      roleId: 1
    },
    resources: {
      users: ['update'],
      roles: ['get']
    },
    updateUser: jest.fn(),
    onChange: jest.fn(),
    error: ''
  }

  const e = {
    preventDefault: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<UserInfo {... props} />)
  })

  it('should render correctly, given correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently given different props', () => {
    wrapper.setProps({
      ...props,
      resources: {
        users: [],
        roles: []
      }
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })

  it('should accept valid passwords ', () => {
    expect(toJson(wrapper)).toMatchSnapshot()

    wrapper.find('#password').simulate('change', { target: { name: 'password', value: 'admin' } })
    wrapper.find('#confirmPassword').simulate('change', { target: { name: 'confirmPassword', value: 'admin' } })

    wrapper.setState({ role: 1 })
    expect(wrapper.instance().validate()).toBe(true)

    wrapper.find('form').simulate('submit', e)
    expect(props.updateUser.mock.calls.length).toBe(1)
  })

  it('and reject invalid ones', () => {
    expect(toJson(wrapper)).toMatchSnapshot()

    wrapper.find('#password').simulate('change', { target: { name: 'password', value: 'admin' } })
    wrapper.find('#confirmPassword').simulate('change', { target: { name: 'confirmPassword', value: 'nimad' } })

    expect(wrapper.instance().validate()).toBe(false)

    wrapper.find('form').simulate('submit', e)
    expect(props.updateUser.mock.calls.length).toBe(0)
  })

  it('should call update user when no password is provided', () => {
    expect(toJson(wrapper)).toMatchSnapshot()

    wrapper.setState({ role: 1 })
    expect(wrapper.instance().validate()).toBe(true)

    wrapper.find('form').simulate('submit', e)
    expect(props.updateUser.mock.calls.length).toBe(1)
  })

  it('Should reject if roleId is 0', () => {
    expect(toJson(wrapper)).toMatchSnapshot()

    wrapper.setProps({
      ...props,
      user: {
        id: 1,
        username: 'admin',
        roleId: 0
      }
    })

    expect(wrapper.instance().validate()).toBe(false)

    wrapper.find('form').simulate('submit', e)
    expect(props.updateUser.mock.calls.length).toBe(0)
  })
})
