import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navigate } from 'react-router-dom'
import PropTypes from 'prop-types'

/* Actions */
import { onChange, updateUser } from '../../actions/userActions'
/* Components */
import RolesDropdown from '../RolesDropdown/RolesDropdown'
/* Styles */
import '../../styles/Main.less'

class UserInfo extends Component {
  constructor (props) {
    super(props)

    this.state = {
      redirect: false,
      password: '',
      confirmPassword: '',
      role: '',
      errors: {}
    }
  }

  onSubmit (e) {
    const { password } = this.state
    const { updateUser, user } = this.props
    e.preventDefault()
    const valid = this.validate()

    if (valid) {
      // If password is empty don't submit it
      updateUser(password !== '' ? { ...user, password } : { ...user })
      this.setState({ redirect: true })
    }
  }

  validate () {
    const { user } = this.props
    const { password, confirmPassword } = this.state

    const errors = {
      confirmPassword: password !== confirmPassword ? 'Password does not match' : null,
      role: parseInt(user.roleId) === 0 ? 'A role must be selected' : null
    }

    this.setState({ errors })

    return Object.keys(errors)
      .map((error) => { return errors[error] !== null ? 1 : 0 }) // Count errors
      .reduce((a, b) => { return a + b }) === 0 // Check if errors are over zero
  }

  render () {
    const { onChange, user, error, resources } = this.props
    const { password, confirmPassword, errors, redirect } = this.state
    const updateRestriction = resources.users.indexOf('update') !== -1

    return redirect
      ? <Navigate to='/users' />
      : (
        <form onSubmit={(e) => { this.onSubmit(e) }}>
          <div className='form-group'>
            <label className='control-label' htmlFor='username'>Username</label>
            <input
              className='form-control'
              id='username'
              name='username'
              type='text'
              value={user.username}
              placeholder='Username'
              disabled
            />

            <label className='control-label' htmlFor='password'>Password</label>
            <input
              className='form-control'
              id='password'
              name='password'
              type='password'
              value={password}
              disabled={!updateRestriction}
              onChange={(e) => this.setState({ password: e.target.value })}
            />

            <div className='text-danger'>{errors.password}</div>

            <label className='control-label' htmlFor='confirmPassword'>Confirm password</label>
            <input
              className='form-control'
              id='confirmPassword'
              name='confirmPassword'
              type='password'
              value={confirmPassword}
              disabled={!updateRestriction}
              onChange={(e) => this.setState({ confirmPassword: e.target.value })}
            />

            <div className='text-danger'>{errors.confirmPassword}</div>

            <label htmlFor='role' className='control-label'>Role</label>
            <RolesDropdown
              id={user.roleId ? user.roleId : 0}
              disabled={!updateRestriction}
              onChange={/* istanbul ignore next */(e) => { onChange({ target: { name: 'roleId', value: e.target.value } }) }}
              restriction={resources.roles.indexOf('list') === -1}
            />

            <div className='text-danger'>{errors.role}</div>
          </div>

          <span className='d-block text-danger'>{error}</span>
          {updateRestriction
            ? <button type='submit' className='btn btn-success saveButton'>Save</button>
            : null}
        </form>
        )
  }
}

/* Needless to test mapStateToProps */
/* istanbul ignore next */
const mapStateToProps = ({ user, auth }) => {
  return {
    user: user.user,
    error: user.error,
    resources: auth.resources
  }
}

UserInfo.propTypes = {
  onChange: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  user: PropTypes.shape({
    role: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    }),
    id: PropTypes.number,
    roleId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    username: PropTypes.string.isRequired,
    error: PropTypes.string
  }).isRequired,
  resources: PropTypes.object.isRequired
}

UserInfo.defaultProps = {
  user: {
    id: null,
    role: {},
    error: ''
  }
}

export { UserInfo }
export default connect(mapStateToProps, { onChange, updateUser })(UserInfo)
