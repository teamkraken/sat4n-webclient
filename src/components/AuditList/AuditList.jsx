import React from 'react'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

const AuditList = ({ logs, searchInput }) => {
  const tableHeaders = logs.length ? ['timestamp', 'action', 'details'] : []

  /* istanbul ignore next */
  const logList = searchInput.length ? filterBySearchInput(logs, searchInput, ['createdAt', 'action', 'details']) : logs
  return (
    <>
      <ul className='list listAudit'>
        <ListHeader headers={tableHeaders} />

        {logList.map((log) => {
        /* no need to test map functions */
        /* istanbul ignore next */
          return (
            <ListItem key={log.id}>
              <dl>
                <dt>timestamp</dt><dd>{new Date(log.createdAt).toLocaleString()}</dd>
                <dt>action</dt><dd>{log.action}</dd>
                <dt>details</dt><dd>{log.details}</dd>
              </dl>
            </ListItem>
          )
        })}
      </ul>
    </>
  )
}

AuditList.propTypes = {
  searchInput: PropTypes.string,
  logs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      action: PropTypes.string.isRequired,
      details: PropTypes.string.isRequired,
      userId: PropTypes.number,
      createdAt: PropTypes.string.isRequired,
      deviceId: PropTypes.number
    }).isRequired
  ).isRequired
}

AuditList.defaultProps = {
  searchInput: ''
}

export { AuditList }
