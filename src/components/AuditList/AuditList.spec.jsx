// AuditList.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { AuditList } from './AuditList'

describe('AuditList', () => {
  let wrapper
  const props = {
    logs: [
      {
        id: 1,
        action: 'Device:CREATE',
        details: 'Detailed info',
        userId: 1,
        createdAt: '01/01/2019',
        deviceId: 1
      }
    ],
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<AuditList {...props} />)
  })

  it('Should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently given different props', () => {
    wrapper.setProps({
      logs: [],
      searchInput: ''
    })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })
})
