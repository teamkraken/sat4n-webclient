import React from 'react'
import PropTypes from 'prop-types'

const FormInput = ({ id, name, type, value, label, error, onChange }) => (
  <>
    <label className='control-label text-secondary' htmlFor={id}>{label}</label>
    <input
      className='form-control'
      id={id}
      name={name}
      type={type}
      value={value}
      onChange={onChange}
      placeholder={label}
    />

    <div className='text-danger'>{error}</div>
  </>
)

FormInput.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'password']).isRequired,
  value: PropTypes.string,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

FormInput.defaultProps = {
  value: '',
  error: ''
}

export default FormInput
