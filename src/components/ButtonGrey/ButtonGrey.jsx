import React from 'react'
import PropTypes from 'prop-types'

const ButtonGrey = ({ onClick, label, type }) => {
  return (<button type={type} className='btn btn-md btn-secondary' onClick={onClick}>{label}</button>)
}

ButtonGrey.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  type: PropTypes.string
}

ButtonGrey.defaultProps = {
  type: 'button'
}

export default ButtonGrey
