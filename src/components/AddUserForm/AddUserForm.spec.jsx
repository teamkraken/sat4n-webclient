// AddUserForm.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { AddUserForm } from './AddUserForm'

// uncovered lines 8,28,73,95
describe('AddUserForm', () => {
  let wrapper

  const props = {
    showModal: false,
    visible: true,
    addUser: jest.fn(),
    onOpenModal: jest.fn(),
    onCloseModal: jest.fn(),
    user: {
      error: ''
    }
  }

  const e = {
    preventDefault: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<AddUserForm {...props} />)
  })

  it('should render correctly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should render differently when visible is false', () => {
    wrapper.setProps({ ...props, visible: false })
    wrapper.update()
    expect(wrapper).toMatchSnapshot()
  })

  it('it should call the proper actions given a successful submit call', () => {
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('FormInput[type="text"]').dive()
      .find('input[type="text"]')
      .simulate('change', { target: { name: 'username', value: 'janedoe' } })
    expect(wrapper.state().username).toEqual('janedoe')

    wrapper.find('FormInput[type="password"]').dive()
      .find('input[type="password"]')
      .simulate('change', { target: { name: 'password', value: 'password' } })
    expect(wrapper.state().password).toEqual('password')

    // Assume a role has been selected
    wrapper.instance().setState({ roleId: '1' })

    wrapper.find('AddButton[label="Add"]').dive()
      .find('button[type="submit"]')
      .simulate('click')
    wrapper.find('form').simulate('submit', e)

    expect(props.addUser.mock.calls.length).toBe(1)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('it should call the proper actions given a unsuccessful submit call', () => {
    expect(toJson(wrapper)).toBeDefined()

    wrapper.find('AddButton[label="Add"]').dive()
      .find('button[type="submit"]')
      .simulate('click')
    wrapper.instance().handleSubmit(e)

    expect(wrapper.state().errors)
      .toEqual({
        password: 'Password can not be empty',
        role: 'A role must be selected',
        username: 'Name can not be empty'
      })

    expect(props.addUser.mock.calls.length).toBe(0)
    expect(wrapper.instance().props.showModal).toBe(false)
  })

  it('Should invoke onCloseModal', () => {
    wrapper.instance().handleClose()
    expect(props.onCloseModal.mock.calls.length).toBe(1)
    wrapper.instance().handleSubmit(e)
    expect(props.onCloseModal.mock.calls.length).toBe(1)
  })
})
