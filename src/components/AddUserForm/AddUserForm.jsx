import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ReactModal from 'react-modal'

/* Actions */
import { addUser } from '../../actions/userActions'
/* Components */
import FormInput from '../FormInput/FormInput'
import GreenButton from '../ButtonGreen/ButtonGreen'
import GreyButton from '../ButtonGrey/ButtonGrey'
import RolesDropdown from '../RolesDropdown/RolesDropdown'

class AddUserForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      roleId: '',
      errors: {}
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  handleClose () {
    const { onCloseModal } = this.props
    this.setState({ username: '', password: '', roleId: '', errors: {} })
    onCloseModal('close')
  }

  handleSubmit (e) {
    e.preventDefault()
    const { addUser, onCloseModal } = this.props
    const { username, password, roleId } = this.state
    const valid = this.validate()

    if (valid) {
      addUser({
        username,
        password,
        roleId
      })
      onCloseModal('submit')
      this.setState({ username: '', password: '', roleId: '', errors: {} })
    }
  }

  handleChange (e) {
    const { name, value } = e.target

    return this.setState({ [name]: value })
  }

  validate () {
    const { username, password, roleId } = this.state

    const errors = {
      username: username === '' ? 'Name can not be empty' : null,
      password: password === '' ? 'Password can not be empty' : null,
      role: roleId === '' ? 'A role must be selected' : null
    }

    this.setState({ errors })

    /* istanbul ignore next */
    return Object.keys(errors)
      .map((error) => { return errors[error] !== null ? 1 : 0 }) // Count errors
      .reduce((a, b) => { return a + b }) === 0 // Check if errors are over zero
  }

  render () {
    const { username, password, errors, roleId } = this.state
    const { onOpenModal, visible, user } = this.props
    const { error } = user

    return visible
      ? (
        <>
          <GreenButton onClick={onOpenModal} label='Add User' />

          <ReactModal
            isOpen={this.props.showModal}
            onClose={this.handleClose}
            contentLabel='Add User'
            className='modal-dialog'
            overlayClassName='modal-backdrop'
            style={{
              overlay: { backgroundColor: 'rgba(0, 0, 0, 0.75)' }
            }}
          >
            <div className='modal-content'>
              <div className='modal-header'>
                <h5 className='modal-title' id='addUserModalLabel'>Add User</h5>
                <h5 className='text-danger'>{error}</h5>
              </div>
              <div className='modal-body'>
                <form onSubmit={(e) => { this.handleSubmit(e) }}>
                  <div className='form-group'>

                    <FormInput
                      id='username'
                      name='username'
                      type='text'
                      value={username}
                      label='Username'
                      error={errors.username}
                      onChange={(e) => { this.handleChange(e) }}
                    />

                    <FormInput
                      id='password'
                      name='password'
                      type='password'
                      value={password}
                      label='Password'
                      error={errors.password}
                      onChange={(e) => { this.handleChange(e) }}
                    />

                    <label htmlFor='role' className='control-label text-secondary'>Role</label>
                    {/* istanbul ignore next */}
                    <RolesDropdown
                      id={roleId}
                      onChange={/* istanbul ignore next */(e) => this.setState({ roleId: e.target.value })}
                    />

                    <div className='text-danger'>{errors.role}</div>
                  </div>
                  <div className='modal-footer'>
                    <GreyButton label='Close' onClick={this.handleClose} />
                    <GreenButton label='Add' type='submit' />
                  </div>
                </form>
              </div>
            </div>
          </ReactModal>
        </>
        )
      : null
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ user }) => {
  return { user }
}

AddUserForm.propTypes = {
  onCloseModal: PropTypes.func.isRequired,
  onOpenModal: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  visible: PropTypes.bool.isRequired,
  addUser: PropTypes.func.isRequired,
  user: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired
}

export { AddUserForm }
export default connect(mapStateToProps, { addUser })(AddUserForm)
