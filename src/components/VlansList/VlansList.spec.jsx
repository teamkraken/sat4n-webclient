// VlansList.jsx
/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { VlansList } from './VlansList.jsx'

describe('VlansList', () => {
  let wrapper
  const props = {
    device: {
      device: {
        vlans: [
          { id: 1, vlan: 'vlan1', name: 'testyVlan2', ports: [] },
          { id: 3, vlan: 'vlan3', name: 'testyVlan1', ports: [] },
          { id: 2, vlan: 'vlan2', name: 'testyVlan3', ports: [] }
        ]
      }
    },
    searchInput: ''
  }

  beforeEach(() => {
    wrapper = shallow(<VlansList {...props} />)
  })

  it('should render properly given the correct props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should render the list in full', () => {
    wrapper.setProps({
      searchInput: '',
      device: {
        device: {
          vlans: [
            { id: 1, vlan: 'vlan1', name: 'testyVlan', ports: [] },
            { id: 2, vlan: 'vlan2', name: 'testyVlan2', ports: [] }
          ]
        }
      }
    })
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('Should change sort order', () => {
    // Expected list sorted descending by username
    const expectedList = [
      { id: 3, vlan: 'vlan3', name: 'testyVlan1', ports: [] },
      { id: 2, vlan: 'vlan2', name: 'testyVlan3', ports: [] },
      { id: 1, vlan: 'vlan1', name: 'testyVlan2', ports: [] }
    ]

    wrapper.instance().sortByColumn('id')
    expect(wrapper.instance().state.sort.order === 'up')
    expect(wrapper.instance().props.device.device.vlans).toEqual(expectedList)
  })

  it('Should change sort column', () => {
    // Expected list sorted by description
    const expectedList = [
      { id: 3, vlan: 'vlan3', name: 'testyVlan1', ports: [] },
      { id: 1, vlan: 'vlan1', name: 'testyVlan2', ports: [] },
      { id: 2, vlan: 'vlan2', name: 'testyVlan3', ports: [] }
    ]

    wrapper.instance().sortByColumn('name')
    expect(wrapper.instance().props.device.device.vlans).toEqual(expectedList)
  })
})
