import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Components */
import { ListHeader } from '../ListHeader/ListHeader'
import { ListItem } from '../ListItem/ListItem'
import UserManualModal from '../UserManualModal/UserManualModal'
import VlansManual from '../Manuals/VlansManual'
/* Utilities */
import { filterBySearchInput } from '../../utils'
/* Styles */
import '../../styles/Main.less'

class VlansList extends Component {
  constructor (props) {
    super(props)

    this.state = {
      sort: {
        column: 'id',
        order: 'down'
      }
    }
  }

  sortVlans (array) {
    const { sort } = this.state
    const { column, order } = sort

    return array.sort((a, b) => {
      // If column is IP pad number with zeros
      const left = column !== 'id' ? a[column].toLowerCase() : a[column]
      const right = column !== 'id' ? b[column].toLowerCase() : b[column]

      if (left < right) {
        return order === 'down' ? -1 : 1
      }

      if (left > right) {
        return order === 'down' ? 1 : -1
      }

      return 0
    })
  }

  sortByColumn (newColumn) {
    const { sort } = this.state
    const { column, order } = sort

    this.setState({
      sort: {
        column: newColumn,
        order: column === newColumn && order === 'down' ? 'up' : 'down'
      }
    })
  }

  render () {
    const { sort } = this.state
    const { device, searchInput } = this.props
    const { vlans } = device.device
    const tableHeaders = vlans.length ? Object.keys(vlans[0]) : []

    // Applying search filter
    /* istanbul ignore next */
    const vlanList = searchInput.length ? filterBySearchInput(this.sortVlans(vlans), searchInput, tableHeaders) : this.sortVlans(vlans)

    return (
      <>
        <span>
          <h2 className='text-light'>Vlans</h2>
        </span>
        <UserManualModal
          title='Vlans User Information'
          CurrentPageManual={VlansManual}
        />
        <ul className='list listVlans'>
          <ListHeader headers={tableHeaders} sort={sort} sortByColumn={(column) => { this.sortByColumn(column) }} />

          {vlanList.map((vlan) => {
            return (
              <ListItem key={vlan.id}>
                <dl>
                  {Object.keys(vlan).map((key) => {
                    return (
                      <React.Fragment key={key}>
                        <dt>{key}</dt><dd>{vlan[key]}</dd>
                      </React.Fragment>
                    )
                  })}
                </dl>
              </ListItem>
            )
          })}
        </ul>
      </>
    )
  }
}
/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ device }) => {
  return { device }
}

VlansList.propTypes = {
  searchInput: PropTypes.string,
  device: PropTypes.shape({
    device: PropTypes.shape({
      vlans: PropTypes.array.isRequired
    }).isRequired
  }).isRequired
}

VlansList.defaultProps = {
  searchInput: ''
}

export { VlansList }
export default connect(mapStateToProps)(VlansList)
