import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

/* Actions */
import { clearErrors, getDevices } from '../../actions/deviceActions'
/* Components */
import AddDeviceForm from '../AddDeviceForm/AddDeviceForm'
import DevicesList from '../DevicesList/DevicesList'
import ErrorForbidden from '../ErrorForbidden/ErrorForbidden'
import SearchBar from '../SearchBar/SearchBar'
import UserManualModal from '../UserManualModal/UserManualModal'
import DevicesPageManual from '../Manuals/DevicesPageManual'
/* Styles */
import '../../styles/Main.less'

class DevicesPage extends Component {
  constructor (props) {
    super(props)

    this.state = {
      showModal: false,
      search: ''
    }
    this.handleInput = this.handleInput.bind(this)
    this.handleOpenModal = this.handleOpenModal.bind(this)
    this.handleCloseModal = this.handleCloseModal.bind(this)
  }

  componentDidMount () {
    const { getDevices } = this.props

    getDevices()
  }

  handleOpenModal () {
    this.setState({ showModal: true })
  }

  handleCloseModal (type) {
    const { clearErrors } = this.props

    if (type === 'close') {
      clearErrors()
    }

    this.setState({ showModal: false })
  }

  handleInput (e) {
    this.setState({ ...this.state, search: e.target.value })
  }

  render () {
    const { search } = this.state
    const { device, auth } = this.props
    const { error } = device

    return error.includes('GET')
      ? (<ErrorForbidden errorMessage={error} />)
      : (
        <div className='pageContainer'>
          <span>
            <h2>Devices</h2>
            <AddDeviceForm
              onOpenModal={this.handleOpenModal}
              onCloseModal={this.handleCloseModal}
              showModal={this.state.showModal || error.includes('CREATE')}
              visible={auth.resources.devices.indexOf('create') !== -1}
            />
          </span>
          <UserManualModal
            title='Device Page User Information'
            CurrentPageManual={DevicesPageManual}
          />
          <SearchBar onInput={this.handleInput} />
          <DevicesList searchInput={search} />
        </div>
        )
  }
}

/* no need to test mapStateToProps functions */
/* istanbul ignore next */
const mapStateToProps = ({ auth, device }) => {
  return { auth, device }
}

DevicesPage.propTypes = {
  clearErrors: PropTypes.func.isRequired,
  getDevices: PropTypes.func.isRequired,
  device: PropTypes.shape({
    error: PropTypes.string.isRequired
  }).isRequired,
  auth: PropTypes.shape({
    resources: PropTypes.object.isRequired
  }).isRequired
}

export { DevicesPage }
export default connect(mapStateToProps, { clearErrors, getDevices })(DevicesPage)
