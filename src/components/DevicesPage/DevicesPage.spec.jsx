// HomePage.jsx
/* eslint-env jest */

import React from 'react'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import { DevicesPage } from './DevicesPage'

describe('DevicesPage', () => {
  let wrapper
  const props = {
    auth: {
      resources: {
        devices: ['create']
      }
    },
    device: {
      error: ''
    },
    getDevices: jest.fn(),
    clearErrors: jest.fn()
  }

  beforeEach(() => {
    wrapper = shallow(<DevicesPage {...props} />)
  })

  it('should render correctly given the right props', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
    expect(wrapper.instance().props.getDevices.mock.calls.length).toBe(1)
  })

  it('Should change showModal state to true/false', () => {
    expect(wrapper.state().showModal).toBe(false)

    wrapper.instance().handleOpenModal()
    expect(wrapper.state().showModal).toBe(true)

    wrapper.instance().handleCloseModal('close')
    expect(wrapper.state().showModal).toBe(false)
  })

  it('Should change state on input', () => {
    wrapper.instance().handleInput({ target: { value: 'search input' } })

    expect(wrapper.state().search).toEqual('search input')
  })

  it('Should clearErrors when modal is closed', () => {
    wrapper.instance().handleCloseModal()
    expect(wrapper.instance().props.clearErrors.mock.calls.length).toBe(0)

    wrapper.instance().handleCloseModal('close')
    expect(wrapper.instance().props.clearErrors.mock.calls.length).toBe(1)
    expect(wrapper.state().showModal).toBe(false)
  })

  it('Should render differently on error', () => {
    wrapper.setProps({ ...props, device: { error: 'GET' } })
    wrapper.update()

    expect(wrapper).toMatchSnapshot()
  })
})
