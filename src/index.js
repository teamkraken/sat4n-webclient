import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { configureStore } from '@reduxjs/toolkit'
import { applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

/* App */
import App from './App'
/* Reducers */
import reducers from './reducers'
/* Styles */
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap'
// import './styles/Main.css'
import './styles/Main.less'
/* Icons */
// import '../public/favicon.ico'

const domNode = document.getElementById('root')
const root = createRoot(domNode)

root.render(
  <Provider store={configureStore({ reducer: reducers }, applyMiddleware(thunk))}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
)
