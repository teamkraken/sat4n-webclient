import React from 'react'
import { useParams } from 'react-router-dom'

export /**
 *  Filter array depending on search input
 * @param {array} arr array of objects
 * @param {string} searchString string to search by
 * @param {array} includedKeys array of fields included in search
 * @returns Filtered array
 */
const filterBySearchInput = (arr, searchString, includedKeys) => {
  const stringToLower = searchString.toLowerCase()

  return arr.filter(obj => { // iterates through all objects in ther array
    return includedKeys.map(key => {
      return obj[key] !== null
        ? obj[key].toString().toLowerCase().includes(stringToLower)
        : false // checks each value in object, if it includes the search string returns true, otherwise false
    }).includes(true) // map will return array of bool values. Check if the array includes true, if so one or more fields in the object include the search string
  })
}

export function withRouter (Children) {
  return (props) => {
    const match = { params: useParams() }
    return <Children {...props} match={match} />
  }
}
