import React from 'react'
import ReactModal from 'react-modal'
import decode from 'jwt-decode'
import { Route, Routes, Navigate } from 'react-router-dom'

/* Components */
import AuditPage from './components/AuditPage/AuditPage'
import DevicePage from './components/DevicePage/DevicePage'
import DevicesPage from './components/DevicesPage/DevicesPage'
import LoginPage from './components/LoginPage/LoginPage'
import NavigationBar from './components/NavigationBar/NavigationBar'
import PageNotFound from './components/ErrorPageNotFound/ErrorPageNotFound'
import RolePage from './components/RolePage/RolePage'
import RolesPage from './components/RolesPage/RolesPage'
import UserPage from './components/UserPage/UserPage'
import UsersPage from './components/UsersPage/UsersPage'

/*  Disable during tests, the screenreading feature fails the tests because the component tries
to anchor the Modal on a non existing node #app or #root */
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'build') {
  ReactModal.setAppElement('#root')
}

// Check if token has expired and remove if needed
if (window.localStorage.sat4nJWT) {
  const decoded = decode(window.localStorage.sat4nJWT)

  if (decoded.exp < Date.now() / 1000) {
    console.log('LOGOUT!')
    window.localStorage.removeItem('sat4nJWT')
  }
}

const ProtectedRoute = ({ user, children }) => {
  if (!window.localStorage.sat4nJWT) {
    return <Navigate to='/' replace />
  }

  return (
    <>
      <NavigationBar />
      {children}
    </>
  )
}

const App = () => (
  <Routes>
    <Route index element={<LoginPage />} />
    <Route path='/login' element={<LoginPage />} />
    <Route
      path='/devices'
      element={
        <ProtectedRoute>
          <DevicesPage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/devices/:id'
      element={
        <ProtectedRoute>
          <DevicePage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/roles'
      element={
        <ProtectedRoute>
          <RolesPage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/roles/:id'
      element={
        <ProtectedRoute>
          <RolePage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/users'
      element={
        <ProtectedRoute>
          <UsersPage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/users/:id'
      element={
        <ProtectedRoute>
          <UserPage />
        </ProtectedRoute>
      }
    />
    <Route
      path='/audit'
      element={
        <ProtectedRoute>
          <AuditPage />
        </ProtectedRoute>
      }
    />
    <Route
      path='*'
      element={
        <ProtectedRoute>
          <PageNotFound />
        </ProtectedRoute>
      }
    />
  </Routes>
)

/*
const App = () => (
  <Routes>
    <Route exact path='/' element={<LoginPage />} />
    <Route exact path='/login' element={<LoginPage />} />
    <Route exact path='/devices' element={<DefaultContainer />} />
    <Route exact path='/devices/:id' element={<DefaultContainer />} />
    <Route exact path='/roles' element={<DefaultContainer />} />
    <Route exact path='/roles/:id' element={<DefaultContainer />} />
    <Route exact path='/users' element={<DefaultContainer />} />
    <Route exact path='/users/:id' element={<DefaultContainer />} />
    <Route exact path='/audit' element={<DefaultContainer />} />
    <Route path='*' element={<NotFoundContainer />} />
  </Routes>
)

const DefaultContainer = () => (
  window.localStorage.sat4nJWT
    ? (
      <>
        <NavigationBar />
        <Route exact path='/devices' element={<DevicesPage />} />
        <Route exact path='/devices/:id' element={<DevicePage />} />
        <Route exact path='/roles' element={<RolesPage />} />
        <Route exact path='/roles/:id' element={<RolePage />} />
        <Route exact path='/users' element={<UsersPage />} />
        <Route exact path='/users/:id' element={<UserPage />} />
        <Route exact path='/audit' element={<AuditPage />} />
      </>
      )
    : (
      <Navigate to='/' />
      )
)

const NotFoundContainer = () => (
  window.localStorage.sat4nJWT
    ? (
      <>
        <NavigationBar />
        <PageNotFound />
      </>
      )
    : (
      <Navigate to='/' />
      )
)
*/

/*
const App = () => {
  let element = useRoutes([
    // These are the same as the props you provide to <Route>
    { path: "/", element: <LoginPage /> },
    { path: "login", element: <LoginPage /> },
    {
      path: "invoices",
      element: <Invoices />,
      // Nested routes use a children property, which is also
      // the same as <Route>
      children: [
        { path: ":id", element: <Invoice /> },
        { path: "sent", element: <SentInvoices /> },
      ],
    },
    // Not found routes work as you'd expect
    //{ path: "*", element: <NotFound /> },
  ])

  // The returned element will render the entire element
  // hierarchy with all the appropriate context it needs
  return element;
}
*/

// export { DefaultContainer, NotFoundContainer }
export default App
