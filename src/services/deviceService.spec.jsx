// deviceService.jsx
/* eslint-env jest */

import axios from 'axios'
import deviceService from './deviceService'

jest.mock('axios')

describe('deviceService functions', () => {
  const device = { id: 1, ip: '10.100.66.10' }
  const newDevice = { id: 2, ip: '10.100.100.10' }
  const updateDevice = { id: 2, ip: '10.100.100.10', vlanId: 2 }
  const devices = [device]
  const deviceID = 1
  const portID = 1
  const err = {
    response: {
      status: 401,
      data: 'Unauthorized'
    }
  }

  /* -----------------------------Testing getDevice----------------------------- */
  it('getDevice should match a mock request', () => {
    const res = { data: device }
    axios.get.mockResolvedValue(res)
    deviceService.getDevice(1, 'token').then(res => expect(res).toEqual(device))
  })

  it('getDevice should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    deviceService.getDevice(1, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* --------------------------------------------------------------------------- */

  /* -----------------------------Testing getDevices----------------------------- */
  it('getDevices should match a mock request', () => {
    const res = { data: devices }
    axios.get.mockResolvedValue(res)
    deviceService.getDevices('token').then(res => expect(res).toEqual(devices))
  })

  it('getDevices should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    deviceService.getDevices('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------- */

  /* -----------------------------Testing addDevice----------------------------- */
  it('addDevice should match a mock request', () => {
    const res = { data: newDevice }
    axios.post.mockResolvedValue(res)
    deviceService.addDevice(newDevice, 'token').then(res => expect(res).toEqual(newDevice))
  })

  it('addDevice should fail when unauthorized', () => {
    axios.post.mockRejectedValue(err)
    deviceService.addDevice(newDevice, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* --------------------------------------------------------------------------- */

  /* -----------------------------Testing deleteDevice----------------------------- */
  it('deleteDevice should match a mock request', () => {
    const res = { data: 1 }
    axios.delete.mockResolvedValue(res)
    deviceService.deleteDevice(2, 'token').then(res => expect(res).toEqual(1))
  })

  it('deleteDevice should fail when unauthorized', () => {
    axios.delete.mockRejectedValue(err)
    deviceService.deleteDevice(2, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ------------------------------------------------------------------------------ */

  /* -----------------------------Testing restartDevicePort----------------------------- */
  it('restartDevicePort should match a mock request', () => {
    const res = { data: 1 }
    axios.get.mockResolvedValue(res)
    deviceService.restartDevicePort(deviceID, portID, 'token').then(res => expect(res).toEqual(1))
  })

  it('restartDevicePort should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    deviceService.restartDevicePort(deviceID, portID, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ----------------------------------------------------------------------------------- */

  /* -----------------------------Testing disableDevicePort----------------------------- */
  it('disableDevicePort should match a mock request', () => {
    const res = { data: 1 }
    axios.get.mockResolvedValue(res)
    deviceService.disableDevicePort(deviceID, portID, 'token').then(res => expect(res).toEqual(1))
  })

  it('disableDevicePort should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    deviceService.disableDevicePort(deviceID, portID, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ----------------------------------------------------------------------------------- */

  /* -----------------------------Testing enableDevicePort----------------------------- */
  it('enableDevicePort should match a mock request', () => {
    const res = { data: 1 }
    axios.get.mockResolvedValue(res)
    deviceService.enableDevicePort(deviceID, portID, 'token').then(res => expect(res).toEqual(1))
  })

  it('enableDevicePort should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    deviceService.enableDevicePort(deviceID, portID, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ----------------------------------------------------------------------------------- */

  /* -----------------------------Testing updateDevicePort----------------------------- */
  it('updateDevicePort should match a mock request', () => {
    const vlanID = 2
    const res = { data: updateDevice }
    axios.put.mockResolvedValue(res)
    deviceService.updateDevicePort(deviceID, portID, vlanID, 'token').then(res => expect(res).toEqual(updateDevice))
  })

  it('updateDevicePort should fail when unauthorized', () => {
    const vlanID = 2
    axios.put.mockRejectedValue(err)
    deviceService.updateDevicePort(deviceID, portID, vlanID, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ----------------------------------------------------------------------------------- */
})
