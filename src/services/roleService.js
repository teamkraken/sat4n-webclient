import axios from 'axios'

const HOSTNAME = window.location.hostname
const PROTOCOL = window.location.protocol
const PORT = window.location.port

const encodeForm = (data) => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

const roleService = () => ({
  /**
  * Sends an HTTP POST request to the server
  * @param {Object} role role object to be created
  * @param {string} token role authorization token
  * @returns
  */
  addRole: (role, token) => axios.post(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/roles`, encodeForm(role), { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP DELETE request to the server
  * @param {number} id id of a role to be deleted
  * @param {string} token role authorization token
  */
  deleteRole: (id, token) => axios.delete(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/roles/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP GET request to the server
  * @param {string} token role authorization token
  * @returns
  */
  getRole: (id, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/roles/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP GET request to the server
  * @param {string} token role authorization token
  * @returns
  */
  getRoles: (token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/roles`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP PUT request to the server
  * @param {Object} role role object to be created
  * @param {string} token role authorization token
  * @returns
  */
  updateRole: (role, token) => axios.put(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/roles/${role.id}`, role, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw Error(`${err.response.status} ${err.response.data}`)
    })
})

export default roleService()
