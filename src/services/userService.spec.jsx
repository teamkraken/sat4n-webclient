// userService.jsx
/* eslint-env jest */
import axios from 'axios'
import userService from './userService'

jest.mock('axios')

describe('UserService functions', () => {
  const user = { id: 1, username: 'admin' }
  const users = [user]
  const newUser = { id: 3, username: 'testus' }
  const updateUser = { id: 3, username: 'testos' }
  const err = {
    response: {
      status: 401,
      data: 'Unauthorized'
    }
  }

  /* -----------------------------Testing getUser----------------------------- */
  it('getUser should match a mock request', () => {
    const res = { data: user }
    axios.get.mockResolvedValue(res)
    return userService.getUser(1, 'token').then(res => expect(res).toEqual(user))
  })

  it('getUser should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    return userService.getUser(1, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ------------------------------------------------------------------------- */

  /* -----------------------------Testing getUsers----------------------------- */
  it('getUsers should match a mock request', () => {
    const res = { data: users }
    axios.get.mockResolvedValue(res)
    return userService.getUsers('token').then(res => expect(res).toEqual(users))
  })

  it('getUsers should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    userService.getUsers('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* -------------------------------------------------------------------------- */

  /* -----------------------------Testing addUser----------------------------- */
  it('addUser should match a mock request', () => {
    const res = { data: newUser }
    axios.post.mockResolvedValue(res)
    userService.addUser(newUser, 'token').then(res => expect(res).toEqual(newUser))
  })

  it('addUser should fail when unauthorized', () => {
    axios.post.mockRejectedValue(err)
    userService.addUser(newUser, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ------------------------------------------------------------------------- */

  /* -----------------------------Testing deleteUser----------------------------- */
  it('deleteUser should match a mock request', () => {
    const res = { data: 1 }
    axios.delete.mockResolvedValue(res)
    userService.deleteUser(3, 'token').then(res => expect(res).toEqual(1))
  })

  it('deleteUser should fail when unauthorized', () => {
    axios.delete.mockRejectedValue(err)
    userService.deleteUser(3, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------- */

  /* -----------------------------Testing updateUser----------------------------- */
  it('updateUser should match a mock request', () => {
    const res = { data: { username: 'testos' } }
    axios.put.mockResolvedValue(res)
    userService.updateUser(updateUser, 'token').then(res => expect(res).toEqual({ username: 'testos' }))
  })

  it('updateUser should fail when unauthorized', () => {
    axios.put.mockRejectedValue(err)
    userService.updateUser(updateUser, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------- */
})
