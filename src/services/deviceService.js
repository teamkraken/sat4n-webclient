import axios from 'axios'

const HOSTNAME = window.location.hostname
const PROTOCOL = window.location.protocol
const PORT = window.location.port

/**
 * Handles all HTTP requests to be sent to the server
 */
const deviceService = () => ({
  /**
   * Requests HTTP GET request to the server
   * Gets a single device by id
   * @param {number} id the id of the device to get
   * @param {string} token the user's JWT token
   * @returns JSON with device info
   */
  getDevice: (id, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Request HTTP GET request to the server
   * Gets all devices
   * @param {string} token the user's JWT token
   * @returns a list of devices
   */
  getDevices: (token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP DELETE request to the server
  * @param {number} id id of a device to be deleted
  * @param {string} token User authorization token
  */
  deleteDevice: (id, token) => axios.delete(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP POST request to the server
  * @param {string} token User authorization token
  */
  addDevice: (device, token) => axios.post(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices`, device, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Requests HTTP GET request to the server
   * Gets a single device by id
   * @param {number} deviceID the id of the device
   * @param {number} portID the id of the port to restart
   * @param {string} token the user's JWT token
   * @returns JSON with device info
   */
  restartDevicePort: (deviceID, portID, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${deviceID}/ports/${portID}/restart`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Requests HTTP GET request to the server
   * Gets a single device by id
   * @param {number} deviceID the id of the device
   * @param {number} portID the id of the port to disable
   * @param {string} token the user's JWT token
   * @returns JSON with device info
   */
  disableDevicePort: (deviceID, portID, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${deviceID}/ports/${portID}/disable`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Requests HTTP GET request to the server
   * Gets a single device by id
   * @param {number} deviceID the id of the device
   * @param {number} portID the id of the port to enable
   * @param {string} token the user's JWT token
   * @returns JSON with device info
   */
  enableDevicePort: (deviceID, portID, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${deviceID}/ports/${portID}/enable`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Requests HTTP GET request to the server
   * Gets a single device by id
   * @param {number} deviceID id of the device
   * @param {number} portID id of the port to update
   * @param {number} vlanID id of the vlan to be applied to the port
   * @param {string} token the user's JWT token
   * @returns JSON with device info
   */
  updateDevicePort: (deviceID, portID, vlanID, token) => axios.put(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${deviceID}/ports/${portID}/`, { vlan: vlanID }, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    })
})

export default deviceService()
