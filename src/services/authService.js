import axios from 'axios'

const HOSTNAME = window.location.hostname
const PROTOCOL = window.location.protocol
const PORT = window.location.port

/**
 * url encodes the data to be sent to the server
 * @param {object} data to send to the server
 * @returns data url encoded
 */
/* No need to test standard JS methods */
/* istanbul ignore next */
const encodeForm = (data) => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

const authService = () => ({
  /**
   * Handles authentication to the sever
   * @param {object} credentials the credentials the user provides
   * @returns {object} object containing username and authentication token if successfull,
   *                   otherwise throws an error
   */
  login: (credentials) => axios.post(`${PROTOCOL}//${HOSTNAME}:${PORT}/auth`, encodeForm(credentials), { headers: { Accept: 'application/json' } })
    .then((res) => { return res.data })
    .catch(err => {
      throw new Error(`${err.response.status}: ${err.response.data}`)
    })
})

export default authService()
