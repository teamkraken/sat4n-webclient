import axios from 'axios'

const HOSTNAME = window.location.hostname
const PROTOCOL = window.location.protocol
const PORT = window.location.port
/* No need to test standard JS methods */
/* istanbul ignore next */
/* const encodeForm = (data) => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
} */

/**
 * Handles all HTTP requests to be sent to the server
 */
const auditService = () => ({

  /**
   * Sends HTTP GET request to the server
   * Gets all audit logs
   * @param {string} token the user's JWT token
   * @returns a list of audit logs
   */
  getAudit: (token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/audit`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Sends HTTP GET request to the server
   * Gets all audit logs associated with a specific user
   * @param {string} token the user's JWT token
   * @param {number} id id of user associated with audit logs to get
   * @returns a list of audit logs associated with a specific user
   */
  getAuditByUserId: (token, id) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users/${id}/audit`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
   * Sends HTTP GET request to the server
   * Gets all audit logs associated with a specific device
   * @param {string} token the user's JWT token
   * @param {number} id id of device associated with audit logs to get
   * @returns a list of audit logs associated with a specific device
   */
  getAuditByDeviceId: (token, id) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/devices/${id}/audit`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    })
})

export default auditService()
