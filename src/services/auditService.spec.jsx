// auditService
/* eslint-env jest */

import axios from 'axios'
import auditService from './auditService'

jest.mock('axios')

describe('auditService functions', () => {
  const logs = [
    { id: 1, action: 'Device:CREATE', details: 'Detailed info', createdAt: '01/01/2019', userId: 1, deviceId: 1 }
  ]
  const res = { data: logs }
  const err = {
    response: {
      status: 401,
      data: 'Unauthorized'
    }
  }

  /* -----------------------------Testing getAudit----------------------------- */
  it('getAudit should match a mock request', () => {
    axios.get.mockResolvedValue(res)
    auditService.getAudit('token').then(res => expect(res).toEqual(logs))
  })

  it('getAudit should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    auditService.getAudit('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* -------------------------------------------------------------------------- */

  /* -----------------------------Testing getAuditByUserId----------------------------- */
  it('getAuditByUserId should match a mock reques', () => {
    axios.get.mockResolvedValue(res)
    auditService.getAuditByUserId(1, 'token').then(res => expect(res).toEqual(logs))
  })

  it('getAuditByUserId should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    auditService.getAuditByUserId('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------------- */

  /* -----------------------------Testing getAuditByDeviceId----------------------------- */
  it('getAuditByDeviceId should match a mock reques', () => {
    axios.get.mockResolvedValue(res)
    auditService.getAuditByDeviceId(1, 'token').then(res => expect(res).toEqual(logs))
  })

  it('getAuditByDeviceId should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    auditService.getAuditByDeviceId('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------------- */
})
