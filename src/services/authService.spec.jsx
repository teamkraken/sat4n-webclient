// authService.jsx
/* eslint-env jest */

import axios from 'axios'
import authService from './authService'

jest.mock('axios')

describe('deviceService functions', () => {
  it('addDevice should match a mock request', () => {
    const cred = { username: 'admin', password: 'admin' }
    const res = { data: 'token' }

    axios.post.mockResolvedValue(res)
    authService.login(cred).then(res => expect(res).toEqual('token'))
  })

  it('addDevice should fail given wrong credentials', () => {
    const cred = { username: 'wrong', password: 'wrong' }
    const err = {
      response: {
        status: 401,
        data: 'Unauthorized'
      }
    }

    axios.post.mockRejectedValue(err)
    authService.login(cred).catch(res => expect(res.message).toEqual('401: Unauthorized'))
  })
})
