// roleService.jsx
/* eslint-env jest */
import axios from 'axios'
import roleService from './roleService'

jest.mock('axios')

describe('RoleService functions', () => {
  const role = { id: 1, name: 'Admins' }
  const roles = [role]
  const newRole = { id: 2, name: 'Users' }
  const updateRole = { id: 2, name: 'SuperUsers' }
  const err = {
    response: {
      status: 401,
      data: 'Unauthorized'
    }
  }
  /* -----------------------------Testing getRole----------------------------- */
  it('getRole should match a mock request', () => {
    const res = { data: role }
    axios.get.mockResolvedValue(res)
    return roleService.getRole(1, 'token').then(res => expect(res).toEqual(role))
  })

  it('getRole should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    return roleService.getRole(1, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ------------------------------------------------------------------------- */

  /* -----------------------------Testing getRoles----------------------------- */
  it('getRoles should match a mock request', () => {
    const res = { data: roles }
    axios.get.mockResolvedValue(res)
    return roleService.getRoles('token').then(res => expect(res).toEqual(roles))
  })

  it('getRoles should fail when unauthorized', () => {
    axios.get.mockRejectedValue(err)
    roleService.getRoles('').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* -------------------------------------------------------------------------- */

  /* -----------------------------Testing addRole----------------------------- */
  it('addRole should match a mock request', () => {
    const res = { data: newRole }
    axios.post.mockResolvedValue(res)
    roleService.addRole(newRole, 'token').then(res => expect(res).toEqual(newRole))
  })

  it('addRole should fail when unauthorized', () => {
    axios.post.mockRejectedValue(err)
    roleService.addRole(newRole, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ------------------------------------------------------------------------- */

  /* -----------------------------Testing deleteRole----------------------------- */
  it('deleteRole should match a mock request', () => {
    const res = { data: 1 }
    axios.delete.mockResolvedValue(res)
    roleService.deleteRole(2, 'token').then(res => expect(res).toEqual(1))
  })

  it('deleteRole should fail when unauthorized', () => {
    axios.delete.mockRejectedValue(err)
    roleService.deleteRole(2, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------- */

  /* -----------------------------Testing updateRole----------------------------- */
  it('updateRole should match a mock request', () => {
    const res = { data: updateRole }
    axios.put.mockResolvedValue(res)
    roleService.updateRole(updateRole, 'token').then(res => expect(res).toEqual(updateRole))
  })

  it('updateRole should fail when unauthorized', () => {
    axios.put.mockRejectedValue(err)
    roleService.updateRole(updateRole, '').catch(res => expect(res.message).toEqual('401 Unauthorized'))
  })
  /* ---------------------------------------------------------------------------- */
})
