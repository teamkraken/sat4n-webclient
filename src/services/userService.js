import axios from 'axios'

const HOSTNAME = window.location.hostname
const PROTOCOL = window.location.protocol
const PORT = window.location.port
/* No need to test standard JS methods */
/* istanbul ignore next */
const encodeForm = (data) => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

const userService = () => ({
  /**
  * Sends an HTTP POST request to the server
  * @param {Object} user user object to be created
  * @param {string} token User authorization token
  * @returns
  */
  addUser: (user, token) => axios.post(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users`, encodeForm(user), { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP DELETE request to the server
  * @param {number} id id of a user to be deleted
  * @param {string} token User authorization token
  */
  deleteUser: (id, token) => axios.delete(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP GET request to the server
  * @param {string} token User authorization token
  * @returns
  */
  getUser: (id, token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users/${id}`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP GET request to the server
  * @param {string} token User authorization token
  * @returns
  */
  getUsers: (token) => axios.get(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users`, { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    }),

  /**
  * Sends an HTTP PUT request to the server
  * @param {Object} user user object to be created
  * @param {string} token User authorization token
  * @returns
  */
  updateUser: (user, token) => axios.put(`${PROTOCOL}//${HOSTNAME}:${PORT}/api/users/${user.id}`, encodeForm(user), { headers: { Authorization: `Bearer ${token}` } })
    .then((res) => { return res.data })
    .catch((err) => {
      throw new Error(`${err.response.status} ${err.response.data}`)
    })
})

export default userService()
