import { ADD_ROLE, DELETE_ROLE, GET_ROLE, GET_ROLES, UPDATE_FORM, TOGGLE_ENDPOINT, ERROR_ROLES, CLEAR_ROLE_ERRORS, UPDATE_ROLE } from '../constants/roleConstants'

const initialState = {
  role: {
    name: '',
    resources: [],
    description: ''
  },
  roles: [],
  error: ''
}

/**
 * Manipulates roles data in redux store state
 *
 * @param {Object} [state=initialState]
 * @param {Object} action
 * @returns New redux state depending on the action
 */
const roleReducer = (state = initialState, action) => {
  const error = ''
  let roles = state.roles
  let role = Object.assign({}, state.role)
  let resources = state.resources

  switch (action.type) {
    case ADD_ROLE:
      roles.push(action.payload)
      return { ...state, roles, error }
    case DELETE_ROLE:
      roles = state.roles.filter(role => role.id !== action.payload)
      return { ...state, roles, error }
    case GET_ROLE:
      return { ...state, role: action.payload, error }
    case GET_ROLES:
      return { ...state, roles: action.payload, error }
    case UPDATE_ROLE:
      roles = state.roles.filter(role => role.id !== action.payload.id)
      roles.push(action.payload)
      return { ...state, roles, error }
    case UPDATE_FORM:
      // Update role and return update state
      role = { ...state.role, [action.payload.name]: action.payload.value }
      return { ...state, role, error }
    case TOGGLE_ENDPOINT:
      // Update endpoints in role and return updated state
      resources = role.resources.map((resource) => {
        return resource.name === action.payload.name
          ? { ...resource, enabled: action.payload.value }
          : resource
      })

      role = { ...state.role, resources }
      return { ...state, role, error }
    case ERROR_ROLES:
      return { ...state, error: action.payload }
    case CLEAR_ROLE_ERRORS:
      return { ...state, error: '' }
    default:
      return state
  }
}

export default roleReducer
