import { ADD_USER, DELETE_USER, GET_USER, GET_USERS, UPDATE_FORM, ERROR_USERS, CLEAR_USER_ERRORS } from '../constants/userConstants'

const initialState = {
  user: {
    username: '',
    roleId: ''
  },
  users: [],
  error: ''
}

/**
 * Manipulates users data in redux store state
 *
 * @param {Object} [state=initialState]
 * @param {Object} action
 * @returns New redux state depending on the action
 */
const userReducer = (state = initialState, action) => {
  const error = ''
  let user = state.user
  let users = state.users

  switch (action.type) {
    case ADD_USER:
      users.push(action.payload)
      return { ...state, users, error }
    case DELETE_USER:
      users = state.users.filter(user => user.id !== action.payload)
      return { ...state, users, error }
    case GET_USER:
      user = { ...action.payload, roleId: action.payload.role ? action.payload.role.id : 0 }
      return { ...state, user, error }
    case GET_USERS:
      return { ...state, users: action.payload, error }
    case UPDATE_FORM:
      // Update user and return update state
      user = { ...state.user, [action.payload.name]: action.payload.value }
      return { ...state, user, error }
    case ERROR_USERS:
      return { ...state, error: action.payload }
    case CLEAR_USER_ERRORS:
      return { ...state, error: '' }
    default:
      return state
  }
}

export default userReducer
