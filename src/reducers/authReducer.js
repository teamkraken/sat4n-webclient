import { AUTH_LOG_IN, AUTH_LOG_OUT, AUTH_FAILED } from '../constants/authConstants'

const initialState = {
  user: JSON.parse(window.localStorage.user || '{}'),
  token: window.localStorage.sat4nJWT || '',
  resources: JSON.parse(window.localStorage.resources || '{}'),
  error: null
}

/**
 * Manipulates users authentication data in redux store state
 *
 * @param {Object} [state=initialState]
 * @param {Object} action
 * @returns New redux state depending on the action
 */
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_LOG_IN:
      // return action.user
      return { ...action.payload, error: null }
    case AUTH_LOG_OUT:
      // return {}
      return initialState
    case AUTH_FAILED:
      return { token: '', error: action.payload }
    default:
      return state
  }
}

export default authReducer
