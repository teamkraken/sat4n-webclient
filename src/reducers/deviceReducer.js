import { ADD_DEVICE, GET_DEVICE, GET_DEVICES, DELETE_DEVICE, DEVICE_PORT_DISABLE, DEVICE_PORT_ENABLE, DEVICE_PORT_RESTART, DEVICE_PORT_UPDATE, ERROR_DEVICES, CLEAR_DEVICE_ERRORS, LOADING_UPDATE } from '../constants/deviceConstants'

const initialState = {
  device: {
    arpTable: [],
    ipSnoop: [],
    macTable: [],
    neighbors: [],
    ports: [],
    vlans: [],
    log: []
  },
  devices: [],
  portLoading: [],
  error: ''
}

/**
 * Manipulates devices data in redux store state
 *
 * @param {Object} [state=initialState]
 * @param {Object} action
 * @returns New redux state depending on the action
 */
const deviceReducer = (state = initialState, action) => {
  const error = ''
  let devices = state.devices
  let portLoading = state.portLoading

  switch (action.type) {
    case ADD_DEVICE:
      return { ...state, devices: [...state.device, action.payload], error }
    case GET_DEVICE:
      return { ...state, device: action.payload, error }
    case GET_DEVICES:
      return { ...initialState, devices: action.payload, error }
    case DELETE_DEVICE:
      devices = state.devices.filter(device => device.id !== action.payload)
      return { ...state, devices, error }
    case ERROR_DEVICES:
      return { ...state, error: action.payload }
    case CLEAR_DEVICE_ERRORS:
      return { ...state, error }
    case DEVICE_PORT_DISABLE:
      portLoading = state.portLoading.filter(portID => portID !== action.payload.portID)
      return { ...state, device: action.payload.device, portLoading, error }
    case DEVICE_PORT_ENABLE:
      portLoading = state.portLoading.filter(portID => portID !== action.payload.portID)
      return { ...state, device: action.payload.device, portLoading, error }
    case DEVICE_PORT_RESTART:
      portLoading = state.portLoading.filter(portID => portID !== action.payload)
      return { ...state, portLoading }
    case DEVICE_PORT_UPDATE:
      portLoading = state.portLoading.filter(portID => portID !== action.payload.portID)
      return { ...state, device: action.payload.device, portLoading, error }
    case LOADING_UPDATE:
      return { ...state, portLoading: [...state.portLoading, action.payload] }
    default:
      return state
  }
}

export default deviceReducer
