import { ERROR_AUDIT, AUDIT_GET } from '../constants/auditConstants'

const initialState = {
  logs: [],
  error: ''
}

/**
 * Manipulates audit logs in redux store state
 *
 * @param {Object} [state=initialState]
 * @param {Object} action
 * @returns New redux state depending on the action
 */
const auditReducer = (state = initialState, action) => {
  const error = ''

  switch (action.type) {
    case AUDIT_GET:
      return { ...initialState, logs: action.payload, error }
    case ERROR_AUDIT:
      return { ...state, error: action.payload }
    default:
      return state
  }
}

export default auditReducer
