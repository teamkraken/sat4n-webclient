import { combineReducers } from 'redux'
import audit from './auditReducer'
import auth from './authReducer'
import device from './deviceReducer'
import role from './roleReducer'
import user from './userReducer'

export default combineReducers({
  audit,
  auth,
  device,
  role,
  user
})
