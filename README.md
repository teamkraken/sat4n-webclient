[![pipeline](https://gitlab.com/teamkraken/sat4n-webclient/badges/master/pipeline.svg)](https://gitlab.com/teamkraken/sat4n-webclient/commits/master)
[![coverage](https://gitlab.com/teamkraken/sat4n-webclient/badges/master/coverage.svg)](https://gitlab.com/teamkraken/sat4n-webclient/commits/master)
[![license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Simple Administrative Tools for Networking
This is a web service to make the management of enterprise level networks more efficient. This is
done by moving small and repetitive tasks from the command line to the web service. This is a web app
frontend for that service. This app is meant to run internally on the network being managed.

## Getting started
To install the web app, follow these steps:

1. Have the latest version of [node.js](https://nodejs.org/en/) installed.
  
2. Clone the repository:
`<local>/ $ git clone https://gitlab.com/teamkraken/sat4n-webclient/`

3. Run the install script in the root directory:
`<local>/sat4n-webclient/ $ npm i`

4. Then run the client using node:
`<local>/sat4n-webclient/ $ npm start`

All the usage documentation is embeded in the web app itself, accessible via the question marks on 
each page.

## License
[gpl-3.0](LICENSE)
You may copy, distribute and modify the software as long as you track changes/dates in source
files. Any modifications to or software including (via compiler) GPL-licensed code must also
be made available under the GPL along with build & install instructions.
This license provides NO warranty and limits the authors liability to the full extent of the law.
This license does not forbid commercial use.