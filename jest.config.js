// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: true,
  // An array of glob patterns indicating a set of files for which coverage information should be collected
  collectCoverageFrom: ['src/**/*.{js,jsx,mjs}'],
  // The directory where Jest should output its coverage files
  coverageDirectory: 'coverage',
  // An object that configures minimum threshold enforcement for coverage results
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 70,
      lines: 70,
      statements: 70
    }
  },
  // An array of file extensions your modules use
  moduleFileExtensions: ['js', 'json', 'jsx', 'css'],
  transform: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '^.+\\.(js|jsx)?$': 'babel-jest'
  },
  // Regex to match necessary objects to mocks
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
    '\\.(jpg|jpeg|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
     '<rootDir>/__mocks__/fileMock.js',
    // mocking .png as identity-obj-proxy to pass tests using img tags
    '\\.(css|less|scss|sass|ico|png)$': 'identity-obj-proxy'
  },
  // Files excluded from coverage report
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/src/index.js',
    '<rootDir>/src/constants',
    '<rootDir>/src/components/Messages'
  ],
  // The paths to modules that run some code to configure or set up the testing environment before each test
  setupFiles: ['<rootDir>/enzyme.config.js'],

  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],

  // The test environment that will be used for testing
  testEnvironment: 'jsdom',
  // The glob patterns Jest uses to detect test files
  testMatch: ['**/__tests__/**/*.js?(x)', '**/?(*.)+(spec|test).js?(x)'],

  // An array of regexp pattern strings that are matched against all test paths,
  // matched tests are skipped
  testPathIgnorePatterns: ['\\\\node_modules\\\\'],
  // This option sets the URL for the jsdom environment.
  // It is reflected in properties such as location.href

  testEnvironmentOptions: {
    url: 'http://localhost'
  },

  // An array of regexp pattern strings that are matched against all source file paths,
  // matched files will skip transformation
  transformIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/public/*ico',
    '<rootDir>/public/*png'
  ],
  // Indicates whether each individual test should be reported during the run
  verbose: false,
  // Do not mock these directories
  unmockedModulePathPatterns: [
    '<rootDir>/node_modules/react',
    '<rootDir>/node_modules/fbjs',
    '<rootDir>/node_modules/react-dom',
    '<rootDir>/node_modules/react-addons-test-utils',
    '<rootDir>/node_modules/bootstrap',
    '<rootDir>/node_modules/react-bootstrap'
  ]
}
