const HtmlWebPackPlugin = require('html-webpack-plugin')
const postcssPresetEnv = require('postcss-preset-env')
const webpack = require('webpack')
const path = require('path')

module.exports = {
  mode: 'development',
  devtool: 'eval',
  entry: {
    main: './src/index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index_bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(?:js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              ['@babel/preset-env', { targets: 'defaults' }]
            ]
          }
        }
      },
      {
        test: /\.(png|jpe?g)/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: '[name].[ext]',
              limit: 10000
            }
          },
          {
            loader: 'img-loader'
          }
        ]
      },
      {
        // For pure CSS - /\.css$/i,
        // For Sass/SCSS - /\.((c|sa|sc)ss)$/i,
        // For Less - /\.((c|le)ss)$/i,
        test: /\.((c|le)ss)$/i,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              // Run `postcss-loader` on each CSS `@import` and CSS modules/ICSS imports, do not forget that `sass-loader` compile non CSS `@import`'s into a single file
              // If you need run `sass-loader` and `postcss-loader` on each CSS `@import` please set it to `2`
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: { postcssOptions: { plugins: () => [postcssPresetEnv({ stage: 0 })] } }
          },
          // Can be `less-loader`
          {
            loader: 'less-loader'
          }
        ]
      },
      // For webpack v5
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/i,
        // More information here https://webpack.js.org/guides/asset-modules/
        type: 'asset'
      },
      {
        test: /\.html&/i,
        loader: 'html-loader'
      },
      {
        test: /\.ico$/,
        use: [{
          loader: 'file-loader?name=[name].[ext]'
        }]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.less', '.css', '.ico']
  },
  plugins: [
    new HtmlWebPackPlugin({
      favicon: 'public/favicon.ico',
      title: 'SAT:4N',
      template: 'public/index.html',
      inject: 'body'
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    })
  ],
  devServer: {
    proxy: {
      context: ['/auth', '/api'],
      target: 'http://localhost:8080'
    },
    static: {
      directory: path.resolve(__dirname, 'public')
    },
    host: '0.0.0.0',
    hot: true,
    allowedHosts: 'all',
    open: false,
    compress: true,
    port: 9000,
    historyApiFallback: true
  }
}
